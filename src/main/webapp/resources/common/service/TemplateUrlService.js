/**
 * Created by ioann on 02.06.16.
 */

'use strict';

angular.module('mainApp').service('templateUrlService',
    [
        function () {
            var couponTemplateUrl = '/resources/common/templates/couponView.html';

            this.getCouponTemplateUrl = function () {
                return couponTemplateUrl;
            }
        }
    ]);