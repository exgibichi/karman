/**
 * Created by ioann on 30.05.16.
 */
angular.module('mainApp').service('dialogService',
    ['$mdDialog', function ($mdDialog) {

        var self = this;

        /**
         * Показать алерт
         * @param title заголовок
         * @param content контент
         */
        this.showAlert = function (title, content) {
            showAlert(title, content);
        };

        /**
         * Показать ошибку (алерт с текстом)
         * @param content контент
         */
        this.showError = function (content) {
            showAlert("Ошибка", content);
        };



        function showAlert(title, text) {
            $mdDialog.show({
                controller: AlertController,
                templateUrl: '/resources/company/app/dialog/alertDialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,

                resolve: {
                    title: function () {
                        return title;
                    },
                    text: function () {
                        return text;
                    }
                }
            });
        }

        function AlertController($scope, title, text) {

            $scope.title = title;
            $scope.text = text;

            $scope.cancel = function() {
                $mdDialog.cancel();
            }
        }
    }
    ]);
