/**
 * Created by ioann on 30.05.16.
 */

'use strict';

angular.module('mainApp').service('httpService',
    ['$http', '$mdDialog', 'dialogService', '$window',
        function ($http, $mdDialog, dialogService, $window) {

            this.get = function (url, callback, errorCallback) {
                errorCallback = errorCallback || defaultErrorCallback;

                $http.get(url)
                    .success(function (response) {
                        if (response.status && response.status == "ERROR") {
                            var message = null;
                            if (response.message) {

                                if (response.message == 'An Authentication object was not found in the SecurityContext') {
                                    return;
                                }

                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            errorCallback(message);
                        } else {
                            callback(response);
                        }
                    })
                    .error(function (errorMessage) {
                        errorCallback(errorMessage);
                    });
            };


            this.post = function (url, data, callback, errorCallback, finallCallback) {
                errorCallback = errorCallback || defaultErrorCallback;

                $http.post(url, data)
                    .success(function (response) {
                        if (response.status && response.status == "ERROR") {
                            var message = null;

                            if (response.code && response.code === 900) {
                                showNotEnoughMoneyError();
                                if (finallCallback) {
                                    finallCallback();
                                }
                                return;
                            }
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            errorCallback(message);

                            if (finallCallback) {
                                finallCallback();
                            }
                        } else {
                            callback(response);

                            if (finallCallback) {
                                finallCallback();
                            }
                        }
                    })
                    .error(function (errorMessage) {
                        errorCallback(errorMessage);

                        if (finallCallback) {
                            finallCallback();
                        }
                    });
            };

            function defaultErrorCallback(errorMessage) {
                dialogService.showError(errorMessage);
            };

            function showNotEnoughMoneyError() {
                $mdDialog.show({
                    controller: NotEnoughMoneyDialog,
                    templateUrl: '/resources/company/app/dialog/notEnoughMoneyDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false
                });
            };

            function NotEnoughMoneyDialog($scope) {
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.toPurse = function () {
                    $window.open('/#/purse', '_blank');
                    $mdDialog.cancel();
                };
            };

        }]);