/**
 * Created by ioann on 04.06.16.
 */


'use strict';

angular.module('mainApp.purse', ['ngRoute', 'ngMaterial'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/purse', {
            templateUrl: '/resources/company/app/purse/purse.html',
            controller: 'purseController'
        });
    }])

    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }

                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    })

    .controller('purseController', ['$rootScope', '$scope', '$location',
        'httpService', 'authService',
        function ($rootScope, $scope, $location, httpService, authService) {
            authService.secure('COMPANY');

            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));
        }]);