/**
 * Created by ioann on 01.06.16.
 */
'use strict';

angular.module('mainApp.createCoupon', ['ngMaterial', 'ngSanitize', 'ui.select',
    'ngFileUpload', 'color.picker', 'md-steppers', 'ui.mask'])

    .config(['$routeProvider', '$mdDateLocaleProvider',
        function ($routeProvider, $mdDateLocaleProvider) {

            $routeProvider
                .when('/createCoupon', {
                    templateUrl: '/resources/company/app/createCoupon/createCoupon.html',
                    controller: 'createCouponController'
                })
                .when('/editCoupon/:id', {
                    templateUrl: '/resources/company/app/createCoupon/createCoupon.html',
                    controller: 'createCouponController'
                });

            var myShortMonths = ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Окт', 'Нояб', 'Дек'];

            $mdDateLocaleProvider.months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
            $mdDateLocaleProvider.shortMonths = myShortMonths;
            $mdDateLocaleProvider.days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
            $mdDateLocaleProvider.shortDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
            // Can change week display to start on Monday.
            $mdDateLocaleProvider.firstDayOfWeek = 1;

            // Example uses moment.js to parse and format dates.
            $mdDateLocaleProvider.parseDate = function (dateString) {
                var m = moment(dateString, 'L', true);
                return m.isValid() ? m.toDate() : new Date(NaN);
            };

            $mdDateLocaleProvider.formatDate = function (date) {
                return date ? moment(date).format('YYYY-MM-DD') : '';
            };

        }])

    .controller('createCouponController', ['$rootScope', '$scope', '$location', 'Upload',
        'dialogService', 'httpService', 'templateUrlService', 'authService',
        '$routeParams', '$mdDialog',
        function ($rootScope, $scope, $location, Upload, dialogService,
                  httpService, templateUrlService, authService, $routeParams, $mdDialog) {
            authService.secure('COMPANY');

            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));

            var id = $routeParams.id;

            initCreation();
            fetchColors();

            var maxAmountOfImages = 4;
            $scope.coupon.images = [];

            if (id) {
                initEditing(id);
            } else {
                $scope.selectedCouponType = 'DISCOUNT';
                $scope.coupon.couponType = {type: 'DISCOUNT'};
                $scope.coupon.flagColorHex = '#FFFFFF';
                $scope.coupon.backgroundColorHex = '#FFFFFF';
            }

            var maxCountOfContacts = 3;

            $scope.couponViewTemplate = templateUrlService.getCouponTemplateUrl();
            $scope.regions = [];
            $scope.categories = [];
            $scope.selectedTab = 0;

            $scope.couponTypes = [
                {
                    label: 'Скидка', value: 'DISCOUNT', class: "create-coupon-type-discount-div"
                },
                {
                    label: 'Подарок', value: 'GIFT', class: "create-coupon-type-gift-div"
                },
                {
                    label: 'Скидка и подарок', value: 'DISCOUNT_GIFT', class: "create-coupon-type-discount-gift-div"
                },
                {
                    label: 'Ценовое предложение', value: 'PRICE', class: "create-coupon-type-price-div"
                }
            ];


            $scope.isFlagColorDeleted = function () {
                var coupon = $scope.coupon;
                var colors = $scope.colors;
                var isDeleted = true;

                if (coupon && coupon.flagColorHex && colors) {

                    var flagColorHex = coupon.flagColorHex;

                    if (flagColorHex.toUpperCase() == '#FFFFFF') {
                        return false;
                    }

                    angular.forEach(colors, function (value) {
                        if (flagColorHex.toLowerCase() == value.colorHex.toLowerCase()) {
                            isDeleted = false;
                        }
                    });

                    if (isDeleted) {
                        coupon.deletedflagColorHex = flagColorHex;
                    }

                    return isDeleted
                } else {
                    return !isDeleted;
                }
            };

            $scope.isBackgroundColorDeleted = function () {
                var coupon = $scope.coupon;
                var colors = $scope.colors;
                var isDeleted = true;

                if (coupon && coupon.backgroundColorHex && colors) {

                    var backgroundColorHex = coupon.backgroundColorHex;

                    if (backgroundColorHex.toUpperCase() == '#FFFFFF') {
                        return false;
                    }

                    angular.forEach(colors, function (value) {
                        if (backgroundColorHex.toLowerCase() == value.colorHex.toLowerCase()) {
                            isDeleted = false;
                        }
                    });

                    if (isDeleted) {
                        coupon.deletedBackgroundColorHex = backgroundColorHex;
                    }

                    return isDeleted
                } else {
                    return !isDeleted;
                }
            };

            var savingDisabled = false;

            $scope.onCouponTypeChange = function (value) {
                $scope.coupon.couponType = {
                    type: value
                };

                if ($scope.coupon.giftImage && value != 'GIFT' && value != 'DISCOUNT_GIFT') {
                    $scope.coupon.giftImage = null;
                }
            };


            $scope.removeGiftImage = function () {
                var couponType = $scope.coupon.giftImage = null;
            };

            $scope.removeCouponImage = function (image) {
                var index = $scope.coupon.images.indexOf(image);

                $scope.coupon.images.splice(index, 1);
            };

            $scope.isNeedToPayForBackgroundColor = function () {
                var coupon = $scope.coupon;

                if (coupon.isBackgroundColorHexPayed) {
                    return false;
                }

                if (coupon.backgroundColorHex != undefined && coupon.backgroundColorHex.toUpperCase() == "#FFFFFF") {
                    return false;
                }

                return true;
            };


            $scope.dateToFilter = function (dateTo) {
                return moment(dateTo) - moment().subtract(1, "days") > 0;
            };

            $scope.dateValidateInput = function() {
                var dateInputValue = document.getElementsByClassName("md-datepicker-input")[0].value;
                document.getElementsByClassName("md-datepicker-input")[0].value = dateInputValue.replace(/[^-\d]*/gi,'');
            }

            $scope.calcBackgroundPrice = function () {
                var dateTo = $scope.coupon.dateTo;

                if ($scope.isNeedToPayForBackgroundColor() && dateTo) {

                    httpService.post('/prices/backgroundColor',
                        {
                            date: moment(dateTo).format('YYYY-MM-DDTHH:mm')
                        },
                        function (response) {
                            $scope.backgroundColorPrice = response;
                        }
                    );
                }

            };

            $scope.nextTab = function () {
                if (validateCurrentTab()) {
                    $scope.selectedTab++;
                }
            };

            $scope.prevTab = function () {
            console.log($scope.coupon);
                if ($scope.selectedTab > 0) {
                    $scope.selectedTab--;
                }
            };


            $scope.daysRemained = function () {
                var dateTo = $scope.coupon.dateTo;

                var daysRemained = moment(dateTo).diff(moment(), 'days');

                if (daysRemained < 0) {
                    return "-";
                } else if (daysRemained == 0) {
                    return 'До 24:00'
                } else {
                    return 'Осталось: ' + daysRemained + ' ' + calcDayDeclension(daysRemained);
                }
            };


            $scope.calcDayDeclension = function (days) {
                return calcDayDeclension(days);
            }

            function calcDayDeclension(days) {
                var daysString;
                if (days % 100 > 10 && days % 100 < 20)
                    daysString = " дней";
                else if (days % 10 == 1)
                    daysString = " день";
                else if (days % 10 > 1 && days % 10 < 5)
                    daysString = " дня";
                else
                    daysString = " дней";

                return daysString;
            };

            $scope.dateToFormatted = function () {
                var dateTo = $scope.coupon.dateTo;

                if (!dateTo) {
                    return moment().add(30, 'days').format('YYYY-MM-DD');
                } else {
                    return moment(dateTo).format('YYYY-MM-DD');
                }

            };

            $scope.showGeocoder = function (contact) {
                $mdDialog.show({
                    controller: 'yandexGeocoderController',
                    templateUrl: '/resources/company/app/dialog/yandex/yandexGeocoderDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    resolve: {
                        callback: function () {
                            return function (geoObject) {
                                contact.address = geoObject.properties.balloonContent;
                                contact.longitude = geoObject.geometry.coordinates[0];
                                contact.latitude = geoObject.geometry.coordinates[1];
                            };
                        }
                    }
                });
            };

            $scope.savingDisabled = function () {
                return savingDisabled;
            };


            function validateCurrentTab() {

                if ($scope.selectedTab == 0) {
                    return validateData();
                } else {
                    dialogService.showError("Данный вид действия не поддерживается");
                    return false;
                }
            }


            function validateData() {

                var coupon = $scope.coupon;

                //Проверка типа размещаемой акции
                var isCouponTypeValid = false;

                angular.forEach($scope.couponTypes, function (value) {
                    if (value.value == coupon.couponType.type) {
                        isCouponTypeValid = true;
                    }
                });

                if (!isCouponTypeValid) {
                    dialogService.showError("Не задан тип размещаемой акции");
                    return false;
                }

                //Проверка цвета флажка
                if (!coupon.flagColorHex) {
                    dialogService.showError("Не задан цвет флажка");
                    return false;
                }

                var couponType = coupon.couponType;
                //Проверка данных, привязанных к типу размещаемой акции
                if (couponType.type == 'DISCOUNT') {
                    //Проверка типа "Скидка"
                    if (!couponType.discount || couponType.discount < 1 || couponType.discount > 100) {
                        dialogService.showError("Должен быть задан размер скидки от 1 до 100");
                        return false;
                    }
                } else if (couponType.type == 'GIFT') {
                    //Проверка типа "Подарок"
                    if (!couponType.giftTitle || !couponType.giftTitle.trim()) {
                        dialogService.showError("Не задано название подарка");
                        return false;
                    }
                    if (!couponType.giftCondition || !couponType.giftCondition.trim()) {
                        dialogService.showError("Не заданы условия получения подарка");
                        return false;
                    }
                    if (!couponType.giftDescription || !couponType.giftDescription.trim()) {
                        dialogService.showError("Не задано описание подарка");
                        return false;
                    }
                    if (!coupon.giftImage) {
                        dialogService.showError("Не задано изображение подарка");
                        return false;
                    }
                } else if (couponType.type == 'DISCOUNT_GIFT') {
                    //Проверка типа "Скидка и подарок"
                    if (!couponType.discount || couponType.discount < 1 || couponType.discount > 100) {
                        dialogService.showError("Должен быть задан размер скидки от 1 до 100");
                        return false;
                    }
                    if (!couponType.giftTitle || !couponType.giftTitle.trim()) {
                        dialogService.showError("Не задано название подарка");
                        return false;
                    }
                    if (!couponType.giftDescription || !couponType.giftDescription.trim()) {
                        dialogService.showError("Не задано описание подарка");
                        return false;
                    }
                    if (!coupon.giftImage) {
                        dialogService.showError("Не задано изображение подарка");
                        return false;
                    }
                } else if (couponType.type == 'PRICE') {
                    //Проверка типа "Ценник"
                    if (!couponType.price || couponType.price < 1) {
                        dialogService.showError("Ценник должен быть положительным числом");
                        return false;
                    }
                }


                if (couponType.type == 'GIFT') {
                    coupon.title = couponType.giftTitle;
                    coupon.description = couponType.giftDescription;
                    coupon.images = [coupon.giftImage];
                }

                //Проверка заголовка акции
                if (!coupon.title || !coupon.title.trim()) {
                    dialogService.showError("Не задано краткое описание акции");
                    return false;
                }

                //Проверка подробного описания акции
                if (!coupon.description || !coupon.description.trim()) {
                    dialogService.showError("Не заданы полные условия акции");
                    return false;
                }

                //Проверка даты
                if (!coupon.dateTo) {
                    dialogService.showError("Не задана дата окончания акции");
                    return false;
                }
                if (moment(coupon.dateTo) - moment().subtract(1, "days") <= 0) {
                    dialogService.showError("Задана некорректная дата окончания акции");
                    return false;
                }

                //Проверка категории
                if (!coupon.category) {
                    dialogService.showError("Не задана категория");
                    return false;
                }

                //Проверка региона
                if (!coupon.region) {
                    dialogService.showError("Не задан регион");
                    return false;
                }

                //Проверка списка изображений
                if (coupon.images.length == 0) {
                    dialogService.showError("Необходимо добавить фото");
                    return false;
                }


                //Проверка контакнтов
                var contacts = coupon.contacts;

                var tempMap = {};

                for (var i = 0; i < contacts.length; ++i) {
                    var contact = contacts[i];

                    if (!contact.address || !contact.address.trim() || !contact.phoneNumber || !contact.phoneNumber.trim()) {
                        dialogService.showError("Данные контактов должны быть заполнены");
                        return false;
                    }

                    if (tempMap[contact.address.trim()]) {
                        dialogService.showError("Недопустимо повторное использование одного и того же адреса");
                        return false;
                    }

                    tempMap[contact.address.trim()] = true;
                }


                //Проверка цвета фона акции
                if (!coupon.backgroundColorHex) {
                    dialogService.showError("Не задан цвет фона акции");
                    return false;
                }

                return true;
            }


            $scope.imagesAvailable = function () {
                return maxAmountOfImages - $scope.coupon.images.length;
            };

            $scope.newContact = function () {
                if ($scope.coupon.contacts.length < maxCountOfContacts) {
                    $scope.coupon.contacts.push({
                        address: '',
                        phoneNumber: ''
                    });
                }
            };

            $scope.removeContact = function (contact) {
                $scope.coupon.contacts = $scope.coupon.contacts.filter(function (i) {
                    return i != contact;
                });
            };

            $scope.isMaxCountOfContacts = function () {
                return $scope.coupon.contacts.length == maxCountOfContacts;
            };

            $scope.searchRegions = function (search) {
                var query;

                if (search && search.trim()) {
                    query = "?title=" + search
                        + "&limit=10";
                } else {
                    return;
                }

                httpService.get('/regions/all' + query,
                    function (response) {
                        $scope.regions = response;
                    });
            };

            $scope.searchCategories = function (search) {
                var query;

                if (search && search.trim()) {
                    query = "?title=" + search
                        + "&limit=10";
                } else {
                    query = "";
                }

                httpService.get('/categories/all' + query,
                    function (response) {
                        $scope.categories = response;
                    });
            };

            $scope.uploadImage = function (file, isGift, errFiles) {

                if (errFiles.length > 0) {

                    var errFile = errFiles[0];

                    dialogService.showError("Размер изображения не должен превышать " + errFile.$errorParam);

                    return;
                }

                if (file) {

                    file.upload = Upload.upload({
                        url: '/image.upload',
                        data: {image: file}
                    });

                    file.upload.then(function (response) {
                        if (response.status && response.status == "ERROR") {
                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            if (isGift) {
                                $scope.coupon.giftImage = response.data.fileName;
                            } else {
                                $scope.coupon.images.push(response.data.fileName);
                            }
                        }
                    }, function (response) {
                        dialogService.showError(response);
                    });
                }
            };


            $scope.saveCoupon = function () {

                var coupon = prepareRequestBody();
                var urlSuffix = coupon.id ? 'edit' : 'create';
                savingDisabled = true;

                httpService.post('/coupons/' + urlSuffix,
                    coupon, function (response) {
                        dialogService.showAlert('Акция успешно сохранена');
                        $location.url('/history');
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    }, function () {
                        savingDisabled = false;
                    });
            };


            function initCreation() {
                $scope.coupon = {};
                $scope.coupon.contacts = [
                    {
                        address: '',
                        phoneNumber: ''
                    }
                ];
            }


            function fetchColors() {
                httpService.get('/colors/all',
                    function (response) {
                        angular.forEach(response, function (value) {
                            value.colorHex = value.colorHex.toUpperCase();
                        });

                        $scope.colors = response;
                    }
                );
            };

            function initEditing(id) {
                httpService.get('/coupons/my/' + id,
                    function (response) {
                        var coupon = angular.copy(response);

                        coupon.dateTo = new Date(response.dateTo);

                        if (!coupon.backgroundColorHex) {
                            coupon.backgroundColorHex = "#FFFFFF";
                        }

                        coupon.backgroundColorHex = coupon.backgroundColorHex.toUpperCase();
                        coupon.flagColorHex = coupon.flagColorHex.toUpperCase();

                        angular.forEach(coupon.contacts, function (value) {
                            value.phoneNumber = value.phoneNumber.substring(2);
                        });

                        $scope.coupon = coupon;

                        $scope.selectedCouponType = coupon.couponType.type;

                        if (coupon.couponType.giftImage) {
                            coupon.giftImage = coupon.couponType.giftImage;
                        }
                    },
                    function (errorResponse) {
                        dialogService.showError(errorResponse);
                        $location.url('/history');
                    });
            };

            function prepareRequestBody() {
                var coupon = angular.copy($scope.coupon);

                if (coupon.dateTo) {
                    coupon.dateTo = moment(coupon.dateTo).format('YYYY-MM-DDTHH:mm');
                }

                if (coupon.region) {
                    coupon.regionId = coupon.region.id;
                }

                if (coupon.category) {
                    coupon.categoryId = coupon.category.id;
                }

                angular.forEach(coupon.couponType, function (value, key) {
                    if (key != 'type') {
                        coupon[key] = value;
                    }
                });

                angular.forEach(coupon.contacts, function(value) {
                    value.phoneNumber = "+7" + value.phoneNumber;
                });


                delete coupon.couponType;

                return coupon;
            };


            function hexToRgb(hex) {
                if(hex == undefined) {
                    return null;
                }
                // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                    return r + r + g + g + b + b;
                });

                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                } : null;
            }


            $scope.calcFontColor = function (backgroundHex) {
                var backGroundRgb = hexToRgb(backgroundHex);

                var d = 0;

                if(backGroundRgb === null) {
                    d = 255;
                } else {
                    // Counting the perceptive luminance - human eye favors green color...
                    var a = 1 - ( 0.299 * backGroundRgb.r
                        + 0.587 * backGroundRgb.g + 0.114 * backGroundRgb.b) / 255;

                    if (a < 0.3) {
                        d = 0; // bright colors - black font
                    } else {
                        d = 255; // dark colors - white font
                    }
                }
                return 'rgb(' + d + ',' + d + ',' + d + ')';
            };

            $scope.calcGiftBackGround = function (flagColorHex) {
                var backGroundRgb = hexToRgb(flagColorHex);

                var cssFileUrlProperty = 0;

                // Counting the perceptive luminance - human eye favors green color...
                var a = 1 - ( 0.299 * backGroundRgb.r
                    + 0.587 * backGroundRgb.g + 0.114 * backGroundRgb.b) / 255;

                if (a < 0.3)
                    cssFileUrlProperty = "url('/resources/img/gift_iconb.png')"; // bright colors - black picture
                else
                    cssFileUrlProperty = "url('/resources/img/gift_iconw.png')";  // dark colors - white picture

                return cssFileUrlProperty;
            }

        }]);