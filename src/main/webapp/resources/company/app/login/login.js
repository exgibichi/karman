/**
 * Created by ioann on 04.06.16.
 */


'use strict';

angular.module('mainApp.login', ['ngRoute', 'ngMaterial', 'ui.mask'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: '/resources/company/app/login/login.html',
            controller: 'loginController'
        });
    }])

    .controller('loginController', ['$rootScope', '$scope', '$location',
        'httpService', 'authService', 'dialogService',
        function ($rootScope, $scope, $location, httpService, authService, dialogService) {
            authService.anonumousOnly();
            $scope.credentials = {};

            $scope.signIn = function () {

                httpService.post('/sign.in', $scope.credentials,
                    function (response) {
                        if (response.role != 'COMPANY') {
                            dialogService.showAlert('Доступ разрешен только представителям компаний');

                            httpService.post('/sign.out', null,
                                function (response) {
                                });

                            return;
                        }


                        $rootScope.principal.name = response.name;
                        $rootScope.principal.phoneNumber = response.phoneNumber;
                        $rootScope.principal.role = response.role;
                        $location.url('/');
                    });
            };

            $scope.toPasswordRecovery = function () {
                $location.url('/passwordRecovery');
            };

            $scope.toRegistration = function () {
                $location.url('/registration');
            };

        }]);
