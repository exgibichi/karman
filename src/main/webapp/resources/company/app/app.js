/**
 * Created by ioann on 03.06.16.
 */

'use strict';

angular.module('mainApp', [
    'ngRoute',
    'ngCookies',
    'ngAnimate',
    'ngMaterial',
    'ngMdIcons',
    'mainApp.registration',
    'mainApp.login',
    'mainApp.passwordRecovery',
    'mainApp.cabinet',
    'mainApp.purse',
    'mainApp.history',
    'mainApp.createCoupon',
    'mainApp.couponStats',
    'mainApp.yandexGeocoder'
])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider
            .when('/settings', {
                templateUrl: '/resources/company/app/cabinet/cabinet.html',
                controller: 'settingsController'
            })
            .otherwise({redirectTo: '/'});
    }])

    .controller('settingsController', ['$location', 'cabinetService',
        function ($location, cabinetService) {
            cabinetService.setIsSettingsActive(true);
            $location.url('/cabinet');
        }])

    .controller('mainController', ['$rootScope', '$scope', '$location',
        'httpService', 'authService', 'cabinetService', 'paymentResultService',
        function ($rootScope, $scope, $location, httpService,
                  authService, cabinetService, paymentResultService) {
            authService.secure('COMPANY');

            $scope.isAuthenticated = function () {
                return $rootScope.principal && $rootScope.principal.role;
            };

            $scope.menu = {};
            $scope.menu.pages = [
                {
                    "url": "/", "description": "Кабинет", "enabled": function () {
                    return true
                }
                },
                {
                    "url": "/history", "description": "Мои акции", "enabled": function () {
                    return true
                }
                },
                {
                    "url": "/createCoupon", "description": "Новая акция", "enabled": function () {
                    return true
                }
                },
                {
                    "url": "/purse", "description": "В кошельке", "enabled": function () {
                    return true
                }
                }
            ];

            $scope.menu.isPageSelected = function (page) {
                return ($scope.menu.currentPage == page);
            };

            $scope.menu.toggleSelectPage = function (page) {
                $scope.menu.currentPage = page;
            };


            $scope.getPageByUrl = function (url, pages) {
                for (var index in pages) {
                    if (pages[index].url == url) {
                        return pages[index];
                    }
                }



                return null;
            };

            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));

            $scope.toCabinet = function () {
                $location.url('/cabinet');
            };

            $scope.toSettings = function () {
                cabinetService.setIsSettingsActive(true);
                $location.url('/cabinet');
            };

            $scope.logout = function () {
                httpService.post('/sign.out', null,
                    function (response) {
                        window.location.href = '/';
                    });
            };

            $scope.$on('$mdMenuOpen', function () {
                angular.element('#dropMenuContent')
                    .parent().addClass('drop-menu-container');
            });


            $scope.isFlexContainer = function() {
                return !$rootScope.principal.role && $location.url() != '/login';
            };

            httpService.get('/prices/all', function(response) {
                $rootScope.prices = response;
            });

        }
    ]);