/**
 * Created by ioann on 22.07.16.
 */
'use strict';

angular.module('mainApp.yandexGeocoder', ['ngMaterial', 'yaMap'])
    .controller('yandexGeocoderController',
        function ($scope, $mdDialog, callback) {

            var geoObject = null;
            var _map;
            $scope.afterMapInit = function (map) {
                _map = map;
            };

            $scope.geoObjects = [];

            $scope.beforeInit = function () {
                var geolocation = ymaps.geolocation;
                geolocation.get({
                    provider: 'yandex',
                    mapStateAutoApply: true
                }).then(function (result) {
                    $scope.userGeoObjects.push({
                        geometry: {
                            type: 'Point',
                            coordinates: result.geoObjects.position
                        },
                        properties: {
                            balloonContent: 'Определено по IP'
                        }
                    });
                    $scope.center = result.geoObjects.position;
                    $scope.$digest();
                });

                geolocation.get({
                    provider: 'browser',
                    mapStateAutoApply: true
                }).then(function (result) {
                    // Синим цветом пометим положение, полученное через браузер.
                    // Если браузер не поддерживает эту функциональность, метка не будет добавлена на карту.
                    $scope.userGeoObjects.push({
                        geometry: {
                            type: 'Point',
                            coordinates: result.geoObjects.position
                        },
                        properties: {
                            balloonContent: 'Определено по данным браузера'
                        }
                    });
                    $scope.$digest();
                });
            };
            $scope.userGeoObjects = [];

            $scope.mapClick = function (e) {
                var coords = e.get('coords');

                // Отправим запрос на геокодирование.
                ymaps.geocode(coords).then(function (res) {
                    var names = [];
                    // Переберём все найденные результаты и
                    // запишем имена найденный объектов в массив names.
                    res.geoObjects.each(function (obj) {
                        var kind = obj.properties.get('metaDataProperty').GeocoderMetaData.kind;
                        if (kind != 'country' && kind != 'province') {
                            names.push(obj.properties.get('name'));
                        }
                    });
                    // Добавим на карту метку в точку, по координатам
                    // которой запрашивали обратное геокодирование.
                    var geoObj = {
                        geometry: {
                            type: 'Point',
                            coordinates: coords
                        },
                        properties: {
                            // В качестве контента иконки выведем
                            // первый найденный объект.
                            iconContent: names[0],
                            // А в качестве контента балуна - подробности:
                            // имена всех остальных найденных объектов.
                            balloonContent: names.reverse().join(', ')
                        }
                    };
                    geoObject = geoObj;
                    $scope.$apply(function () {
                        $scope.geoObjects = [geoObj];
                    });
                })
            };

            $scope.submitDisabled = function () {
                return geoObject == null;
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.submit = function () {
                callback(geoObject);
                $scope.cancel();
            }
        });