/**
 * Created by ioann on 04.06.16.
 */


'use strict';

angular.module('mainApp.cabinet', ['ngRoute', 'ngMaterial', 'ui.mask'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: '/resources/company/app/cabinet/cabinet.html',
            controller: 'cabinetController'
        });
    }])

    .controller('cabinetController', ['$rootScope', '$scope', '$location',
        'Upload', 'httpService', 'authService', 'dialogService', 'cabinetService', '$mdDialog',
        function ($rootScope, $scope, $location, Upload,
                  httpService, authService, dialogService, cabinetService, $mdDialog) {
            authService.secure('COMPANY');
            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));

            $scope.info = {};
            $scope.company = {};
            $scope.categories = [];

            if (cabinetService.getIsSettingsActive()) {
                $scope.selectedTabIndex = 1;
                cabinetService.setIsSettingsActive(false);
            } else {
                $scope.selectedTabIndex = 0;
            }


            httpService.get('/companies/current', function (response) {
                acceptActualData(response);
            });

            $scope.setActiveTabIndex = function(index) {
                $scope.selectedTabIndex = index;
            }

            $scope.inputWidthCorrection = function() {
            console.log('tada');
            }

            $scope.searchCategories = function (search) {

                var query;

                if (search && search.trim()) {
                    query = "?title=" + search
                        + "&limit=10";
                } else {
                    query = "";
                }

                httpService.get('/categories/all' + query,
                    function (response) {
                        $scope.categories = response;
                    });
            };

            $scope.updateAvatar = function (file, errFiles) {

                if (errFiles.length > 0) {

                    var errFile = errFiles[0];

                    dialogService.showError("Размер изображения не должен превышать " + errFile.$errorParam);

                    return;
                }

                uploadFileAndUpdateAvatar(file);
            };

            $scope.showGeocoder = function () {
                $mdDialog.show({
                    controller: 'yandexGeocoderController',
                    templateUrl: '/resources/company/app/dialog/yandex/yandexGeocoderDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    resolve: {
                        callback: function () {
                            return function (geoObject) {
                                $scope.company.address = geoObject.properties.balloonContent;
                                $scope.company.longitude = geoObject.geometry.coordinates[0];
                                $scope.company.latitude = geoObject.geometry.coordinates[1];
                            };
                        }
                    }
                });
            };

            
            function uploadFileAndUpdateAvatar(file) {
                if (file) {

                    file.upload = Upload.upload({
                        url: '/image.upload',
                        data: {image: file}
                    });

                    file.upload.then(function (response) {
                        if (response.status && response.status == "ERROR") {
                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            postUpdateAvatar(response.data.fileName);
                        }
                    }, function (response) {
                        dialogService.showError(response);
                    });
                }
            };

            function postUpdateAvatar(avatar) {
                httpService.post("/companies/updateAvatar",
                    {avatar: avatar}, function (response) {
                        $rootScope.principal.logo = response.avatar;
                    })
            };

            $scope.updateInfo = function () {
                var company = angular.copy($scope.company);

                if (company.contactPhoneNumber) {
                    company.contactPhoneNumber = "+7" + company.contactPhoneNumber;
                }

                httpService.post('/companies/edit', company,
                    function (response) {
                        acceptActualData(response);
                        $rootScope.principal.name = angular.copy(response.name);
                        $scope.selectedTabIndex = 0;
                    });
            };

            $scope.deleteAvatar = function () {
                httpService.post('/companies/deleteAvatar', {},
                    function (response) {
                        $rootScope.principal.logo = null;
                    });
            };


            function acceptActualData(data) {
                $scope.info = data;

                if (data.contactPhoneNumber) {
                    data.contactPhoneNumber = data.contactPhoneNumber.substring(2);
                }

                var siteUrlHref = data.siteUrl;

                if (siteUrlHref
                    && !siteUrlHref.toLowerCase().startsWith("https://")
                    && !siteUrlHref.toLowerCase().startsWith("http://")) {
                    siteUrlHref = "http://" + siteUrlHref;
                }

                $scope.info.siteUrlHref = siteUrlHref;
                $scope.company = angular.copy(data);
            };

        }]);
