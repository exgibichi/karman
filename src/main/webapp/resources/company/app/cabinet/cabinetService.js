/**
 * Created by ioann on 13.06.16.
 */

'use strict';

angular.module('mainApp').service('cabinetService', function() {

    var _isSettingsActive = false;

    this.setIsSettingsActive = function (isSettingsActive) {
        _isSettingsActive = isSettingsActive;
    };

    this.getIsSettingsActive = function() {
        return _isSettingsActive;
    };

});
