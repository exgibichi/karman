/**
 * Created by ioann on 04.06.16.
 */

'use strict';

angular.module('mainApp.passwordRecovery', ['ngRoute', 'ngMaterial', 'ui.mask'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/passwordRecovery', {
            templateUrl: '/resources/company/app/passwordRecovery/passwordRecovery.html',
            controller: 'passwordRecoveryController'
        });
    }])

    .controller('passwordRecoveryController', ['$scope', '$location',
        'httpService', 'dialogService', 'authService',
        function ($scope, $location, httpService, dialogService, authService) {
            authService.anonumousOnly();
            $scope.credentials = {};

            $scope.recoverPassword = function () {

                httpService.post("/password.recovery", $scope.credentials,
                    function (response) {
                        dialogService.showAlert('Пароль восстановлен',
                            'На ваш телефон было выслано сообщение с паролем');
                        $location.url('/login');
                    });
            };

            $scope.toLogin = function () {
                $location.url('/login');
            };

        }]);
