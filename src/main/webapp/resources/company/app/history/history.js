/**
 * Created by ioann on 04.06.16.
 */


'use strict';

angular.module('mainApp.history', ['ngRoute', 'ngMaterial', 'ngTable',
    'ngResource', 'chart.js', 'dibari.angular-ellipsis'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/history', {
            templateUrl: '/resources/company/app/history/history.html',
            controller: 'historyController'
        });
    }])

    .controller('historyController', ['$rootScope', '$scope', '$location',
        '$http', '$mdDialog', 'NgTableParams', 'httpService', 'dialogService', 'authService',
        function ($rootScope, $scope, $location, $http, $mdDialog, NgTableParams,
                  httpService, dialogService, authService) {

            authService.secure('COMPANY');
            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));

            $scope.coupons = [];

            $scope.tableParams = new NgTableParams({count: 20}, {counts: []});

            loadData().then(function (data) {
                $scope.coupons = data;
                $scope.tableParams.settings({
                    dataset: data,
                });
            });


            $scope.raise = function (coupon) {

                $mdDialog.show({
                    controller: RaisingController,
                    templateUrl: '/resources/company/app/dialog/raisingDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,

                    resolve: {
                        coupon: function () {
                            return coupon;
                        }
                    }
                });

            };

            $scope.fix = function (coupon) {

                $mdDialog.show({
                    controller: FixingController,
                    templateUrl: '/resources/company/app/dialog/fixingDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,

                    resolve: {
                        coupon: function () {
                            return coupon;
                        }
                    }
                });

            };

            $scope.forceAddToFavorites = function (coupon) {

                $mdDialog.show({
                    controller: ForceAddingToFavoritesController,
                    templateUrl: '/resources/company/app/dialog/toPocketsDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    resolve: {
                        coupon: function () {
                            return coupon;
                        }
                    }
                });
            };

            $scope.edit = function (id) {
                $location.url('/editCoupon/' + id);
            };


            $scope.extend = function (coupon) {
                $mdDialog.show({
                    controller: ExtendCouponDialogController,
                    templateUrl: '/resources/company/app/dialog/datepickerDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,

                    resolve: {
                        coupon: function () {
                            return [
                            coupon,
                            function () {
                                loadData().then(function (data) {
                                    $scope.coupons = data;
                                        $scope.tableParams.settings({
                                            dataset: data,
                                        });
                                   });
                            }
                            ];
                        }
                    }
                });
            };

            $scope.delete = function (coupon) {

                $mdDialog.show({
                    controller: DeleteCouponController,
                    templateUrl: '/resources/company/app/dialog/confirmDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,

                    resolve: {
                        callback: function () {
                            return function() {
                                httpService.post('/coupons/delete',
                                    {id: coupon.id},
                                    function (response) {
                                        loadData().then(function (data) {
                                            $scope.coupons = data;
                                            $scope.tableParams.settings({
                                                dataset: data,
                                            });
                                        });
                                    });
                            };
                        }
                    }
                });

            };

            function loadData() {

                return $http.get('/coupons/history')
                    .then(function (response) {
                        if (response.data &&
                            response.data.status &&
                            response.data.status == "ERROR") {

                            var message = null;
                            if (response.data.message) {
                                message = response.data.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            var coupons = response.data;

                            angular.forEach(coupons, function (value, key) {
                                var status = value.status;

                                if (status == "REVIEW") {
                                    value.status = "В модерации";
                                } else if (status == "REJECTED") {
                                    value.status = "Отклонено";
                                } else if (status == "ACTIVE") {
                                    value.status = "Активно";
                                } else if (status == "PAUSED") {
                                    value.status = "Пауза";
                                } else if (status == "FINISHED") {
                                    value.status = "Конец";
                                }

                                value.statusEn = status.toLowerCase();

                                var daysRemained = moment(value.dateTo).diff(moment(), 'days');

                                if (daysRemained < 0) {
                                    daysRemained = "-";
                                } else if (daysRemained == 0) {
                                    daysRemained = 'до 24:00';
                                    //daysRemained = 'до ' + moment(value.dateTo).format('hh:mm')
                                } else {
                                    daysRemained += (' ' + calcDayDeclension(daysRemained));
                                }

                                value.daysRemained = daysRemained;
                                value.dateToFormatted = moment(value.dateTo).format('DD.MM.YYYY');

                                value.editingEnabled = (value.statusEn == 'rejected')
                                    || (value.hasShadow && value.rejectionReason)
                                    || ((value.statusEn == 'active' || value.statusEn == 'finished') && !value.hasShadow);

                                value.raisingEnabled = (value.statusEn == 'active');

                                value.fixingEnabled = (!value.fixingDate && value.statusEn == 'active');

                                value.forceAddingToFavoritesEnabled = (value.statusEn == 'active');

                                value.extendingEnabled = (value.statusEn == 'finished');


                                value.charts = {
                                    data: [value.chartsData[0].yCoords, value.chartsData[1].yCoords],
                                    labels: value.chartsData[0].xLabels,
                                    series: ['Добавления в карман', 'Просмотры'],
                                    colours: ['#46BFBD', '#f7464a'],
                                    options: {
                                        scaleGridLineWidth: 2,
                                        scaleGridLineColor: "rgba(47, 42, 47, 0.5);"
                                    }
                                };
                            });

                            return response.data;
                        }
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    })
            }


            function updateBalance() {
                httpService.get('/companies/balance',
                    function (response) {
                        $rootScope.principal.balance = response;
                    });
            };

            function ExtendCouponDialogController($scope, $mdDialog, coupon) {
                var callback = coupon[1];
                coupon = coupon[0];
                var submitDisabled = true;

                $scope.backgroundColorPrice = {
                    price: 0
                };

                $scope.submitDisabled = function () {

                    if (submitDisabled || !$scope.date) {
                        return true;
                    }

                    return false;
                };

                $scope.dateFilter = function (dateTo) {
                    return moment(dateTo) - moment().subtract(1, "days") > 0;
                };

                $scope.calcBackgroundPrice = function () {

                    if (!$scope.date) {
                        $scope.backgroundColorPrice = {
                            price: 0
                        };
                        return;
                    }

                    submitDisabled = true;
                    httpService.post('/prices/extending',
                        {
                            id: coupon.id,
                            dateTo: moment($scope.date).format('YYYY-MM-DDThh:mm')
                        },
                        function (response) {
                            if (response) {
                                $scope.backgroundColorPrice = response;
                            } else {
                                $scope.backgroundColorPrice = {
                                    price: 0
                                }
                            }
                        }, null, function () {
                            submitDisabled = false;
                        });
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.submit = function () {

                    $scope.submitDisabled = true;

                    httpService.post(
                        "/coupons/extend",
                        {
                            id: coupon.id,
                            dateTo: moment($scope.date).format('YYYY-MM-DDThh:mm')
                        },
                        function (response) {
                            dialogService.showAlert('Публикация акции продлена до даты: "'
                                + moment($scope.date).format('YYYY-MM-DD') + '"');
                            coupon.dateTo = $scope.date;
                            coupon.status = 'Активно';
                            coupon.statusEn = 'ACTIVE'.toLowerCase();

                            var daysRemained = moment($scope.date).diff(moment(), 'days');

                            if (daysRemained < 0) {
                                daysRemained = "-";
                            } else {
                                daysRemained += ' дней';
                            }

                            coupon.daysRemained = daysRemained;
                            callback();
                            $scope.cancel();
                        },
                        function (errorResponse) {
                            dialogService.showError(errorResponse);
                            $scope.submitDisabled = false;
                        },
                        function() {
                            updateBalance();
                        }
                    );
                };


                $scope.calcDayDeclension = function (days) {
                    return calcDayDeclension(days);
                };

            };

            function ForceAddingToFavoritesController($rootScope, $scope, $mdDialog, coupon) {
               $scope.regions = [];
               $scope.region = {id:{id: 0, title:"Все регионы"}};

                            $scope.searchRegions = function (search) {
                                var query;

                                if (search && search.trim()) {
                                    query = "?title=" + search
                                        + "&limit=10";
                                } else {
                                    return;
                                }

                                httpService.get('/regions/all' + query,
                                    function (response) {
                                        $scope.regions = response;
                                    });
                            };
                $scope.count = 0;

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                function calcPocketsDeclension(pockets) {
                    var pocketsString;
                    if (pockets % 100 > 10 && pockets % 100 < 20)
                        pocketsString = " карманов";
                    else if (pockets % 10 == 1)
                        pocketsString = " карман";
                    else if (pockets % 10 > 1 && pockets % 10 < 5)
                        pocketsString = " кармана";
                    else
                        pocketsString = " карманов";

                    return pocketsString;
                };

                $scope.onCountChange = function () {
                    if (!$scope.count || $scope.count < 1) {
                        $scope.count = 0;
                    }
                    ;
                };
                $scope.loading = false;
                $scope.start = false;
                $scope.submit = function () {
                    $scope.loading = true;
                    $scope.start = true;
                    httpService.post('/coupons/forceAddToFavorites',
                        {
                            id: coupon.id,
                            region_id: $scope.region.id.id,
                            count: $scope.count
                        },
                        function (response) {
                            $scope.start = false;
                            $scope.loading = false;
                            dialogService.showAlert('Акция добавлена в '
                                + response.count + ' ' + calcPocketsDeclension(response.count) + ' за '
                                + $rootScope.prices.forceAddingToFavoritesPerCustomerPrice * response.count + ' руб.');
                        },
                        null,
                        updateBalance
                    );
                };
            };


            function FixingController($rootScope, $scope, $mdDialog, coupon) {
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.submit = function () {
                    httpService.post('/coupons/fix',
                        {
                            id: coupon.id
                        },
                        function (response) {
                            dialogService.showAlert('Акция успешно закреплена в топе');
                            coupon.fixingEnabled = false;
                        },
                        null,
                        updateBalance
                    );
                };
            }


            function RaisingController($rootScope, $scope, $mdDialog, coupon) {
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.submit = function () {
                    httpService.post('/coupons/raise',
                        {
                            id: coupon.id
                        },
                        function (response) {
                            dialogService.showAlert('Акция успешно поднята в топ');
                        },
                        null,
                        updateBalance
                    );
                };
            };


            function DeleteCouponController($scope, $mdDialog, callback) {
                function cancel() {
                    $mdDialog.cancel();
                }

                $scope.title = "Удалить";
                $scope.text = "Вы действительно хотите удалить акцию?";
                $scope.confirmBtnText = "Да";
                $scope.cancelBtnText = "Отмена";

                $scope.callback = function (result) {
                    if (!result) {
                        cancel();
                        return;
                    }

                    callback();
                    cancel();
                };
            };

            function calcDayDeclension(days) {
                var daysString;
                if (days % 100 > 10 && days % 100 < 20)
                    daysString = " дней";
                else if (days % 10 == 1)
                    daysString = " день";
                else if (days % 10 > 1 && days % 10 < 5)
                    daysString = " дня";
                else
                    daysString = " дней";

                return daysString;
            };
        }]);