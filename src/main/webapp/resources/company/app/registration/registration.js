/**
 * Created by ioann on 03.06.16.
 */

'use strict';

angular.module('mainApp.registration', ['ngRoute', 'ngAnimate', 'ngMaterial', 'ui.mask'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/registration', {
            templateUrl: '/resources/company/app/registration/registration.html',
            controller: 'registrationController'
        });
    }])

    .controller('registrationController', ['$scope', '$location',
        'httpService', 'dialogService', 'authService',
        function ($scope, $location, httpService, dialogService, authService) {
            authService.anonumousOnly();

            $scope.credentials = {};
            $scope.credentials.role = "COMPANY";
            $scope.isCompanyToggled = function () {
                return $scope.credentials.role == "COMPANY";
            };

            $scope.toggleRole = function() {
                if ($scope.credentials.role == "COMPANY") {
                    $scope.credentials.role = "CUSTOMER";
                } else {
                    $scope.credentials.role = "COMPANY";
                }
            };

            $scope.signUp = function() {
                httpService.post("/sign.up", $scope.credentials,
                    function(response) {
                        dialogService.showAlert('Регистрация прошла успешно',
                            'На ваш телефон было выслано сообщение с паролем');
                        $location.url("/login");
                    });
            };

            $scope.namePlaceholder = function () {
                if ($scope.credentials.role = "COMPANY") {
                    return "";
                } else {
                    return "Имя"
                }
            };

        }]);