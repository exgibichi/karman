/**
 * Created by ioann on 20.06.16.
 */

'use strict';

angular.module('mainApp').service('paymentResultService',
    ['$cookies', '$mdDialog', 'httpService',
        function ($cookies, $mdDialog, httpService) {

            if ($cookies.get("SUCCESS_ROBOKASSA_PAYMENT")) {
                httpService.get('/paymentHistory/' + $cookies.get("SUCCESS_ROBOKASSA_PAYMENT"),
                    function (payment) {
                        showPaymentDialog('/resources/company/app/dialog/successPaymentDialog.html',
                            payment,
                            SuccessPaymentController,
                            false
                        );
                    });
                $cookies.remove("SUCCESS_ROBOKASSA_PAYMENT");
            } else if ($cookies.get("FAIL_ROBOKASSA_PAYMENT")) {
                httpService.get('/paymentHistory/' + $cookies.get("FAIL_ROBOKASSA_PAYMENT"),
                    function (payment) {
                        showPaymentDialog('/resources/company/app/dialog/failPaymentDialog.html',
                            payment,
                            FailPaymentController,
                            true
                        );
                    });
                $cookies.remove("FAIL_ROBOKASSA_PAYMENT");
            }


            function showPaymentDialog(templateUrl, payment, controller, clickOutsideToClose) {
                $mdDialog.show({
                    controller: controller,
                    templateUrl: templateUrl,
                    parent: angular.element(document.body),
                    clickOutsideToClose: clickOutsideToClose,
                    resolve: {
                        payment: function () {
                            return payment;
                        }
                    }
                });
            }


            function SuccessPaymentController($scope, payment) {
                $scope.payment = payment;

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
            }


            function FailPaymentController($scope, payment) {
                $scope.payment = payment;
            }

        }
    ]);