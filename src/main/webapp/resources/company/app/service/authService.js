/**
 * Created by ioann on 03.06.16.
 */

'use strict';

angular.module('mainApp').service('authService',
    ['$rootScope', '$location', 'httpService', 'dialogService',
        function ($rootScope, $location, httpService, dialogService) {

            this.secure = function (role) {

                if (!$rootScope.principal) {
                    $rootScope.principal = {};
                }

                httpService.get('/user/principal', function (response) {
                    if (!response) {
                        $rootScope.principal = {};
                        var url = $location.$$url;

                        if (url != '/login' && url != '/registration' && url != '/passwordRecovery') {
                            $location.url('/login');
                        }
                    } else {

                        if (response.role != role) {

                            dialogService.showAlert('Доступ разрешен только представителям компаний');

                            httpService.post('/sign.out', null,
                                function (response) {
                                });

                            return;
                        }

                        var principal = {
                            name: response.name,
                            role: response.role,
                        };

                        if (response.role == 'COMPANY') {
                            principal.balance = response.balance;

                            if (response.logo) {
                                principal.logo = response.logo;
                            }

                            principal.isActive = response.isActive;
                            principal.moderatorsComment = response.moderatorsComment;
                            principal.inModeration = response.inModeration;
                        }

                        $rootScope.principal = principal;

                        var url = $location.$$url;
                        if (url == '/login' || url == '/registration' || url == '/passwordRecovery') {
                            $location.url('/');
                        }
                    }
                });
            };

            this.anonumousOnly = function () {
                if ($rootScope.principal && $rootScope.principal.role) {
                    $location.url('/');
                }
            };
        }]);