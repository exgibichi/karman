/**
 * Created by ioann on 11.07.16.
 */
'use strict';


angular.module('mainApp.couponStats', ['ngMaterial', 'chart.js'])

    .config(['$routeProvider',
        function ($routeProvider) {

            $routeProvider
                .when('/couponStats/:id', {
                    templateUrl: '/resources/company/app/couponStats/couponStats.html',
                    controller: 'couponStatsController'
                });
        }
    ])

    .controller('couponStatsController', ['$scope', '$location',
        '$routeParams', 'authService', 'httpService', 'dialogService',
        function ($scope, $location, $routeParams, authService,
                  httpService, dialogService) {
            authService.secure('COMPANY');

            $scope.menu.toggleSelectPage(
                $scope.getPageByUrl($location.$$url, $scope.menu.pages));

            var id = $routeParams.id;


            $scope.favoriteData =  {
                data: new Array(1),
                labels: [],
                series : ['Добавления в карман'],
                options : {
                    pointBorderWidth: 5
                }
            };
            $scope.reviewData = {
                data: new Array(1),
                labels: [],
                series : ['Просмотры']
            };

            fetchInitialData();

            function fetchInitialData() {
                httpService.get('/coupons/stats/favorite/lastWeek/' + id,
                    function (response) {
                        $scope.favoriteData.labels = response.xLabels;
                        $scope.favoriteData.data[0] = response.yCoords;
                    },
                    function (errorResponse) {
                        dialogService.showError(errorResponse);
                        $location.url('/history');
                    });

                httpService.get('/coupons/stats/review/lastWeek/' + id,
                    function (response) {
                        $scope.reviewData.labels = response.xLabels;
                        $scope.reviewData.data[0] = response.yCoords;
                    },
                    function (errorResponse) {
                        dialogService.showError(errorResponse);
                        $location.url('/history');
                    });
            }

        }]);