/**
 * Created by ioann on 31.05.16.
 */

'use strict';

angular.module('mainApp.hello', ['ngMaterial'])

    .controller('helloController', ['$rootScope' ,'$scope', '$state', 'httpService',
        '$location',
        function ($rootScope, $scope, $state, httpService, $location) {

                $scope.logout = function () {
                        httpService.post('/sign.out', null,
                        function (response) {
                                $rootScope.principal = {};
                                $state.go('login');
                        });
                };

                $scope.toCabinet = function () {
                        window.location.href = '/company/';
                };

                $scope.toSettings = function () {
                        window.location.href = '/company/#/settings';
                };

                $scope.toCompanyApp = function() {
                        window.location.href = '/company/';
                };

                $scope.logoBackground = function() {
                        if ($rootScope.principal && $rootScope.principal.logo) {
                                return "url('/images/" + $rootScope.principal.logo + "')";
                        } else {
                                return '';
                        }
                };

        }]);