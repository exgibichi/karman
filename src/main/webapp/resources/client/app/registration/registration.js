/**
 * Created by ioann on 01.06.16.
 */
'use strict';

angular.module('mainApp.registration', ['ngMaterial', 'ui.mask'])

    .controller('registrationController', ['$rootScope', '$scope', '$location',
        '$state', 'httpService', 'dialogService',
        function ($rootScope, $scope, $location, $state, httpService, dialogService) {

                $scope.credentials = {};
                $scope.credentials.role = "CUSTOMER";

                $scope.isCompanyToggled = function() {
                        return $scope.credentials.role == "COMPANY";
                };

                $scope.toggleRole = function() {
                        if ($scope.credentials.role == "COMPANY") {
                                $scope.credentials.role = "CUSTOMER";
                        } else {
                                $scope.credentials.role = "COMPANY";
                        }
                };

                $scope.signUp = function() {

                        httpService.post("/sign.up", $scope.credentials,
                        function(response) {
                                dialogService.showAlert('Регистрация прошла успешно',
                                'На ваш телефон было выслано сообщение с паролем');
                                $state.go("login");
                        });

                };

        }]);