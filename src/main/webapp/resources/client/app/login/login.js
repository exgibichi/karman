/**
 * Created by ioann on 31.05.16.
 */

'use strict';

angular.module('mainApp.login', ['ngMaterial', 'ui.mask'])

    .controller('loginController', ['$rootScope', '$scope', '$location',
        '$state', 'httpService',
        function ($rootScope, $scope, $location, $state, httpService) {

            $scope.credentials = {};

            $scope.signIn = function () {
                httpService.post('/sign.in',
                    {
                        phoneNumber: $scope.credentials.login,
                        password: $scope.credentials.password
                    }, function (response) {

                        if (response.role == "COMPANY") {
                            window.location.href = "/company/";
                        }

                        $rootScope.principal.name = response.name;
                        $rootScope.principal.phoneNumber = response.phoneNumber;
                        $rootScope.principal.role = response.role;
                        console.log(response);
                        $state.go("hello");
                    });
            };

            $scope.toRegistration = function() {
                $state.go('registration');
            };

            $scope.toPasswordRecovery = function() {
                $state.go('passwordRecovery');
            };
        }]);