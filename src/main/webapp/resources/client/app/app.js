/**
 * Created by ioann on 30.05.16.
 */

'use strict';

angular.module('mainApp', [
    'ngRoute',
    'ngMaterial',
    'ngMdIcons',
    'ui.router',
    'ngSanitize',
    'ui.select',
    'mainApp.registration',
    'mainApp.login',
    'mainApp.hello',
    'mainApp.passwordRecovery'
])

    .config(['$stateProvider',
        function ($stateProvider) {
            var registrationView = "/resources/client/app/registration/registration.html";
            var loginView = "/resources/client/app/login/login.html";
            var authenticatedView = "/resources/client/app/hello/hello.html";
            var passwordRecoveryView = "/resources/client/app/passwordRecovery/passwordRecovery.html";
            var commonCouponsView = "/resources/client/app/coupons/coupons.html";
            var createCouponView = "/resources/client/app/createCoupon/createCoupon.html";

            $stateProvider
            //Страница регистрации
                .state('registration', {
                    url: "/registration",
                    views: {
                        "menuView": {
                            templateUrl: registrationView
                        },
                        "contentView": {
                            templateUrl: commonCouponsView
                        }
                    }
                })
                //Страница логина
                .state('login', {
                    url: "/login",
                    views: {
                        "menuView": {
                            templateUrl: loginView
                        },
                        "contentView": {
                            templateUrl: commonCouponsView
                        }
                    }
                })
                //Страница регистрации
                .state('passwordRecovery', {
                    url: "/passwordRecovery",
                    views: {
                        "menuView": {
                            templateUrl: passwordRecoveryView
                        },
                        "contentView": {
                            templateUrl: commonCouponsView
                        }
                    }
                })
                //Пользователь аутентифицирован
                .state('hello', {
                    url: "",
                    views: {
                        "menuView": {
                            templateUrl: authenticatedView
                        },
                        "contentView": {
                            templateUrl: commonCouponsView
                        }
                    }
                })
                //Создание акции
                .state('createCoupon', {
                    url: "/createCoupon",
                    views: {
                        "menuView": {
                            templateUrl: authenticatedView
                        },
                        "contentView": {
                            templateUrl: createCouponView
                        }
                    }
                });
        }])

    .controller("mainController", ["$rootScope", "$scope", "$state", "$location", "httpService",
        function ($rootScope, $scope, $state, $location, httpService) {

            $rootScope.principal = {};

            httpService.get('/user/principal', function (response) {
                if (!response) {
                    var url = $location.$$url;

                    if (url != '/login' && url != '/registration' && url != '/passwordRecovery') {
                        $state.go('login');
                    }
                } else {
                    $rootScope.principal = response;
                }
            });

            $scope.regions = [];
            $scope.categories = [];

            $scope.region = null;
            $scope.category = null;

            $scope.searchRegions = function (search) {
                var query;

                if (search && search.trim()) {
                    query = "?title=" + search
                        + "&limit=10";
                } else {
                    query = "?isParent=true";
                }

                httpService.get('/regions/all' + query,
                    function (response) {
                        $scope.regions = [{id: null, title: 'Все регионы'}];
                        angular.forEach(response, function (value) {
                            $scope.regions.push(value);
                        });
                    });
            };


            $scope.searchCategories = function (search) {
                var query;

                if (search && search.trim()) {
                    query = "?title=" + search
                        + "&limit=10";
                } else {
                    query = "";
                }

                httpService.get('/categories/all' + query,
                    function (response) {
                        $scope.categories = [{id: null, title: 'Все категории'}];
                        angular.forEach(response, function (value) {
                            $scope.categories.push(value);
                        });
                    });
            };


            $scope.onRegionSelect = function ($item) {
                $scope.region = $item;
                $scope.fetchCoupons();
            };

            $scope.onCategorySelect = function ($item) {
                $scope.category = $item;
                $scope.fetchCoupons();
            };

            $scope.fetchCoupons = function () {
                var region = $scope.region;
                var category = $scope.category;

                var querySuffix = '';

                if (region && region.id) {
                    querySuffix += ('&regionId=' + region.id);
                }
                if (category && category.id) {
                    querySuffix += ('&categoryId=' + category.id);
                }

                httpService.get('/coupons/getAll?skip=0&limit=12' + querySuffix,
                    function (response) {
                        $scope.coupons = response;
                        console.log(response);
                    }
                );

            };

            $scope.daysRemained = function (dateTo) {
                var daysRemained = moment(dateTo).diff(moment(), 'days');

                if (daysRemained < 0) {
                    return "-";
                } else if (daysRemained == 0) {
                    return 'До ' + moment(dateTo).format('hh:mm')
                } else {
                   return 'Осталось: ' + daysRemained + ' ' + calcDayDeclension(daysRemained);
                }
            };

            $scope.fetchCoupons();

            $scope.toCompanyApp = function () {
                window.location.href = "/company/";
            };


            function hexToRgb(hex) {
                // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                    return r + r + g + g + b + b;
                });

                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                } : null;
            }


            $scope.calcFontColor = function(backgroundHex) {
                var backGroundRgb = hexToRgb(backgroundHex);

                var d = 0;

                // Counting the perceptive luminance - human eye favors green color...
                var a = 1 - ( 0.299 * backGroundRgb.r
                    + 0.587 * backGroundRgb.g + 0.114 * backGroundRgb.b) / 255;

                if (a < 0.3)
                    d = 0; // bright colors - black font
                else
                    d = 255; // dark colors - white font

                return  'rgb(' + d + ',' + d + ',' + d + ')';
            }


            function calcDayDeclension(days) {
                var daysString;
                if (days % 100 > 10 && days % 100 < 20)
                    daysString = " дней";
                else if (days % 10 == 1)
                    daysString = " день";
                else if (days % 10 > 1 && days % 10 < 5)
                    daysString = " дня";
                else
                    daysString = " дней";

                return daysString;
            };
        }]);