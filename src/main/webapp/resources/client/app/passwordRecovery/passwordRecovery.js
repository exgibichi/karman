/**
 * Created by ioann on 01.06.16.
 */

'use strict';

angular.module('mainApp.passwordRecovery', ['ngMaterial', 'ui.mask'])

    .controller('passwordRecoveryController', ['$rootScope', '$scope', '$location',
        '$state', 'httpService', 'dialogService',
        function ($rootScope, $scope, $location, $state, httpService, dialogService) {

                $scope.credentials = {};

                $scope.recoverPassword = function() {

                        httpService.post("/password.recovery", $scope.credentials,
                            function(response) {
                                    dialogService.showAlert('Пароль восстановлен',
                                        'На ваш телефон было выслано сообщение с паролем');
                                    $state.go("login");
                            });

                };

        }]);