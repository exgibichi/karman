/**
 * Created by ioann on 30.05.16.
 */

'use strict';

angular.module('mainApp.categories', ['ngRoute', 'ngMaterial', 'ngTable'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/categories', {
            templateUrl: '/resources/admin/app/categories/categories.html',
            controller: 'categoriesController'
        });
    }])

    .controller('categoriesController', ['$scope', '$mdDialog', 'httpService', 'dialogService', 'NgTableParams', '$http',
        function ($scope, $mdDialog, httpService, dialogService, NgTableParams, $http) {

            $scope.tableParams = new NgTableParams({}, {
                getData: function() {
                    return $http.get('/categories/all')
                        .then(function(response) {
                        if (response.data &&
                            response.data.status &&
                            response.data.status == "ERROR") {

                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            return response.data;
                        }
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    })
                }
            });


             $scope.update = function (ev, category) {
                 $mdDialog.show({
                     controller: UpdateDialogController,
                     templateUrl: 'resources/admin/app/dialog/updateCategoryDialog.tmpl.html',
                     parent: angular.element(document.body),
                     targetEvent: ev,
                     clickOutsideToClose: true,
                     resolve: {
                         category: function() {
                             return category;
                         },
                         tableParams: function () {
                             return $scope.tableParams;
                         }
                     }
                 });
             };

            $scope.delete = function (id) {
                var confirmDeleting = $mdDialog.confirm()
                    .textContent('Вы действительно хотите удалить эту категорию?')
                    .ok('Удалить')
                    .cancel('Отмена');
                $mdDialog.show(confirmDeleting).then(function (result) {
                    if (!result) {
                        return;
                    }
                    httpService.post('/admin/categories/delete',
                        {
                            id: id,
                        }, function () {
                            $scope.tableParams.reload();
                        });
                });
            };

            $scope.showDialog = function (ev) {

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'resources/admin/app/dialog/newCategoryDialog.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    resolve: {
                        tableParams: function () {
                            return $scope.tableParams;
                        }
                    }
                });
            };

            function DialogController($scope, $mdDialog, tableParams) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.correctData = function () {
                    var category = $scope.category;
                    return category && category.title;
                };

                $scope.submit = function () {
                    httpService.post(
                        "admin/categories/save",
                        $scope.category,
                        function (response) {
                            var category = response;
                            tableParams.reload();
                            dialogService.showAlert('Создана новая категория "' + category.title + '"');
                            $scope.cancel();
                        }
                    );
                };

            }

            function UpdateDialogController($scope, $mdDialog, category) {
                $scope.category = category;
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.correctData = function () {
                    var category = $scope.category;
                    return category && category.title;
                };

                $scope.submit = function () {
                    httpService.post(
                        "admin/categories/update",
                        $scope.category,
                        function (response) {
                            dialogService.showAlert('Категория успешно отредактирована');
                            $scope.cancel();
                        }
                    );
                };

            }

        }]);