/**
 * Created by ioann on 28.06.16.
 */

'use strict';

angular.module('mainApp.newCompanies', ['ngRoute', 'ngMaterial', 'ngTable'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/companies/new', {
            templateUrl: '/resources/admin/app/newCompanies/newCompanies.html',
            controller: 'newCompaniesController'
        });
    }])

    .controller('newCompaniesController', ['$scope', '$http', '$mdDialog', 'NgTableParams',
        'httpService', 'dialogService',
        function ($scope, $http, $mdDialog, NgTableParams, httpService, dialogService) {

            $scope.newCompanies = [];

            $scope.tableParams = new NgTableParams({}, {
                getData: function () {

                    return $http.get('/admin/companies/allReview')
                        .then(function (response) {
                            if (response.data &&
                                response.data.status &&
                                response.data.status == "ERROR") {

                                var message = null;
                                if (response.message) {
                                    message = response.message;
                                } else {
                                    message = "Произошла ошибка"
                                }
                                dialogService.showError(message);
                            } else {
                                return response.data;
                            }
                        }, function (errorResponse) {
                            dialogService.showError(errorResponse);
                        })
                }
            });

            $scope.reject = function (company) {
                var confirmRejection = $mdDialog.prompt()
                    .title('Отказ')
                    .textContent('Введите причину отказа')
                    .placeholder('Причина отказа')
                    .ok('Отказать')
                    .cancel('Отмена');

                $mdDialog.show(confirmRejection).then(function (result) {
                    httpService.post('/admin/companies/rejectModeration',
                        {
                            id: company.id,
                            rejectionReason: result
                        }, function () {
                            $scope.tableParams.reload();
                        });
                });

            };

            $scope.confirm = function(company) {
                httpService.post('/admin/companies/confirmModeration',
                    {
                        id: company.id,
                    }, function () {
                        $scope.tableParams.reload();
                    });
            };
        }]);
