/**
 * Created by ioann on 30.05.16.
 */

'use strict';

angular.module('mainApp.regions', ['ngRoute', 'ngMaterial', 'ngTable', 'ui.select'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/regions', {
            templateUrl: '/resources/admin/app/regions/regions.html',
            controller: 'regionsController'
        });
    }])

    .controller('regionsController', ['$scope', '$mdDialog', 'httpService', 'dialogService', 'NgTableParams', '$http',
        function ($scope, $mdDialog, httpService, dialogService, NgTableParams, $http) {

            $scope.tableParams = new NgTableParams({}, {
                getData: function() {
                    return $http.get('/regions/all')
                        .then(function(response) {
                        if (response.data &&
                            response.data.status &&
                            response.data.status == "ERROR") {

                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            return response.data;
                        }
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    })
                }
            });

            $scope.delete = function (id) {
                var confirmDeleting = $mdDialog.confirm()
                    .textContent('Вы действительно хотите удалить этот город/регион?')
                    .ok('Удалить')
                    .cancel('Отмена');
                $mdDialog.show(confirmDeleting).then(function (result) {
                    if (!result) {
                        return;
                    }
                    httpService.post('/admin/regions/delete',
                        {
                            id: id,
                        }, function () {
                            $scope.tableParams.reload();
                        });
                });
            };

            $scope.newRegion = function (ev) {
                $mdDialog.show({
                    controller: RegionDialogController,
                    templateUrl: 'resources/admin/app/dialog/newRegionDialog.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    resolve: {
                        regions: function() {
                            return $scope.regions;
                        },
                        tableParams: function () {
                            return $scope.tableParams;
                        }
                    }
                });
            };

            $scope.newCity = function (ev) {
                $mdDialog.show({
                    controller: CityDialogController,
                    templateUrl: 'resources/admin/app/dialog/newCityDialog.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    resolve: {
                           region: function() {
                               return $scope.region;
                           },
                           cities: function() {
                               return $scope.cities;
                           },
                           tableParams: function () {
                               return $scope.tableParams;
                           }
                    }
                });
            };

            function RegionDialogController($scope, $mdDialog, regions, tableParams) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.correctData = function () {
                    var region = $scope.region;
                    return region && region.title;
                };

                $scope.submit = function () {
                    $scope.region.parentId = null;
                    httpService.post(
                        "admin/regions/save",
                        $scope.region,
                        function (response) {
                              var region = response;
                              tableParams.reload();
                              dialogService.showAlert('Создан новый регион "' + region.title + '"');
                              $scope.cancel();
                        }
                    );
                };
            }

            function CityDialogController($scope, $mdDialog, region, cities, tableParams, httpService) {
                $scope.regions = [];
                $scope.city = {};
                            $scope.searchRegions = function (search) {
                                var query;

                                if (search && search.trim()) {
                                    query = "?title=" + search
                                        + "&limit=10";
                                } else {
                                    return;
                                }

                                httpService.get('/regions/all' + query,
                                    function (response) {
                                        $scope.regions = response;
                                    });
                            };
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.correctData = function () {
                    var city = $scope.city;
                    var region = $scope.city.region;
                    return city && city.title && region && region.id;
                };
                $scope.submit = function () {
                    $scope.city.parentId = $scope.city.region.id;
                    httpService.post(
                        "admin/regions/save",
                        $scope.city,
                        function (response) {
                              var city = response;
                              tableParams.reload();
                              dialogService.showAlert('Создан новый регион "' + city.title + '"');
                              $scope.cancel();
                        }
                    );
                };
            }
        }]);