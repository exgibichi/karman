'use strict';

angular.module('mainApp.prices', ['ngRoute', 'ngMaterial'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/prices', {
            templateUrl: '/resources/admin/app/prices/prices.html',
            controller: 'pricesController'
        });
    }])

    .controller('pricesController', ['$scope', '$mdDialog', 'httpService', 'dialogService',
        function ($scope, $mdDialog, httpService, dialogService) {

            $scope.prices = {};

            fetchCosts();

            $scope.edit = function(type, price) {
                httpService.post('/admin/prices/edit',
                    {
                        type: type,
                        price: price
                    }, function (response) {
                        dialogService.showAlert('Успешно', 'Цена обновлена');
                    }
                );
            };
            
            function fetchCosts() {
                httpService.get('/prices/all', function (response) {
                    $scope.prices = response;
                });
            };

        }]);