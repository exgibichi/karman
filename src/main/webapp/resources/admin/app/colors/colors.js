/**
 * Created by ioann on 08.07.16.
 */

'use strict';

angular.module('mainApp.colors', ['ngRoute', 'ngMaterial',
    'color.picker', 'ngTable'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/colors', {
            templateUrl: '/resources/admin/app/colors/colors.html',
            controller: 'colorsController'
        });
    }])

    .controller('colorsController', ['$scope', '$mdDialog', '$http',
        'NgTableParams', 'httpService', 'dialogService',
        function ($scope, $mdDialog, $http, NgTableParams,
                  httpService, dialogService) {

            $scope.tableParams = new NgTableParams({}, {
                getData: function() {

                    return $http.get('/colors/all')
                        .then(function(response) {
                        if (response.data &&
                            response.data.status &&
                            response.data.status == "ERROR") {

                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            return response.data;
                        }
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    })
                }
            });

            
            $scope.delete = function (id) {
                var confirmDeleting = $mdDialog.confirm()
                    .textContent('Вы действительно хотите удалить этот цвет?')
                    .ok('Удалить')
                    .cancel('Отмена');

                $mdDialog.show(confirmDeleting).then(function (result) {

                    if (!result) {
                        return;
                    }

                    httpService.post('/admin/colors/delete',
                        {
                            id: id,
                        }, function () {
                            $scope.tableParams.reload();
                        });
                });
            };
            
            $scope.showDialog = function () {

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: '/resources/admin/app/dialog/newColorDialog.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    resolve: {
                        colors: function() {
                            return $scope.colors;
                        },
                        tableParams: function () {
                            return $scope.tableParams;
                        }
                    }
                });
            };


            function DialogController($scope, $mdDialog, colors, tableParams) {

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.correctData = function () {
                    var category = $scope.category;
                    return category && category.title;
                };

                $scope.submitDisabled = false;

                $scope.submit = function () {

                    $scope.submitDisabled = true;

                    httpService.post(
                        "admin/colors/save",
                        $scope.color,
                        function (response) {
                            var color = response;
                            tableParams.reload();
                            dialogService.showAlert('Создан новый цвет "' + color.colorHex + '"');
                            $scope.cancel();
                        }
                    );
                };

            }

        }]);
