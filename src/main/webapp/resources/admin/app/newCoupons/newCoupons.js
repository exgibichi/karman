/**
 * Created by ioann on 03.06.16.
 */

'use strict';

angular.module('mainApp.newCoupons', ['ngRoute', 'ngMaterial', 'ngTable'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/coupons/new', {
            templateUrl: '/resources/admin/app/newCoupons/newCoupons.html',
            controller: 'newCouponsController'
        });
    }])

    .controller('newCouponsController', ['$scope', '$http', '$mdDialog', 'NgTableParams',
        'httpService', 'dialogService',
        function ($scope, $http, $mdDialog, NgTableParams, httpService, dialogService) {

            $scope.newCoupons = [];


            $scope.tableParams = new NgTableParams({}, {
                getData: function() {

                    return $http.post('/admin/coupons/all',
                        {
                            status: "REVIEW"
                        }
                    ).then(function(response) {
                        if (response.data &&
                            response.data.status &&
                            response.data.status == "ERROR") {

                            var message = null;
                            if (response.message) {
                                message = response.message;
                            } else {
                                message = "Произошла ошибка"
                            }
                            dialogService.showError(message);
                        } else {
                            return response.data;
                        }
                    }, function (errorResponse) {
                        dialogService.showError(errorResponse);
                    })
                }
            });


            $scope.reject = function (coupon) {
                var confirmRejection = $mdDialog.prompt()
                    .title('Отклонение акции')
                    .textContent('Введите причину отклонения')
                    .placeholder('Причина отклонения')
                    .ok('Отклонить')
                    .cancel('Отмена');

                $mdDialog.show(confirmRejection).then(function (result) {
                    httpService.post('/admin/coupons/reject',
                        {
                            id: coupon.id,
                            rejectionReason: result
                        }, function () {
                            $scope.tableParams.reload();
                        });
                });

            };

            $scope.confirm = function(coupon) {
                httpService.post('/admin/coupons/confirm',
                    {
                        id: coupon.id,
                    }, function () {
                        $scope.tableParams.reload();
                    });
            };

            $scope.showCouponDialog = function (coupon) {

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: '/resources/admin/app/dialog/couponViewDialog.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    resolve: {
                        coupon: function () {
                            return coupon;
                        }
                    }
                })
            };

            function DialogController($scope, httpService, coupon) {

                $scope.coupon = { };

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.daysRemained = function() {
                    var dateTo = $scope.coupon.dateTo;
                    var daysRemained = moment(dateTo).diff(moment(), 'days');
                    if (daysRemained < 0) {
                        return "-";
                    } else if (daysRemained == 0) {
                        return 'До 24:00';
                        //return 'До ' + moment(dateTo).format('hh:mm');
                    } else {
                        return 'Осталось: ' + daysRemained + ' ' + calcDayDeclension(daysRemained);
                    }
                };

                function calcDayDeclension(days) {
                    var daysString;
                    if (days % 100 > 10 && days % 100 < 20)
                        daysString = " дней";
                    else if (days % 10 == 1)
                        daysString = " день";
                    else if (days % 10 > 1 && days % 10 < 5)
                        daysString = " дня";
                    else
                        daysString = " дней";

                    return daysString;
                };

                httpService.get("/admin/coupons/" + coupon.id,
                    function (response) {
                        var tempCoupon = angular.copy(response);
                        tempCoupon.dateTo = moment(response.dateTo);
                        $scope.coupon = tempCoupon;
                        $scope.isAdmin = true;
                    }
                );

                function hexToRgb(hex) {
                    if(hex === undefined) {
                        return null;
                    }
                    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                        return r + r + g + g + b + b;
                    });

                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    return result ? {
                        r: parseInt(result[1], 16),
                        g: parseInt(result[2], 16),
                        b: parseInt(result[3], 16)
                    } : null;
                }


                $scope.calcFontColor = function(backgroundHex) {
                    var backGroundRgb = hexToRgb(backgroundHex);

                    var d = 0;

                    if(backGroundRgb === null) {
                        d = 255;
                        return  'rgb(' + d + ',' + d + ',' + d + ')';
                    }


                    // Counting the perceptive luminance - human eye favors green color...
                    var a = 1 - ( 0.299 * backGroundRgb.r
                        + 0.587 * backGroundRgb.g + 0.114 * backGroundRgb.b) / 255;

                    if (a < 0.3)
                        d = 0; // bright colors - black font
                    else
                        d = 255; // dark colors - white font

                    return  'rgb(' + d + ',' + d + ',' + d + ')';
                };


                $scope.calcGiftBackGround = function(flagColorHex) {
                    var backGroundRgb = hexToRgb(flagColorHex);

                    var cssFileUrlProperty = 0;

                    // Counting the perceptive luminance - human eye favors green color...
                    var a = 1 - ( 0.299 * backGroundRgb.r
                        + 0.587 * backGroundRgb.g + 0.114 * backGroundRgb.b) / 255;

                    if (a < 0.3)
                        cssFileUrlProperty = "url('/resources/img/gift_iconb.png')"; // bright colors - black picture
                    else
                        cssFileUrlProperty = "url('/resources/img/gift_iconw.png')";  // dark colors - white picture

                    return  cssFileUrlProperty;
                };

            }

        }]);