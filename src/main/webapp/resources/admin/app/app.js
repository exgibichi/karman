/**
 * Created by ioann on 30.05.16.
 */

'use strict';

angular.module('mainApp', [
    'ngRoute',
    'ngMaterial',
    'mainApp.categories',
    'mainApp.regions',
    'mainApp.newCoupons',
    'mainApp.newCompanies',
    'mainApp.colors',
    'mainApp.prices',
    'ngMdIcons'
])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/categories'});
    }])

    .controller('MainController', ['$scope', '$timeout', '$mdSidenav',
        function ($scope, $timeout, $mdSidenav) {

            $scope.toggleRight = function () {
                $mdSidenav('left').toggle();
            };

            $scope.menu = {};
            $scope.menu.pages = [
                {"url": "/categories", "description": "Категории"},
                {"url": "/coupons/new", "description": "Новые акции"},
                {"url": "/companies/new", "description": "Новые компании"},
                {"url": "/colors", "description": "Цвета"},
                {"url": "/prices", "description": "Цены"},
                {"url": "/regions", "description": "Регионы/Города"}
            ];

            $scope.menu.isPageSelected = function (page) {
                return ($scope.menu.currentPage == page);
            };

            $scope.menu.toggleSelectPage = function (page) {
                $scope.menu.currentPage = page;
            };
        }])

    .controller('LeftController', ['$scope', '$timeout', '$mdSidenav',
        function ($scope, $timeout, $mdSidenav) {
            $scope.close = function() {
                $mdSidenav('left').close();
            };
        }]);

