/* Добавляет таблицу coupon_images. У одной акции может быть более одной картинки*/

CREATE TABLE coupon_images
(
  coupon_id bigint NOT NULL,
  image character varying(255),
  CONSTRAINT fkr9jsb49jx5qmaur8axv7g6poh FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupon_images
  OWNER TO saleband;



INSERT INTO coupon_images (coupon_id, image)
SELECT c.id, c.image FROM coupons c WHERE c.image IS NOT NULL;


ALTER TABLE coupons DROP column image;