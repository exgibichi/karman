/* Добавляет в таблицу companies столбцы,
   использующиеся при модерации активной компании */

ALTER TABLE companies ADD COLUMN new_name character varying(80);
ALTER TABLE companies ADD COLUMN new_email character varying(260);
ALTER TABLE companies ADD COLUMN new_full_name character varying(100);