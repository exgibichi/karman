/* Добавляет столбец для отражения даты поднятия приоритета акции в лентах просмотра */


ALTER TABLE coupons ADD COLUMN raising_date timestamp without time zone;
UPDATE coupons SET raising_date = created;
ALTER TABLE coupons ALTER COLUMN raising_date SET NOT NULL;