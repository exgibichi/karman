/* Этот скрипт добавляет в базу таблицу coupons для хранения информации об акциях
 */

CREATE TABLE coupons
(
  id bigserial NOT NULL,
  background_color_hex character varying(7),
  created timestamp without time zone,
  date_to timestamp without time zone,
  description character varying(1000) NOT NULL,
  discount integer,
  gift_description character varying(1000),
  image character varying(255),
  rejection_reason character varying(300),
  status character varying(255) NOT NULL,
  title character varying(100) NOT NULL,
  category_id bigint,
  company_id bigint NOT NULL,
  region_id bigint,
  CONSTRAINT coupons_pkey PRIMARY KEY (id),
  CONSTRAINT fkdcx0ovgcgvc3v9clxn2b9kvg9 FOREIGN KEY (company_id)
      REFERENCES companies (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkrbgg3811vru2ktg77gpxxksvl FOREIGN KEY (region_id)
      REFERENCES regions (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fksywuemw12y5s03cupsj3mjs3b FOREIGN KEY (category_id)
      REFERENCES categories (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupons
  OWNER TO saleband;

  ALTER SEQUENCE coupons_id_seq OWNED BY coupons.id;

ALTER TABLE ONLY coupons ALTER COLUMN id SET DEFAULT nextval('coupons_id_seq'::regclass);