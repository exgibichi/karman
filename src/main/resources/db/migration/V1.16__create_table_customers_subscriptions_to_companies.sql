/* Добавляет таблицу подписок пользователей на компании через отношение ManyToMany */

CREATE TABLE customers_subscriptions_to_companies
(
  customer_id bigint NOT NULL,
  company_id bigint NOT NULL,
  CONSTRAINT customers_subscriptions_to_companies_pkey PRIMARY KEY (customer_id, company_id),
  CONSTRAINT fkcr67gdfnpyvjj5iwjna7gv980 FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkdg5td22f0nsqhd5vhphecu6h9 FOREIGN KEY (company_id)
      REFERENCES companies (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE customers_subscriptions_to_companies
  OWNER TO saleband;