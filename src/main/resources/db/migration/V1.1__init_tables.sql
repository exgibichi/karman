/*Скрипт, инициализирующий структуру базы данных*/

--Таблица простых пользователей
CREATE TABLE customers
(
  id bigserial NOT NULL,
  CONSTRAINT customers_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE customers
  OWNER TO saleband;

ALTER SEQUENCE customers_id_seq OWNED BY customers.id;

ALTER TABLE ONLY customers ALTER COLUMN id SET DEFAULT nextval('customers_id_seq'::regclass);

--Таблица компаний
CREATE TABLE companies
(
  id bigserial NOT NULL,
  CONSTRAINT companies_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE companies
  OWNER TO saleband;

ALTER SEQUENCE companies_id_seq OWNED BY companies.id;

ALTER TABLE ONLY companies ALTER COLUMN id SET DEFAULT nextval('companies_id_seq'::regclass);

--Корневая таблица пользователей
CREATE TABLE users
(
  id bigserial NOT NULL,
  name character varying(80) NOT NULL,
  password_hash character varying(60) NOT NULL,
  phone_number character varying(16) NOT NULL,
  registration_date timestamp without time zone,
  company_id bigint,
  customer_id bigint,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT fkchxdoybbydcaj5smgxe0qq5mk FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkin8gn4o1hpiwe6qe4ey7ykwq7 FOREIGN KEY (company_id)
      REFERENCES companies (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_72efjd5u3nqtjdduip5wwvd39 UNIQUE (customer_id),
  CONSTRAINT uk_9q63snka3mdh91as4io72espi UNIQUE (phone_number),
  CONSTRAINT uk_camlvklbluxpcvlps0s8vs8wp UNIQUE (company_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO saleband;

ALTER SEQUENCE users_id_seq OWNED BY users.id;

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--Таблица категорий
CREATE TABLE categories
(
  id bigserial NOT NULL,
  title character varying(255) NOT NULL,
  CONSTRAINT categories_pkey PRIMARY KEY (id),
  CONSTRAINT uk_tkwloef0k6ccv94cipgnmvma8 UNIQUE (title)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE categories
  OWNER TO saleband;


ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--Таблица регионов
CREATE TABLE regions
(
  id bigserial NOT NULL,
  title character varying(255) NOT NULL,
  parent_id bigint,
  CONSTRAINT regions_pkey PRIMARY KEY (id),
  CONSTRAINT fk1nqjho4xdkm4f2tw928am9451 FOREIGN KEY (parent_id)
      REFERENCES regions (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT region_city_constraint UNIQUE (title, parent_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regions
  OWNER TO saleband;

ALTER SEQUENCE regions_id_seq OWNED BY regions.id;

ALTER TABLE ONLY regions ALTER COLUMN id SET DEFAULT nextval('regions_id_seq'::regclass);


