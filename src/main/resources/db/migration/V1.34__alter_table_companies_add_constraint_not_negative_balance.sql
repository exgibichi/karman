/* Запрещает хранение негативных значений в колонке balance таблицы companies */
ALTER TABLE companies ADD constraint not_negative_balance check(balance >= 0);