/* Добавляет флаг активации компании и комментарий модератора/админа */

ALTER TABLE companies ADD COLUMN moderators_comment character varying(255);

ALTER TABLE companies ADD COLUMN is_active boolean;
UPDATE companies SET is_active = true;
ALTER TABLE companies ALTER COLUMN is_active SET NOT NULL;
