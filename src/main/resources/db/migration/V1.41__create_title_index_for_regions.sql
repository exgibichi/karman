/* Создает индекс на название региона */
CREATE INDEX title_index
  ON regions
  USING btree
  (title COLLATE pg_catalog."default");