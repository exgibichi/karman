/* Добавляет компаниям поля email и fullName */

ALTER TABLE companies ADD COLUMN email character varying(260);
ALTER TABLE companies ADD COLUMN full_name character varying(100);

UPDATE companies
SET email = 'email';

UPDATE companies
SET full_name = email;

ALTER TABLE companies ALTER COLUMN email SET NOT NULL;
ALTER TABLE companies ALTER COLUMN full_name SET NOT NULL;