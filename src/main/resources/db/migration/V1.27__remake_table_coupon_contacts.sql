/* Скрипт расширяет таблицу coupon_contacts геолокационными данными */


--Создание новой таблицы
CREATE TABLE coupon_contacts
(
  id bigserial NOT NULL,
  address character varying(500) NOT NULL,
  latitude character varying(15),
  longitude character varying(15),
  phone_number character varying(100) NOT NULL,
  coupon_id bigint NOT NULL,
  CONSTRAINT coupon_contacts_pkey PRIMARY KEY (id),
  CONSTRAINT fk8ehthwncfiesgmbjy3hl0w2ry FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ukjg85pmecxdfnfmdxjx1d4dn59 UNIQUE (coupon_id, address)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupon_contacts
  OWNER TO saleband;


ALTER SEQUENCE coupon_contacts_id_seq OWNED BY coupon_contacts.id;

ALTER TABLE ONLY coupon_comments ALTER COLUMN id SET DEFAULT nextval('coupon_contacts_id_seq'::regclass);

--Перенос данных из старой в новую
INSERT INTO coupon_contacts(id, address, phone_number, coupon_id)
SELECT nextval('coupon_contacts_id_seq'), old.address, old.phonenumber, old.coupon_id FROM coupons_contacts AS old;

--Удаление старой таблицы
DROP table coupons_contacts;