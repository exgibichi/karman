/* Скрипт изменяет структуру таблицы payment_histories
   для большего соответствия требованиям бизнес логики */

ALTER TABLE payment_histories DROP COLUMN inv_id;
ALTER TABLE payment_histories ALTER COLUMN payment_date DROP NOT NULL;
ALTER TABLE payment_histories ADD COLUMN info character varying(255);