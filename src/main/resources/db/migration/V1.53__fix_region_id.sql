/* Фиксим тип для region_id */
ALTER TABLE "users"
ALTER "region_id" TYPE bigint,
ALTER "region_id" DROP DEFAULT,
ALTER "region_id" DROP NOT NULL;