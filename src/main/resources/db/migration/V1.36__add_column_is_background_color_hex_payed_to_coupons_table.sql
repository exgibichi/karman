/*
   Добавляет колонку is_background_color_hex_payed в таблицу coupons
   Это нужно для контроля оплаты цвета фона акции
*/

ALTER TABLE coupons ADD COLUMN is_background_color_hex_payed boolean;
UPDATE coupons SET is_background_color_hex_payed = false;
ALTER TABLE coupons ALTER COLUMN is_background_color_hex_payed SET NOT NULL;