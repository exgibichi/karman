/* Добавляет в таблицу coupons колонку fixing_date,
   отвечающую за дату закрепления акции */

ALTER TABLE coupons ADD COLUMN fixing_date timestamp without time zone;