CREATE TABLE customers_favorite_coupons
(
  customer_id bigint NOT NULL,
  coupon_id bigint NOT NULL,
  CONSTRAINT customers_favorite_coupons_pkey PRIMARY KEY (customer_id, coupon_id),
  CONSTRAINT fk5i0ybxc4uqapk7q8k87d1wiev FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkijt8dxflsh0k72tt55pxwi3rv FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE customers_favorite_coupons
  OWNER TO saleband;