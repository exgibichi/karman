/* Скрипт добавляет колонку parent_id в таблицу coupons.
   Записи с непустым полем означают, что они хранят данные теневой копии акции,
   используемой для редактирования акции, находящейся в состоянии публикации.
 */

 ALTER TABLE coupons ADD COLUMN parent_id bigint;

 ALTER TABLE coupons
  ADD CONSTRAINT fksvljjmadox65oth26q2eewjb9 FOREIGN KEY (parent_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;