/* Добавляет поле с числом комментов в таблицу акций */
ALTER TABLE coupons ADD COLUMN comments_count bigint;

UPDATE coupons SET comments_count = (SELECT COUNT(cm.id) FROM coupon_comments cm WHERE cm.coupon_id = coupons.id)
WHERE comments_count IS NULL;

ALTER TABLE coupons ALTER COLUMN comments_count SET NOT NULL;