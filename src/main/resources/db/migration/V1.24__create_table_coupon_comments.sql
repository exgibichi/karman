/* Добавляет таблицу комментариев акций */

CREATE TABLE coupon_comments
(
  id bigserial NOT NULL,
  text character varying(1000),
  coupon_id bigint NOT NULL,
  customer_id bigint NOT NULL,
  created timestamp without time zone NOT NULL,

  CONSTRAINT coupon_comments_pkey PRIMARY KEY (id),
  CONSTRAINT fk7co9n8eg5cpktsqdfn2qeykuu FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fknpi00vbb5li72lup6eaohvpxc FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ukc2vurj6sgpb9ufx3hyngabjs5 UNIQUE (coupon_id, customer_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupon_comments
  OWNER TO saleband;


ALTER SEQUENCE coupon_comments_id_seq OWNED BY coupon_comments.id;

ALTER TABLE ONLY coupon_comments ALTER COLUMN id SET DEFAULT nextval('coupon_comments_id_seq'::regclass);