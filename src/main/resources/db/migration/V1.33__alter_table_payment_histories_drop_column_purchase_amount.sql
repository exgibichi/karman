/* Удаляет из таблицы payment_histories колонку purchase_amount.
   Это связано с изменением модели хранения. В балансе хранятся не купоны, а рубли.
 */

 ALTER TABLE payment_histories DROP COLUMN purchase_amount;