/* Скрипт добавляет таблицу coupons_contacts для учета контактов, привязанных к акциям  */

CREATE TABLE coupons_contacts
(
  coupon_id bigint NOT NULL,
  phonenumber character varying(100),
  address character varying(500) NOT NULL,
  CONSTRAINT coupons_contacts_pkey PRIMARY KEY (coupon_id, address),
  CONSTRAINT fk1kndnbjw7w8d9hsm6yirwupjt FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupons_contacts
  OWNER TO saleband;