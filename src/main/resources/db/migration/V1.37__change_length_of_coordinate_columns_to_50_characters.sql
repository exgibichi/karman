/* Расширяет размер колонок с координатами до 50 символов */

UPDATE pg_attribute SET atttypmod = 50+4
WHERE attrelid = 'coupon_contacts'::regclass
AND attname = 'latitude';

UPDATE pg_attribute SET atttypmod = 50+4
WHERE attrelid = 'coupon_contacts'::regclass
AND attname = 'longitude';