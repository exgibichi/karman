/* Добавляет в базу таблицу цветов colors */
CREATE TABLE colors
(
  id bigserial NOT NULL,
  color_hex character varying(7),
  CONSTRAINT colors_pkey PRIMARY KEY (id),
  CONSTRAINT uk_tnldke1q7ui2p6k6xyp455582 UNIQUE (color_hex)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE colors
  OWNER TO saleband;



ALTER SEQUENCE colors_id_seq OWNED BY colors.id;

ALTER TABLE ONLY colors ALTER COLUMN id SET DEFAULT nextval('colors_id_seq'::regclass);