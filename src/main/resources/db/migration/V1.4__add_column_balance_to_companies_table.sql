/* Скрипт добаляет столбец balance в таблицу companies и отвечает
 за баланс доступных акций для размещения*/
 ALTER TABLE companies ADD COLUMN balance integer;
 UPDATE companies SET balance = 0;
 ALTER TABLE companies ALTER COLUMN balance SET NOT NULL;