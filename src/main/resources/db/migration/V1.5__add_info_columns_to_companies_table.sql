/* Скрипт добавляет в таблицу companies поля category_id, description и logo */

ALTER TABLE companies ADD COLUMN category_id bigint;
ALTER TABLE companies ADD COLUMN description character varying(1500);
ALTER TABLE companies ADD COLUMN logo character varying(255);

ALTER TABLE companies
  ADD CONSTRAINT fkecyq3nar23jngmnw4vui7n6ey FOREIGN KEY (category_id)
      REFERENCES categories (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;