/* В подарки добавляется заголовок и картинка */
ALTER TABLE coupons ADD COLUMN gift_title character varying(63);
ALTER TABLE coupons ADD COLUMN gift_image character varying(255);

UPDATE coupons SET gift_image = image, gift_title = 'Подарок' where gift_description IS NOT NULL;