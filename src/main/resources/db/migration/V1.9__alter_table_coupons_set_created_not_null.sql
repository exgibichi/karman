/* Скрипт делает так, чтобы дата создания акции было обязательным полем */
ALTER TABLE coupons ALTER COLUMN created SET NOT NULL;