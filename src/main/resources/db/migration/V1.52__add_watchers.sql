/* Добавляет таблицу подписок пользователей на компании через отношение ManyToMany */

CREATE TABLE customers_watches_to_coupons
(
  customer_id bigint NOT NULL,
  coupon_id bigint NOT NULL,
  CONSTRAINT customers_watches_to_coupons_pkey PRIMARY KEY (customer_id, coupon_id),
  CONSTRAINT fkcr99gdfnpyvjj5iwjna7gv980 FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkdg4td23f0nsqhd5vhphecu6h9 FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE customers_watches_to_coupons
  OWNER TO saleband;