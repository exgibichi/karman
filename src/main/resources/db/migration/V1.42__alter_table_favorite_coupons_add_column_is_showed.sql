/* Добавляет в таблицу favorite_coupons флаг is_showed для определения
  непросмотренных принудительно добавленных акций
*/

ALTER TABLE favorite_coupons ADD COLUMN is_showed boolean;
UPDATE favorite_coupons SET is_showed = TRUE;
ALTER TABLE favorite_coupons ALTER COLUMN is_showed SET NOT NULL;