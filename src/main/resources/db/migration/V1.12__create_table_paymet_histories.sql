/* Добавляет таблицу payment_histories для сохранения фактов оплаты
   внутрисистемной валюты компаниями*/

   CREATE TABLE payment_histories
(
  id bigserial NOT NULL,
  amount_of_money numeric(10,2) NOT NULL,
  inv_id integer NOT NULL,
  payment_date timestamp without time zone NOT NULL,
  purchase_amount integer NOT NULL,
  status character varying(255) NOT NULL,
  company_id bigint NOT NULL,
  CONSTRAINT payment_histories_pkey PRIMARY KEY (id),
  CONSTRAINT fkdcq9uir40fvf3yshqosw5nk7u FOREIGN KEY (company_id)
      REFERENCES companies (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE payment_histories
  OWNER TO saleband;


ALTER SEQUENCE payment_histories_id_seq OWNED BY payment_histories.id;

ALTER TABLE ONLY payment_histories ALTER COLUMN id SET DEFAULT nextval('payment_histories_id_seq'::regclass);