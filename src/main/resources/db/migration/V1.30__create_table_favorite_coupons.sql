/* Добавляет более сложную таблицу любимых акций */

CREATE TABLE favorite_coupons
(
  id bigserial NOT NULL,
  created timestamp without time zone NOT NULL,
  coupon_id bigint NOT NULL,
  customer_id bigint NOT NULL,
  CONSTRAINT favorite_coupons_pkey PRIMARY KEY (id),
  CONSTRAINT fk8bwyait9dqkamwj86oykv9bl2 FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fke72fc5h4lxl13u54bmee6f8ax FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE favorite_coupons
  OWNER TO saleband;


ALTER SEQUENCE favorite_coupons_id_seq OWNED BY favorite_coupons.id;

ALTER TABLE ONLY favorite_coupons ALTER COLUMN id SET DEFAULT nextval('favorite_coupons_id_seq'::regclass);

--Перенос данных из старой в новую
INSERT INTO favorite_coupons(id, coupon_id, customer_id, created)
SELECT nextval('favorite_coupons_id_seq'), old.coupon_id, old.customer_id, NOW() FROM customers_favorite_coupons AS old;

--Удаление старой таблицы
DROP table customers_favorite_coupons;