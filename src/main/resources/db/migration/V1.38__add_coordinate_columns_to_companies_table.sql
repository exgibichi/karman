/* Добавляет колонки с координатами в таблицу компаний companies */

ALTER TABLE companies ADD COLUMN longitude character varying(50);
ALTER TABLE companies ADD COLUMN latitude character varying(50);