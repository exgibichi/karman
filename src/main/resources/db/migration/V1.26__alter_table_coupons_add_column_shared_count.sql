/* Добавляет поле с числом поделившихся в таблицу акций */
ALTER TABLE coupons ADD COLUMN shared_count bigint;
UPDATE coupons SET shared_count = 0 WHERE shared_count IS NULL;
ALTER TABLE coupons ALTER COLUMN shared_count SET NOT NULL;
