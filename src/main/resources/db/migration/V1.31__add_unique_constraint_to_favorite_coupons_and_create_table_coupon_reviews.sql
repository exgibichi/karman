/*
   1. Добавляет ограничение на уникальность пары customer_id, coupon_id
      в таблицу favorite_coupons
   2. Создает таблицу просмотров акций coupon_reviews
*/

-- 1.
ALTER TABLE favorite_coupons ADD CONSTRAINT ukc2y35ju2g6afoygc2bbhk5rah UNIQUE (customer_id, coupon_id);


-- 2.
CREATE TABLE coupon_reviews
(
  id bigserial NOT NULL,
  date timestamp without time zone NOT NULL,
  token character varying(255) NOT NULL,
  coupon_id bigint NOT NULL,
  CONSTRAINT coupon_reviews_pkey PRIMARY KEY (id),
  CONSTRAINT fkrxa97dqbsihivg3mdey10y1jc FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coupon_reviews
  OWNER TO saleband;


ALTER SEQUENCE coupon_reviews_id_seq OWNED BY coupon_reviews.id;

ALTER TABLE ONLY coupon_reviews ALTER COLUMN id SET DEFAULT nextval('coupon_reviews_id_seq'::regclass);