/* Добавляет таблицу с ценами */

CREATE TABLE prices
(
  id bigserial NOT NULL,
  price integer NOT NULL,
  type character varying(255) NOT NULL,
  CONSTRAINT prices_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE prices
  OWNER TO saleband;


ALTER SEQUENCE prices_id_seq OWNED BY prices.id;

ALTER TABLE ONLY prices ALTER COLUMN id SET DEFAULT nextval('prices_id_seq'::regclass);

ALTER TABLE prices ADD constraint not_negative_price check(price >= 0);
