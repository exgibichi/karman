/* Скрипт добавляем необходимые структуры данных для хранения информации о лайках акций
   1. Таблицу customers_liked_coupons для связей пользователей с лайками.
   2. Колонку likes_count в таблицу coupons с числом лайков
*/

CREATE TABLE customers_liked_coupons
(
  customer_id bigint NOT NULL,
  coupon_id bigint NOT NULL,
  CONSTRAINT customers_liked_coupons_pkey PRIMARY KEY (customer_id, coupon_id),
  CONSTRAINT fk6c5x94vq414y46opk4lfxjvwc FOREIGN KEY (customer_id)
      REFERENCES customers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkrwbnulk56s58o2whwxiwv0op5 FOREIGN KEY (coupon_id)
      REFERENCES coupons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE customers_liked_coupons
  OWNER TO saleband;


ALTER TABLE coupons ADD COLUMN likes_count bigint;
UPDATE coupons SET likes_count = 0 WHERE likes_count IS NULL;
ALTER TABLE coupons ALTER COLUMN likes_count SET NOT NULL;