/* В акции добавляется цвет флажка */
ALTER TABLE coupons ADD COLUMN flag_color_hex character varying(7);

UPDATE coupons SET flag_color_hex = background_color_hex;