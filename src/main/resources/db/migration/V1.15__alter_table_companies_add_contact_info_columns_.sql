/* Добавление в таблицу компаний колонок, связанных с контактной информацией */
ALTER TABLE companies ADD COLUMN site_url character varying(300);
ALTER TABLE companies ADD COLUMN address character varying(300);
ALTER TABLE companies ADD COLUMN contact_phone_number character varying(30);