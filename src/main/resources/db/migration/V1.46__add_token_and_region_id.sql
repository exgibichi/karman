/* Скрипт добаляет столбец token и region_id в таблицу users */
 ALTER TABLE users ADD COLUMN region_id integer;
 ALTER TABLE users ADD COLUMN token character varying(500);