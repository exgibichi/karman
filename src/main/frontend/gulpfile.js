var gulp           = require('gulp');
var concat         = require('gulp-concat');
var concatVendor   = require('gulp-concat-vendor');
var uglify         = require('gulp-uglify');
var minify         = require('gulp-minify-css');
var mainBowerFiles = require('main-bower-files');
var inject         = require('gulp-inject');
var runSequence    = require('gulp-run-sequence');
var gzip           = require('gulp-gzip');
var clone          = require('gulp-clone');
var series         = require('stream-series');

var vendorJs;
var vendorCss;


gulp.task('lib-js-files', function () {
    vendorJs = gulp.src(mainBowerFiles('**/*.js'),{ base: 'bower_components' })
        .pipe(concatVendor('lib.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../webapp/resources/vendor/js'));


    vendorJs.pipe(clone())
        .pipe(gzip())
        .pipe(gulp.dest('../webapp/resources/vendor/js'));
});

gulp.task('lib-css-files', function () {
    vendorCss = gulp.src(mainBowerFiles('**/*.css'),{ base: 'bower_components'})
        .pipe(concat('lib.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('../webapp/resources/vendor/css'));

    vendorCss.pipe(clone())
        .pipe(gzip())
        .pipe(gulp.dest('../webapp/resources/vendor/css'));
});


gulp.task('index', function () {
    var target = gulp.src("../webapp/WEB-INF/views/index.html");
    var sources = gulp.src(['../webapp/resources/client/**/*.js',
        '../webapp/resources/common/**/*.js',
        '../webapp/resources/common/**/*.css',
        '../webapp/resources/client/**/*.css'], {read: false});
    target.pipe(inject(series(vendorJs, vendorCss, sources), {relative: true}))
        .pipe(gulp.dest('../webapp/WEB-INF/views'));
});

gulp.task('company', function() {
    var target = gulp.src("../webapp/WEB-INF/views/company.html");
    var sources = gulp.src(['../webapp/resources/company/**/*.js',
        '../webapp/resources/common/**/*.js',
        '../webapp/resources/common/**/*.css',
        '../webapp/resources/company/**/*.css'], {read: false});
    target.pipe(inject(series(vendorJs, vendorCss, sources), {relative: true}))
        .pipe(gulp.dest('../webapp/WEB-INF/views'));
});

gulp.task('admin', function() {
    var target = gulp.src("../webapp/WEB-INF/views/admin.html");
    var sources = gulp.src(['../webapp/resources/admin/**/*.js',
        '../webapp/resources/common/**/*.js',
        '../webapp/resources/common/**/*.css',
        '../webapp/resources/admin/**/*.css'], {read: false});
    target.pipe(inject(series(vendorJs, vendorCss, sources), {relative: true}))
        .pipe(gulp.dest('../webapp/WEB-INF/views'));
});


// Default Task
gulp.task('default', function () {
    runSequence('lib-js-files', 'lib-css-files', 'index',
        'company', 'admin');});