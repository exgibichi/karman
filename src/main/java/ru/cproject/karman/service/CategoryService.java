package ru.cproject.karman.service;

import ru.cproject.karman.dao.jpa.query.common.CategoryQuery;
import ru.cproject.karman.domain.common.Category;

import java.util.List;

/**
 * Created by ioann on 30.05.16
 */
public interface CategoryService {

    Category save(Category category);
    void update(Long id, String title);
    void delete(Long id);
    List<Category> getAll(CategoryQuery query);

}
