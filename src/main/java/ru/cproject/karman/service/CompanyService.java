package ru.cproject.karman.service;

import ru.cproject.karman.dao.jpa.query.account.CompanyQuery;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;

import java.util.List;

/**
 * Created by ioann on 06.06.16
 */
public interface CompanyService {

    /**
     * Позволяет получить подробную информацию о компании по ее id
     * @param companyId id компании
     * @return Company
     */
    Company get(Long companyId);

    /**
     * Позволяет получить список всех компаний
     * @return
     */
    List<Company> getAll();


    List<Company> getPageByQuery(CompanyQuery query);

    /**
     * Позволяет получить список всех неактивных компаний, готовых к модерации
     * @return
     */
    List<Company> getAllToReview();

    /**
     * Позволяет отредактировать информацию о компании
     * @param company domain объект компании
     * @return Company
     */
    Company edit(Company company);


    /**
     * Позволяет обновить аватар для компании
     * @param avatarFileName имя файла аватара
     * @param companyId id компании
     * @return String имя файла аватара
     */
    String updateAvatar(String avatarFileName, Long companyId);

    /**
     * Позволяет удалить аватарку для указанной компании
     * @param company компания
     */
    void deleteAvatar(Company company);


    /**
     * Позволяет получить список идентификаторов компаний, на которые подписан потребитель
     * @param customer потребитель
     * @return List<Long>
     */
    List<Long> getSubscriptionIdsByCustomer(Customer customer);


    /**
     * Позволяет отклонить модерируемые данные
     * @param companyId id компании
     * @param moderatorsComment коментарий
     */
    void rejectModeration(Long companyId, String moderatorsComment);

    /**
     * Позволяет подтвердить модерируемые данные
     * @param companyId
     */
    void confirmModeration(Long companyId);
}
