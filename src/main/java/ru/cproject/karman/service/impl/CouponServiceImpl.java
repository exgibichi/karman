package ru.cproject.karman.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.component.PriceCalculator;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.dao.jpa.query.business.CouponQuery;
import ru.cproject.karman.dao.jpa.repositories.account.CompanyRepository;
import ru.cproject.karman.dao.jpa.repositories.account.CustomerRepository;
import ru.cproject.karman.dao.jpa.repositories.business.*;
import ru.cproject.karman.dao.jpa.repositories.common.CategoryRepository;
import ru.cproject.karman.dao.jpa.repositories.common.RegionRepository;
import ru.cproject.karman.dao.jpa.specifications.CouponSpecifications;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponContact;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponContactEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;
import ru.cproject.karman.model.common.CategoryEntity;
import ru.cproject.karman.model.common.RegionEntity;
import ru.cproject.karman.service.CouponService;
import ru.cproject.karman.service.CustomerService;
import ru.cproject.karman.service.YandexGeocoderService;
import ru.cproject.karman.validator.FieldValidator;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ioann on 02.06.16
 */
@Service
public class CouponServiceImpl implements CouponService {

    private static final Logger LOGGER = Logger.getLogger(CouponServiceImpl.class.getName());

    @Value("${saleband.imagepath}")
    private String imagePath;

    private CouponRepository couponRepository;
    private CompanyRepository companyRepository;
    private CustomerRepository customerRepository;
    private RegionRepository regionRepository;
    private CategoryRepository categoryRepository;
    private CouponContactRepository couponContactRepository;
    private CouponReviewRepository couponReviewRepository;
    private CouponCommentRepository couponCommentRepository;
    private FavoriteCouponRepository favoriteCouponRepository;
    private CustomerService customerService;
    private CustomSecurityContext customSecurityContext;
    private PriceCalculator priceCalculator;


    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Autowired
    public void setCompanyRepository(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Autowired
    public void setRegionRepository(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public void setCouponContactRepository(CouponContactRepository couponContactRepository) {
        this.couponContactRepository = couponContactRepository;
    }

    @Autowired
    public void setCouponReviewRepository(CouponReviewRepository couponReviewRepository) {
        this.couponReviewRepository = couponReviewRepository;
    }

    @Autowired
    public void setCouponCommentRepository(CouponCommentRepository couponCommentRepository) {
        this.couponCommentRepository = couponCommentRepository;
    }

    @Autowired
    public void setFavoriteCouponRepository(FavoriteCouponRepository favoriteCouponRepository) {
        this.favoriteCouponRepository = favoriteCouponRepository;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setPriceCalculator(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }

    @Override
    @Transactional
    public Coupon create(Coupon coupon) {
        validateSave(coupon);

        CouponEntity couponEntity = fillCouponEntity(coupon);

        CompanyEntity companyEntity = companyRepository.findOne(
            customSecurityContext.getCurrentPrincipal()
                .getCompany().getId()
        );


        couponEntity.setCompany(companyEntity);

        couponEntity.setCreated(LocalDateTime.now());
        couponEntity.setDateTo(coupon.getDateTo());
        couponEntity.setRaisingDate(LocalDateTime.now());
        couponEntity.setStatus(CouponStatus.REVIEW);
        couponEntity.setLikesCount(0L);
        couponEntity.setCommentsCount(0L);
        couponEntity.setSharedCount(0L);
        couponEntity.setBackgroundColorHexPayed(false);

        couponEntity = couponRepository.save(couponEntity);

        fillCouponEntityFromDomainContacts(couponEntity, coupon.getContacts());

        if (!"#FFFFFF".toUpperCase().equals(couponEntity.getBackgroundColorHex().toUpperCase())) {
            int totalPrice = priceCalculator.calcPriceForBackgroundColor(couponEntity.getDateTo());
            companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);
            couponEntity.setBackgroundColorHexPayed(true);
        }

        return couponRepository.save(couponEntity).toDomain();
    }

    @Override
    @Transactional
    public void delete(Coupon coupon) {
        validateGet(coupon.getId());

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        if (couponEntity.getChild() != null) {
            couponContactRepository.deleteAllByCouponId(couponEntity.getId());
            couponReviewRepository.deleteAllByCouponId(coupon.getId());
            couponCommentRepository.deleteAllByCouponId(couponEntity.getId());
            favoriteCouponRepository.deleteAllByCouponId(couponEntity.getId());
            couponRepository.deleteImages(couponEntity.getId());

            couponRepository.delete(couponEntity.getChild().getId());
        }

        couponContactRepository.deleteAllByCouponId(couponEntity.getId());
        couponReviewRepository.deleteAllByCouponId(coupon.getId());
        couponCommentRepository.deleteAllByCouponId(couponEntity.getId());
        favoriteCouponRepository.deleteAllByCouponId(couponEntity.getId());
        couponRepository.deleteImages(couponEntity.getId());

        couponRepository.delete(couponEntity.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public Coupon get(Long couponId) {
        validateGet(couponId);
        return couponRepository.findOne(couponId).toDomain();
    }

    @Override
    @Transactional
    public void checkFavorite(Long couponId, Long customerId) {
        CouponEntity couponEntity = couponRepository.findOne(couponId);
        CustomerEntity customerEntity = customerRepository.findOne(customerId);
        if(favoriteCouponRepository.couponExistsInFavorites(customerEntity, couponEntity)) {
            favoriteCouponRepository.updateFavoritesByUserSetShowedWhereIsNotShowed(customerEntity, couponEntity);
        }
    }


    @Override
    @Transactional
    public Coupon edit(Coupon coupon) {
        validateEdit(coupon);
        CouponEntity childCouponEntity = fillCouponEntity(coupon);

        CompanyEntity companyEntity = companyRepository.findOne(
            customSecurityContext.getCurrentPrincipal()
                .getCompany().getId()
        );


        childCouponEntity.setCompany(companyEntity);

        childCouponEntity.setStatus(CouponStatus.REVIEW);

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        childCouponEntity.setDateTo(coupon.getDateTo());

        //Если до этого цвет фона не был оплачен и новый не белый,
        // то происходит списывание средств
        if (!couponEntity.isBackgroundColorHexPayed()
            && !"#FFFFFF".toUpperCase().equals(coupon.getBackgroundColorHex().toUpperCase())) {
            int totalPrice = priceCalculator.calcPriceForBackgroundColor(couponEntity.getDateTo());
            companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);
            couponEntity.setBackgroundColorHexPayed(true);
        }

        Coupon result = null;

        /*Есть два пути.
            1. Акция отклонена - значит, ее можно обновлять напрямую
            2. Акция активна и не должна сниматься с публикации - в этом случае
               следует создать теневую копию для модерации. После модерации копия
               в зависимости от результата накладывается на реальную, а затем удаляется (это происходит всегда).
         */
        if (CouponStatus.REJECTED.equals(couponEntity.getStatus())) {
            childCouponEntity.setId(couponEntity.getId());
            childCouponEntity.setCreated(couponEntity.getCreated());
            childCouponEntity.setRaisingDate(couponEntity.getRaisingDate());
            couponEntity = childCouponEntity;
            couponEntity.setLikesCount(0L);
            couponEntity.setCommentsCount(0L);
            couponEntity.setSharedCount(0L);
            couponEntity.setBackgroundColorHexPayed(false);

            fillCouponEntityFromDomainContacts(couponEntity, coupon.getContacts());

            result = couponRepository.save(couponEntity).toDomain();
        } else if (CouponStatus.ACTIVE.equals(couponEntity.getStatus()) || CouponStatus.FINISHED.equals(couponEntity.getStatus())) {

            if (couponEntity.getChild() != null) {
                childCouponEntity.setId(couponEntity.getChild().getId());
            }

            couponEntity.setChild(childCouponEntity);
            childCouponEntity.setParent(couponEntity);
            childCouponEntity.setCreated(LocalDateTime.now());
            childCouponEntity.setRaisingDate(LocalDateTime.now());
            childCouponEntity.setLikesCount(0L);
            childCouponEntity.setCommentsCount(0L);
            childCouponEntity.setSharedCount(0L);
            childCouponEntity.setBackgroundColorHexPayed(false);

            couponEntity = couponRepository.save(couponEntity);

            for (CouponContact contact : coupon.getContacts()) {
                CouponContactEntity contactEntity = new CouponContactEntity(contact);
                contactEntity.setCoupon(couponEntity.getChild());
                couponEntity.getChild().getContacts().add(contactEntity);
            }

            result = couponRepository.save(couponEntity).getChild().toDomain();
        }

        return result;
    }


    @Override
    @Transactional
    public void confirm(Long couponId) {
        validateConfirm(couponId);

        CouponEntity couponEntity = couponRepository.findOne(couponId);

        /* Возможны 2 ситуации.
           1. Подтверждается реальная акция - просто меняем статус;
           2. Подтверждается теневая копия акции - копируем данные из теневой копии
              в реальную, после чего удаляем теневую копию
         */
        if (couponEntity.getParent() == null) {

            if (LocalDateTime.now().isAfter(couponEntity.getDateTo())) {
                couponEntity.setStatus(CouponStatus.FINISHED);
            } else {
                couponEntity.setStatus(CouponStatus.ACTIVE);
            }

            couponEntity.setRejectionReason(null);
        } else {

            CouponEntity realCouponEntity = couponEntity.getParent();

            realCouponEntity.setRegion(couponEntity.getRegion());
            realCouponEntity.setCategory(couponEntity.getCategory());
            realCouponEntity.setTitle(couponEntity.getTitle());
            realCouponEntity.setImages(couponEntity.getImages());
            realCouponEntity.setDiscount(couponEntity.getDiscount());
            realCouponEntity.setPrice(couponEntity.getPrice());
            realCouponEntity.setGiftTitle(couponEntity.getGiftTitle());
            realCouponEntity.setGiftImage(couponEntity.getGiftImage());
            realCouponEntity.setGiftDescription(couponEntity.getGiftDescription());
            realCouponEntity.setGiftCondition(couponEntity.getGiftCondition());
            realCouponEntity.setDescription(couponEntity.getDescription());
            realCouponEntity.setDateTo(couponEntity.getDateTo());
            realCouponEntity.setBackgroundColorHex(couponEntity.getBackgroundColorHex());
            realCouponEntity.setFlagColorHex(couponEntity.getFlagColorHex());
            realCouponEntity.setStatus(CouponStatus.ACTIVE);

            Set<CouponContact> contacts = new LinkedHashSet<>();
            couponEntity.getContacts().forEach(c -> contacts.add(c.toDomain()));

            couponEntity.setParent(null);
            couponEntity.getContacts().clear();

            couponRepository.delete(couponEntity);


            realCouponEntity.getContacts().clear();
            realCouponEntity.setChild(null);
            for (CouponContact couponContact : contacts) {
                CouponContactEntity contactEntity = new CouponContactEntity(couponContact);
                contactEntity.setCoupon(realCouponEntity);
                contactEntity.setLatitude(couponContact.getLatitude());
                contactEntity.setLongitude(couponContact.getLongitude());

                contactEntity.setLongitude(couponContact.getLongitude());
                contactEntity.setLatitude(couponContact.getLatitude());

                realCouponEntity.getContacts().add(contactEntity);
            }

            couponRepository.save(realCouponEntity);

            return;
        }

        couponRepository.save(couponEntity);
    }

    @Override
    @Transactional
    public void reject(Long couponId, String rejectionReason) {
        validateRejection(couponId, rejectionReason);

        CouponEntity couponEntity = couponRepository.findOne(couponId);
        couponEntity.setStatus(CouponStatus.REJECTED);
        couponEntity.setRejectionReason(rejectionReason);

        couponRepository.save(couponEntity);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Coupon> getAllByStatus(CouponStatus status) {
        return couponRepository.findAllByStatus(status)
            .stream()
            .map(CouponEntity::toDomain)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public List<Coupon> getAllToReview() {
        return couponRepository.findAllByStatusAndActiveCompany(CouponStatus.REVIEW)
            .stream()
            .map(CouponEntity::toDomain)
            .collect(Collectors.toList());
    }

    @Override
    public List<Coupon> getAllParentByCompany(Company company) {
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());
        return couponRepository.findAllByCompanyAndParentIsNullOrderByCreatedDesc(companyEntity)
            .stream()
            .map(CouponEntity::toDomain)
            .collect(Collectors.toList());
    }

    @Override
    public List<Coupon> getAllByQuery(CouponQuery query) {
        Page<CouponEntity> page = couponRepository.findAll(CouponSpecifications.generateSearchSpecifications(query),
            query.getPageRequest());

        List<CouponEntity> content = page.getContent();

        return content.stream()
            .map(CouponEntity::toDomain)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional(readOnly = true)
    public List<Long> getFavoriteIdsByCustomer(Customer customer) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        return customerEntity.getFavoriteCoupons()
            .stream()
            .map(f -> f.getCoupon().getId())
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public HashMap<Long, Long> getFIdsByCustomer(Customer customer) {
                CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
                Set<FavoriteCouponEntity> iterator = customerEntity.getFavoriteCoupons();
                HashMap<Long, Long> fids = new HashMap<Long, Long>();
                for(FavoriteCouponEntity f : iterator) {
                        fids.put(f.getCoupon().getId(), f.getId());
                    }
                return fids;
            }

    @Override
    @Transactional(readOnly = true)
    public List<Long> getLikedIdsByCustomer(Customer customer) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        return customerEntity.getLikedCoupons()
            .stream()
            .map(CouponEntity::getId)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> getWatchedIdsByCustomer(Customer customer) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        return customerEntity.getWatchedCoupons()
            .stream()
            .map(CouponEntity::getId)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional
    public void raise(Company company, Coupon coupon) {
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        //Не берем в расчет фоновые акции
        if (couponEntity.getParent() != null) {
            throw new RuntimeException("Указанной акции не существует");
        }

        if (!companyEntity.equals(couponEntity.getCompany())) {
            throw new RuntimeException("Попытка поднять чужую акцию");
        }

        couponEntity.setRaisingDate(LocalDateTime.now());
        couponRepository.save(couponEntity);

        int totalPrice = priceCalculator.calcPriceForRaising();
        companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);
    }

    @Override
    @Transactional
    public void fix(Company company, Coupon coupon) {
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        //Не берем в расчет фоновые акции
        if (couponEntity.getParent() != null) {
            throw new RuntimeException("Указанной акции не существует");
        }

        if (!companyEntity.equals(couponEntity.getCompany())) {
            throw new RuntimeException("Попытка закрепить чужую акцию");
        }

        if (couponEntity.getFixingDate() != null) {
            throw new RuntimeException("Данная акция уже закреплена в топе");
        }

        couponEntity.setFixingDate(LocalDateTime.now());
        couponRepository.save(couponEntity);

        int totalPrice = priceCalculator.calcPriceForFixing();
        companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);
    }

    @Override
    @Transactional
    public void incSharedCount(Coupon coupon) {
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        couponRepository.incSharedCount(couponEntity);
    }

    @Override
    @Transactional
    public int forceAddCouponInFavoritesToRandomCustomers(Coupon coupon, int limit, Long region_id) {

        if (limit < 1) {
            throw new RuntimeException("Не указано число пользователей");
        }

        int countOfAddings = 0;

        //Получение пользователей, в чьи карманы попадет акция
        List<Customer> customersToProcess = getRandomCustomersWhoHaveNotCouponInFavorites(coupon, limit, region_id);

        //Распределение по карманам
        for (Customer customer : customersToProcess) {
            try {
                if (customerService.addCouponToFavorites(customer, coupon, true)) {
                    ++countOfAddings;
                }
            } catch (Exception e) {
                LOGGER.info("Can't force add coupon with id=" + coupon.getId() +
                    " to favorites for customer with id=" + customer.getId() + "\n" +
                    "Cause: " + e.getMessage());
            }
        }

        CompanyEntity companyEntity = companyRepository.findOne(coupon.getCompany().getId());
        int totalPrice = priceCalculator.calcPriceForForceAddingToFavorites(countOfAddings);
        companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);

        return countOfAddings;
    }


    @Override
    @Transactional
    public void extendCoupon(Coupon coupon, LocalDateTime dateTo) {
        CouponEntity couponEntity = couponRepository.getOne(coupon.getId());

        validateExtendingOfCoupon(coupon, dateTo);

        couponEntity.setDateTo(dateTo);
        couponEntity.setStatus(CouponStatus.ACTIVE);

        if (!"#FFFFFF".toUpperCase().equals(couponEntity.getBackgroundColorHex().toUpperCase())) {
            CompanyEntity companyEntity = companyRepository.findOne(coupon.getCompany().getId());
            int totalPrice = priceCalculator.calcPriceForBackgroundColor(couponEntity.getDateTo());
            companyRepository.decreaseCompanyBalance(companyEntity, totalPrice);
            couponEntity.setBackgroundColorHexPayed(true);
        }

        couponRepository.save(couponEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public long getCountOfReviews(Coupon coupon) {
        validateGet(coupon.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        return couponReviewRepository.countByCoupon(couponEntity);
    }

    private void validateSave(Coupon coupon) {
        Region region = coupon.getRegion();
        Category category = coupon.getCategory();

        if (region == null || region.getId() == null) {
            throw new FieldValidationException("Не задан регион");
        }

        if (!regionRepository.exists(region.getId())) {
            throw new FieldValidationException("Указанный регион не поддерживается");
        }

        if (category == null || category.getId() == null) {
            throw new FieldValidationException("Не задана категория");
        }

        if (!categoryRepository.exists(category.getId())) {
            throw new FieldValidationException("Указанная категория не поддерживается");
        }


        if (!FieldValidator.validateStringNotEmpty(coupon.getTitle())) {
            throw new FieldValidationException("У акции нет названия");
        }

        if (!FieldValidator.validateStringNotEmpty(coupon.getDescription())) {
            throw new FieldValidationException("У акции нет описания");
        }


        if (FieldValidator.validateStringNotEmpty(coupon.getFlagColorHex())) {
            FieldValidator.validateHexColor(coupon.getFlagColorHex());
        } else {
            throw new FieldValidationException("Не задан цвет флажка");
        }

        if (FieldValidator.validateStringNotEmpty(coupon.getBackgroundColorHex())) {
            FieldValidator.validateHexColor(coupon.getBackgroundColorHex());
        } else {
            throw new FieldValidationException("Не задан цвет фона акции");
        }

        if (coupon.getImages().isEmpty()) {
            throw new FieldValidationException("Отсутствует изображение");
        }


        validateTypeOfCoupon(coupon);

        Integer discount = coupon.getDiscount();

        if (discount != null && (discount <= 0 || discount > 100)) {
            throw new FieldValidationException("Размер скидки должен быть положительным числом не более 100");
        }

        if (coupon.getGiftTitle() != null
            && !FieldValidator.validateStringNotEmpty(coupon.getGiftTitle())) {
            throw new FieldValidationException("Отсутствует название подарка");
        }

        if (coupon.getGiftDescription() != null
            && !FieldValidator.validateStringNotEmpty(coupon.getGiftDescription())) {
            throw new FieldValidationException("Отсутствует описание подарка");
        }


        if (coupon.getGiftImage() != null) {
            if (!FieldValidator.validateStringNotEmpty(coupon.getGiftImage())) {
                throw new FieldValidationException("Отсутствует изображение подарка");
            }

            File file = new File(imagePath.substring(5), coupon.getGiftImage());

            if (!file.exists()) {
                throw new FieldValidationException("Указанного изображения подарка не существует");
            }
        }


        Integer price = coupon.getPrice();

        if (price != null && (price <= 0)) {
            throw new FieldValidationException("Размер скидки должен быть положительным числом");
        }


        if (coupon.getDateTo() == null) {
            throw new FieldValidationException("Отсутствует дата окончания акции");
        }

        LocalDateTime today = LocalDateTime.now();
        LocalDateTime yesterday = today.minusDays(1);

        if (yesterday.isAfter(coupon.getDateTo())) {
            throw new FieldValidationException("Задана некорректная дата окончания акции");
        }

        Set<CouponContact> contacts = coupon.getContacts();

        if (contacts.isEmpty()) {
            throw new FieldValidationException("Отсутствуют контакты");
        }

        if (contacts.size() > 3) {
            throw new FieldValidationException("Нельзя указывать более трех контактов");
        }

        for (CouponContact contact : contacts) {

            if (!FieldValidator.validateStringNotEmpty(contact.getAddress())
                || !FieldValidator.validateStringNotEmpty(contact.getPhoneNumber())) {
                throw new FieldValidationException("Указанные контакты заполнены не полностью");
            }
        }

        coupon.getImages().forEach(
            image -> {
                File file = new File(imagePath.substring(5), image);

                if (!file.exists()) {
                    throw new FieldValidationException("Указанного изображения не существует");
                }
            }
        );

    }


    private void validateTypeOfCoupon(Coupon coupon) {
        Integer discount = coupon.getDiscount();
        Integer price = coupon.getPrice();
        String giftTitle = coupon.getGiftTitle();
        String giftImage = coupon.getGiftImage();
        String giftDescription = coupon.getGiftDescription();
        String giftCondition = coupon.getGiftCondition(); //TODO !!!НЕТ ВАЛИДАЦИИ

        boolean isGift = giftTitle != null && giftImage != null && giftDescription != null;

        if (discount != null) {

            if (price != null) {
                throw new RuntimeException("Некорректный тип акции");
            }

        } else if (price != null && isGift) {
            throw new RuntimeException("Некорректный тип акции");
        } else if (price == null && !isGift) {
            throw new RuntimeException("Некорректный тип акции");
        }

    }


    private void validateEdit(Coupon coupon) {
        Coupon realCoupon = get(coupon.getId());

        if (coupon.getParent() != null) {
            //Нельзя редактировать теневые копии
            throw new RuntimeException("Указанной акции не существует");
        }

        Coupon child = coupon.getChild();

        if (child != null && !CouponStatus.REJECTED.equals(child.getStatus())) {
            throw new RuntimeException("Повторное редактирование акции невозможно " +
                "до прохождения модерации");
        }

        CouponStatus status = realCoupon.getStatus();

        if (!(CouponStatus.ACTIVE.equals(status) || CouponStatus.REJECTED.equals(status) || CouponStatus.FINISHED.equals(status))) {
            throw new RuntimeException("Разрешено редактировать только акции со статусами: " +
                " 'Активно', 'Отклонено' и 'Конец'");
        }

        validateSave(coupon);
    }

    private void validateGet(Long couponId) {
        if (couponId == null) {
            throw new FieldValidationException("Не указана акция");
        } else if (!couponRepository.exists(couponId)) {
            throw new FieldValidationException("Указанной акции не существует");
        }
    }

    private void validateConfirm(Long couponId) {
        if (couponId == null) {
            throw new FieldValidationException("Не указана акция");
        } else if (!couponRepository.exists(couponId)) {
            throw new FieldValidationException("Указанной акции не существует");
        }

        CouponEntity couponEntity = couponRepository.findOne(couponId);

        if (!CouponStatus.REVIEW.equals(couponEntity.getStatus())) {
            throw new FieldValidationException("Указанная акция не может быть принята, " +
                "так как не имеет статуса 'На рассмотрении'");
        }

    }


    private void validateRejection(Long couponId, String rejectionReason) {

        if (couponId == null) {
            throw new FieldValidationException("Не указана акция");
        } else if (!couponRepository.exists(couponId)) {
            throw new FieldValidationException("Указанной акции не существует");
        }

        CouponEntity couponEntity = couponRepository.findOne(couponId);

        if (!CouponStatus.REVIEW.equals(couponEntity.getStatus())) {
            throw new FieldValidationException("Указанная акция не может быть отклонена, " +
                "так как не имеет статуса 'На рассмотрении'");
        }

        if (!FieldValidator.validateStringNotEmpty(rejectionReason)) {
            throw new FieldValidationException("Не введена причина отклонения");
        }

    }


    private void validateExtendingOfCoupon(Coupon coupon, LocalDateTime dateTime) {

        validateGet(coupon.getId());

        CouponEntity couponEntity = couponRepository.getOne(coupon.getId());

        if (!CouponStatus.FINISHED.equals(couponEntity.getStatus())) {
            throw new RuntimeException("Допустимо продлевать только истекшие акции");
        }

        if (dateTime == null) {
            throw new FieldValidationException("Не указана дата");
        }

        if (LocalDateTime.now().isAfter(dateTime)) {
            throw new FieldValidationException("Должна быть указана будущая дата");
        }

    }


    /**
     * Позволяет заполнить Entity акции из domain объекта
     *
     * @param coupon domain акция
     * @return неперсистентный объект CouponEntity
     */
    private CouponEntity fillCouponEntity(Coupon coupon) {
        CouponEntity couponEntity = new CouponEntity();

        if (coupon.getRegion() != null && coupon.getRegion().getId() != null) {
            RegionEntity regionEntity = regionRepository.findOne(coupon.getRegion().getId());
            couponEntity.setRegion(regionEntity);
        }

        if (coupon.getCategory() != null && coupon.getCategory().getId() != null) {
            CategoryEntity categoryEntity = categoryRepository.findOne(coupon.getCategory().getId());
            couponEntity.setCategory(categoryEntity);
        }

        couponEntity.setTitle(coupon.getTitle());
        couponEntity.setImages(coupon.getImages());
        couponEntity.setDiscount(coupon.getDiscount());
        couponEntity.setPrice(coupon.getPrice());
        couponEntity.setGiftTitle(coupon.getGiftTitle());
        couponEntity.setGiftImage(coupon.getGiftImage());
        couponEntity.setGiftDescription(coupon.getGiftDescription());
        couponEntity.setGiftCondition(coupon.getGiftCondition());
        couponEntity.setDescription(coupon.getDescription());

        couponEntity.setBackgroundColorHex(coupon.getBackgroundColorHex());

        couponEntity.setFlagColorHex(coupon.getFlagColorHex());


        return couponEntity;
    }


    /**
     * Позволяет получить указанное число случайных пользователей,
     * не добавивших в карман акцию
     *
     * @param coupon акция
     * @param limit  максимальное число пользователей в результате
     * @return List<Customer>
     */
    public List<Customer> getRandomCustomersWhoHaveNotCouponInFavorites(Coupon coupon, int limit, Long region_id) {
        final List<Customer> result = new ArrayList<>();

        if (limit < 1) {
            return result;
        }

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        List<CustomerEntity> notFavored;

        if(region_id == 0) {
            notFavored = customerRepository.findAllNotFavored(couponEntity);
        } else {
            List<Long> regionChildsId;

            RegionEntity region = regionRepository.findOne(region_id);

            if (region.getParent() == null) {
                regionChildsId = region.getChildsId();
                notFavored = customerRepository.findAllNotFavoredInRegions(couponEntity, regionChildsId);
            } else {
                notFavored = customerRepository.findAllNotFavoredInRegion(couponEntity, region_id);
            }
        }
        if (notFavored.size() <= limit) {
            notFavored.forEach(c -> result.add(c.toDomain()));
        } else {
            Collections.shuffle(notFavored);

            for (CustomerEntity customerEntity : notFavored) {
                result.add(customerEntity.toDomain());

                if (result.size() == limit) {
                    break;
                }
            }
        }

        return result;
    }


    private void fillCouponEntityFromDomainContacts(CouponEntity couponEntity, Set<CouponContact> contacts) {
        for (CouponContact contact : contacts) {
            CouponContactEntity contactEntity = new CouponContactEntity(contact);
            contactEntity.setCoupon(couponEntity);
            contactEntity.setLongitude(contact.getLongitude());
            contactEntity.setLatitude(contact.getLatitude());
            couponEntity.getContacts().add(contactEntity);
        }
    }
}
