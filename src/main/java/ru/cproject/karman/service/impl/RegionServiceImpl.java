package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.cproject.karman.dao.jpa.query.common.RegionQuery;
import ru.cproject.karman.dao.jpa.repositories.common.RegionRepository;
import ru.cproject.karman.dao.jpa.specifications.RegionSpecifications;
import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.common.RegionEntity;
import ru.cproject.karman.service.RegionService;
import ru.cproject.karman.validator.FieldValidator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 02.06.16
 */
@Service
public class RegionServiceImpl implements RegionService {

    private RegionRepository regionRepository;


    @Autowired
    public void setRegionRepository(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public Region save(Region region) {
        validateSave(region);
        RegionEntity regionEntity = new RegionEntity();
        if (region.getParent() != null && region.getParent().getId() != null) {
            RegionEntity parentRegionEntity = regionRepository.findOne(region.getParent().getId());
            regionEntity.setParent(parentRegionEntity);
        }
        regionEntity.setTitle(region.getTitle());
        return regionRepository.save(regionEntity).toDomain();
    }

    @Override
    public Region findOne(Long id) {
        return regionRepository.findOne(id).toDomain();
    }

    @Override
    public void delete(Long id) {
        validateDelete(id);
        regionRepository.delete(id);
    }

    private void validateSave(Region region) {
        if (!FieldValidator.validateStringNotEmpty(region.getTitle())) {
            throw new FieldValidationException("Не указано название");
        }
    }


    private void validateDelete(Long id) {
        if (!regionRepository.exists(id)) {
            throw new RuntimeException("Указанного региона не существует");
        }
    }

    @Override
    public List<Region> getAll(RegionQuery query) {
        return regionRepository.findAll(
            RegionSpecifications.generateSearchSpecifications(query),
            query.getPageRequest()
        ).getContent().stream()
            .map(RegionEntity::toDomain)
            .collect(Collectors.toList());
    }

}
