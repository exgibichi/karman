package ru.cproject.karman.service.impl;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cproject.karman.exception.SmsException;
import ru.cproject.karman.service.SmsService;
import ru.cproject.karman.util.HttpUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by ioann on 25.05.16
 */
@Service
public class SmsServiceImpl implements SmsService {

    @Value("${smsc.ru.login}")
    private String login;

    @Value("${smsc.ru.password}")
    private String password;

    @Value("${smsc.ru.sender}")
    private String sender;


    @Override
    public void sendMessage(String message, String phoneNumber)
        throws IOException {
        HttpGet httpGet = prepareRequest(message, phoneNumber);
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(httpGet);

        try {
            JSONObject jsonResponse = HttpUtils.getJsonFromHttpResponse(response);

            if (jsonResponse.get("error") != null) {
                //Произошла ошибка - передаем наверх, так как все логгеры в web слое
                throw new SmsException("Error response from smsc.ru: "
                    + jsonResponse.toJSONString());
            } else {
                if (jsonResponse.get("id") == null || jsonResponse.get("cnt") == null) {
                    throw new SmsException("Unexpected response from smsc.ru: "
                        + jsonResponse.toJSONString());
                }
            }

        } catch (ParseException e) {
            throw new SmsException(e.getMessage(), e);
        }

    }


    private HttpGet prepareRequest(String message, String phoneNumber) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder()
            .append("http://smsc.ru/sys/send.php")
            .append("?login=").append(login)
            .append("&psw=").append(password)
            .append("&phones=").append(phoneNumber)
            .append("&mes=").append(URLEncoder.encode(message, StandardCharsets.UTF_8.toString()))
            .append("&sender=").append(URLEncoder.encode(sender, StandardCharsets.UTF_8.toString()))
            .append("&charset=utf-8")
            //В ответе ждем JSON
            .append("&fmt=3");

        return new HttpGet(sb.toString());
    }

}
