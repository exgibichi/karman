package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.query.common.CategoryQuery;
import ru.cproject.karman.dao.jpa.repositories.common.CategoryRepository;
import ru.cproject.karman.dao.jpa.specifications.CategorySpecifications;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.common.CategoryEntity;
import ru.cproject.karman.service.CategoryService;
import ru.cproject.karman.validator.FieldValidator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 30.05.16
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    @Transactional
    public Category save(Category category) {
        validateToSave(category);
        CategoryEntity categoryEntity = new CategoryEntity(category);
        return categoryRepository.save(categoryEntity).toDomain();
    }

    @Override
    @Transactional
    public void update(Long id, String title) {
        CategoryEntity categoryEntity = categoryRepository.findOneById(id);
        categoryEntity.setTitle(title);
        categoryRepository.save(categoryEntity);
    }

    @Override
    public void delete(Long id) {
        validateDelete(id);
        categoryRepository.delete(id);
    }

    private void validateDelete(Long id) {
        if (!categoryRepository.exists(id)) {
            throw new RuntimeException("Указанной категории не существует");
        }
    }

    @Override
    public List<Category> getAll(CategoryQuery query) {
        return categoryRepository.findAll(
            CategorySpecifications.generateSearchSpecifications(query),
            query.getPageRequest() )
            .getContent()
            .stream()
            .map(CategoryEntity::toDomain)
            .collect(Collectors.toList());
    }


    private void validateToSave(Category category) {
        if (!FieldValidator.validateStringNotEmpty(category.getTitle())) {
            throw new FieldValidationException("Не указан заголовок категории");
        }

        CategoryEntity categoryEntity = categoryRepository.findOneByTitle(category.getTitle());

        if (categoryEntity != null) {
            throw new FieldValidationException("Категория с таким заголовком уже существует");
        }
    }

}
