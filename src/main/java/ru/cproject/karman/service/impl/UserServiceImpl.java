package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.model.account.UserEntity;
import ru.cproject.karman.service.UserService;

/**
 * Created by ioann on 01.06.16
 */
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;


    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User getByPhoneNumber(String phoneNumber) {
        return userRepository.findOneByPhoneNumber(phoneNumber).toDomain();
    }

    @Override
    @Transactional
    public User edit(User user) {
        UserEntity userEntity = userRepository.findOne(user.getId());
        userEntity.setToken(user.getToken());
        userEntity.setRegion(user.getRegion());
        return userRepository.save(userEntity).toDomain();
    }
}
