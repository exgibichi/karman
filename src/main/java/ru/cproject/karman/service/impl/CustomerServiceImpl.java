package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.account.CompanyRepository;
import ru.cproject.karman.dao.jpa.repositories.account.CustomerRepository;
import ru.cproject.karman.dao.jpa.repositories.business.CouponRepository;
import ru.cproject.karman.dao.jpa.repositories.business.FavoriteCouponRepository;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;
import ru.cproject.karman.service.CustomerService;
import ru.cproject.karman.service.PushService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by ioann on 16.06.16
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    private CouponRepository couponRepository;
    private CompanyRepository companyRepository;
    private PushService pushService;
    private FavoriteCouponRepository favoriteCouponRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Autowired
    public void setPushService(PushService pushService) {
        this.pushService = pushService;
    }

    @Autowired
    public void setCompanyRepository(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Autowired
    public void setFavoriteCouponRepository(FavoriteCouponRepository favoriteCouponRepository) {
        this.favoriteCouponRepository = favoriteCouponRepository;
    }

    @Override
    @Transactional
    public boolean addCouponToFavorites(Customer customer, Coupon coupon, boolean forceAdding) {
        if(customer.getId() == null || coupon.getId() == null) {
           return false;
        }
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        if (!CouponStatus.ACTIVE.equals(couponEntity.getStatus())) {
            throw new RuntimeException("Добавлять в карман можно только активные акции");
        }

        if (favoriteCouponRepository.couponExistsInFavorites(customerEntity, couponEntity)) {
            return false;
        }

        FavoriteCouponEntity favoriteCouponEntity = new FavoriteCouponEntity();
        favoriteCouponEntity.setCoupon(couponEntity);
        favoriteCouponEntity.setCustomer(customerEntity);
        favoriteCouponEntity.setCreated(LocalDateTime.now());
        favoriteCouponEntity.setShowed(!forceAdding);

        if(forceAdding) {
            String token = customerEntity.getUser().getToken();
            if(token != null) {
                try {
                    pushService.push(token, "В ваш карман добавлена новая акция!", coupon.getId());
                } catch (Exception e) {
                    System.out.println("e:"+e.getMessage());
                }
            }
        }

        customerEntity.getFavoriteCoupons().add(favoriteCouponEntity);
        customerRepository.save(customerEntity);

        return true;
    }


    @Override
    @Transactional
    public boolean removeCouponFromFavorites(Customer customer, Coupon coupon) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        if (!favoriteCouponRepository.couponExistsInFavorites(customerEntity, couponEntity)) {
            return false;
        }

        FavoriteCouponEntity favoriteCouponEntity = favoriteCouponRepository
            .findByCouponAndCustomer(couponEntity, customerEntity);

        customerEntity.getFavoriteCoupons().remove(favoriteCouponEntity);
        customerRepository.save(customerEntity);

        return true;
    }


    @Override
    @Transactional
    public Long addCouponToLiked(Customer customer, Coupon coupon) {

        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());


        if (isLiked(customer, coupon)) {
            return couponEntity.getLikesCount();
        }

        if (!CouponStatus.ACTIVE.equals(couponEntity.getStatus())) {
            throw new RuntimeException("Поставить лайк можно только активной акции");
        }

        customerEntity.getLikedCoupons().add(couponEntity);
        customerRepository.save(customerEntity);
        couponRepository.incLike(couponEntity);

        return couponEntity.getLikesCount() + 1;
    }

    @Override
    @Transactional
    public boolean addCouponToWatched(Customer customer, Coupon coupon) {

        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());


        if (isWatched(customer, coupon)) {
            return true;
        }

        customerEntity.getWatchedCoupons().add(couponEntity);
        customerRepository.save(customerEntity);

        return true;
    }


    @Override
    @Transactional
    public Long removeCouponFromLiked(Customer customer, Coupon coupon) {

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        if (!isLiked(customer, coupon)) {
            return couponEntity.getLikesCount();
        }

        if (!CouponStatus.ACTIVE.equals(couponEntity.getStatus())) {
            throw new RuntimeException("Снять лайк можно только с активной акции");
        }

        customerEntity.getLikedCoupons().remove(couponEntity);
        customerRepository.save(customerEntity);
        couponRepository.decLike(couponEntity);

        return couponEntity.getLikesCount() - 1;
    }


    @Override
    @Transactional
    public boolean subscribeToCompany(Customer customer, Company company) {

        if (isCustomerSubscribedToCompany(customer, company)) {
            return false;
        }

        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());

        customerEntity.getSubscriptions().add(companyEntity);
        customerRepository.save(customerEntity);

        return true;
    }

    @Override
    @Transactional
    public boolean unsubscribeFromCompany(Customer customer, Company company) {

        if (!isCustomerSubscribedToCompany(customer, company)) {
            return false;
        }

        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());

        customerEntity.getSubscriptions().remove(companyEntity);
        customerRepository.save(customerEntity);

        return true;
    }


    @Override
    @Transactional(readOnly = true)
    public boolean isLiked(Customer customer, Coupon coupon) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        return customerRepository.couponExistInLiked(customerEntity, couponEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isWatched(Customer customer, Coupon coupon) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        return customerRepository.couponExistInWatched(customerEntity, couponEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isFavorite(Customer customer, Coupon coupon) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());

        return favoriteCouponRepository.couponExistsInFavorites(customerEntity, couponEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public int getCountOfFavoriteByCustomer(Customer customer) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        return favoriteCouponRepository.countOfFavoriteByCustomer(customerEntity);
    }


    @Override
    @Transactional
    public int getCountOfFavoriteByCustomerAndAreNotShowed(Customer customer) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        int result = favoriteCouponRepository.countOfFavoriteByCustomerAndAreNotShowed(customerEntity);
        //favoriteCouponRepository.updateFavoritesByUserSetShowedWhereIsNotShowed(customerEntity);

        return result;

    }

    @Override
    public boolean isCustomerSubscribedToCompany(Customer customer, Company company) {
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());

        return customerRepository.isCustomerSubscribedToCompany(customerEntity, companyEntity);
    }

}
