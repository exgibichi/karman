package ru.cproject.karman.service.impl;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cproject.karman.service.PushService;
import ru.cproject.karman.util.HttpUtils;

@Service
public class PushServiceImpl implements PushService {
    private static final Logger LOGGER = LogManager.getLogger(PushServiceImpl.class.getName());
    private static final String SEND_PUSH_ERROR = "Ошбика при отправке push уведомлений";

    @Value("${gcm.api.key}")
    private String api_key;

    @Override
    public void push(String token, String message, Long couponId) {
        try {
            HttpResponse httpResponse = executeSendPushRequest(token, message, couponId);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                JSONObject jsonResponse = HttpUtils.getJsonFromHttpResponse(httpResponse);
                if (jsonResponse.get("errors") != null) {
                    LOGGER.log(Level.WARN, SEND_PUSH_ERROR + ".\n Тело ответа: " + jsonResponse.toJSONString());
                }
            } else {
                LOGGER.log(Level.ERROR, SEND_PUSH_ERROR + ".\n " +
                        "Тело ответа: " + EntityUtils.toString(httpResponse.getEntity()));
            }
        } catch (Exception e) {
            LOGGER.log(Level.ERROR, SEND_PUSH_ERROR + ": " + e.getMessage(), e);
        }
    }
    private HttpResponse executeSendPushRequest(String token, String message, Long couponId) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            return httpClient.execute(
                    prepareSendPushRequest(token, message, couponId)
            );
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    private HttpPost prepareSendPushRequest(String token, String message, Long couponId) {
        HttpPost httpPost = new HttpPost("https://fcm.googleapis.com/fcm/send");
        //Выставляем заголовки
        httpPost.addHeader("Authorization","key="+api_key);
        httpPost.addHeader("Content-Type","application/json; charset=UTF-8");
        //Подготавливаем тело запроса
        JSONObject requestBody = new JSONObject();
        requestBody.put("to", token);
        //Идентификаторы устройств
        //Контент
        //Заголовок уведомления
        JSONObject data = new JSONObject();
        data.put("cid", couponId);
        JSONObject notify = new JSONObject();
        notify.put("title", "Карман");
        notify.put("body", message);
        requestBody.put("notification", notify);
        requestBody.put("data", data);

        StringEntity requestEntity = new StringEntity(requestBody.toJSONString(), ContentType.APPLICATION_JSON);
        httpPost.setEntity(requestEntity);
        return httpPost;
    }
}