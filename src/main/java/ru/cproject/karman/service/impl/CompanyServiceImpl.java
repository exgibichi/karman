package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.query.account.CompanyQuery;
import ru.cproject.karman.dao.jpa.repositories.account.CompanyRepository;
import ru.cproject.karman.dao.jpa.repositories.account.CustomerRepository;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.dao.jpa.repositories.common.CategoryRepository;
import ru.cproject.karman.dao.jpa.specifications.CompanySpecifications;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.account.UserEntity;
import ru.cproject.karman.model.common.CategoryEntity;
import ru.cproject.karman.service.CompanyService;
import ru.cproject.karman.validator.FieldValidator;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 06.06.16
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Value("${saleband.imagepath}")
    private String imagePath;

    private CompanyRepository companyRepository;
    private CategoryRepository categoryRepository;
    private UserRepository userRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public void setCompanyRepository(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Company get(Long companyId) {
        validateGet(companyId);
        return companyRepository.findOne(companyId).toDomain();
    }


    @Override
    @Transactional(readOnly = true)
    public List<Company> getAll() {
        return companyRepository.findAll()
            .stream()
            .map(CompanyEntity::toDomain)
            .collect(Collectors.toList());
    }

    @Override
    public List<Company> getPageByQuery(CompanyQuery query) {
        return companyRepository.findAll(
            CompanySpecifications.generateSearchSpecifications(query),
            query.getPageRequest()
        ).getContent()
            .stream()
            .map(CompanyEntity::toDomain)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Company> getAllToReview() {
        return companyRepository.findAllToReview()
            .stream()
            .map(CompanyEntity::toDomain)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional
    public Company edit(Company company) {
        validateEdit(company);

        UserEntity userEntity = userRepository.findOne(company.getUser().getId());
        CompanyEntity companyEntity = userEntity.getCompany();

        //Обновляем данные, не требующие модерации
        updateNotModeratingFields(company, companyEntity);

        //Обновляем данные, которые уйдут на модерацию
        if (!companyEntity.toDomain().inModeration() && needInModeration(company, companyEntity)) {
            updateFieldsForModeration(company, companyEntity);
        }

        return userRepository.save(userEntity).getCompany().toDomain();
    }


    @Override
    @Transactional
    public String updateAvatar(String avatarFileName, Long companyId) {
        validateUpdateAvatar(avatarFileName, companyId);

        CompanyEntity companyEntity = companyRepository.findOne(companyId);
        companyEntity.setLogo(avatarFileName);
        companyRepository.save(companyEntity);

        return avatarFileName;
    }


    @Override
    @Transactional
    public void deleteAvatar(Company company) {
        CompanyEntity companyEntity = companyRepository.findOne(company.getId());

        if (companyEntity.getLogo() == null) {
            //Аватарки и так нет, выходим
            return;
        }

        companyEntity.setLogo(null);
        companyRepository.save(companyEntity);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Long> getSubscriptionIdsByCustomer(Customer customer) {
        return customerRepository.findOne(customer.getId())
            .getSubscriptions()
            .stream()
            .map(CompanyEntity::getId)
            .collect(Collectors.toList());
    }


    @Override
    @Transactional
    public void rejectModeration(Long companyId, String moderatorsComment) {
        validateRejectionOfModeration(companyId, moderatorsComment);

        CompanyEntity companyEntity = companyRepository.findOne(companyId);

        companyEntity.setModeratorsComment(moderatorsComment);

        companyEntity.setNewName(null);
        companyEntity.setNewEmail(null);
        companyEntity.setNewFullName(null);

        companyRepository.save(companyEntity);
    }

    @Override
    @Transactional
    public void confirmModeration(Long companyId) {
        validateConfirmationOfModeration(companyId);

        CompanyEntity companyEntity = companyRepository.findOne(companyId);
        UserEntity userEntity = companyEntity.getUser();

        if (!companyEntity.isActive()) {
            companyEntity.setActive(true);
        }

        companyEntity.setModeratorsComment(null);

        if (companyEntity.getNewName() != null) {
            userEntity.setName(companyEntity.getNewName());
        }

        if (companyEntity.getNewEmail() != null) {
            companyEntity.setEmail(companyEntity.getNewEmail());
        }

        if (companyEntity.getNewFullName() != null) {
            companyEntity.setFullName(companyEntity.getNewFullName());
        }

        companyEntity.setNewName(null);
        companyEntity.setNewEmail(null);
        companyEntity.setNewFullName(null);

        userRepository.save(userEntity);
    }

    private void updateNotModeratingFields(Company company, CompanyEntity companyEntity) {
        Category category = company.getCategory();

        CategoryEntity categoryEntity = null;

        if (category != null && category.getId() != null) {
            categoryEntity = categoryRepository.findOne(category.getId());
        }
        companyEntity.setCategory(categoryEntity);

        companyEntity.setDescription(company.getDescription());
        companyEntity.setSiteUrl(company.getSiteUrl());
        companyEntity.setAddress(company.getAddress());
        companyEntity.setLongitude(company.getLongitude());
        companyEntity.setLatitude(company.getLatitude());
        companyEntity.setContactPhoneNumber(company.getContactPhoneNumber());
    }


    private void updateFieldsForModeration(Company company, CompanyEntity companyEntity) {
        companyEntity.setNewName(company.getUser().getName());
        companyEntity.setNewEmail(company.getEmail());
        companyEntity.setNewFullName(company.getFullName());

        companyEntity.setModeratorsComment(null);
    }

    /**
     * Позволяет определить, нуждаются ли новые данны компании в модерации
     *
     * @param company       компания, полученная из DTO
     * @param companyEntity компания, полученная из базы
     */
    private boolean needInModeration(Company company, CompanyEntity companyEntity) {

        if (!company.getUser().getName().equals(companyEntity.getUser().getName())) {
            return true;
        }

        if (!company.getEmail().equals(companyEntity.getEmail())) {
            return true;
        }

        if (!company.getFullName().equals(companyEntity.getFullName())) {
            return true;
        }

        return false;
    }

    private void validateGet(Long companyId) {
        if (companyId == null) {
            throw new FieldValidationException("Не указана компания");
        } else if (!companyRepository.exists(companyId)) {
            throw new FieldValidationException("Указанной компании не существует");
        }
    }

    private void validateEdit(Company company) {
        if (company.getId() == null) {
            throw new FieldValidationException("Не указана компания");
        }

        if (!companyRepository.exists(company.getId())) {
            throw new FieldValidationException("Указанной компании не существует");
        }

        User user = company.getUser();

        if (!FieldValidator.validateStringNotEmpty(user.getName())) {
            throw new FieldValidationException("У компании должно быть название");
        }

        Category category = company.getCategory();

        if (category != null && category.getId() != null) {
            if (!categoryRepository.exists(category.getId())) {
                throw new FieldValidationException("Указанной категории не существует");
            }
        }

    }


    public void validateUpdateAvatar(String avatarFileName, Long companyId) {

        if (companyId == null) {
            throw new FieldValidationException("Не указана компания");
        }

        if (!companyRepository.exists(companyId)) {
            throw new FieldValidationException("Указанной компании не существует");
        }

        if (!FieldValidator.validateStringNotEmpty(avatarFileName)) {
            throw new FieldValidationException("Отсутствует изображение");
        }

        File file = new File(imagePath.substring(5), avatarFileName);

        if (!file.exists()) {
            throw new FieldValidationException("Указанного изображения не существует");
        }

    }


    private void validateRejectionOfModeration(Long companyId, String moderatorsComment) {

        if (companyId == null) {
            throw new FieldValidationException("Не указана компания");
        } else if (!companyRepository.exists(companyId)) {
            throw new FieldValidationException("Указанной компании не существует");
        }

        CompanyEntity companyEntity = companyRepository.findOne(companyId);

        if (!companyEntity.toDomain().inModeration()) {
            throw new FieldValidationException("Учетная запись компании больше не находится в модерации. Невозможно осуществить отказ.");
        }

        if (!FieldValidator.validateStringNotEmpty(moderatorsComment)) {
            throw new FieldValidationException("Не введена причина отказа");
        }

    }


    private void validateConfirmationOfModeration(Long companyId) {
        if (companyId == null) {
            throw new FieldValidationException("Не указана компания");
        } else if (!companyRepository.exists(companyId)) {
            throw new FieldValidationException("Указанной компании не существует");
        }

        CompanyEntity companyEntity = companyRepository.findOne(companyId);

        if (!companyEntity.toDomain().inModeration()) {
            throw new FieldValidationException("Учетная запись компании больше не находится в модерации. Невозможно осуществить подтверждение.");
        }

    }

}
