package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.conf.security.PasswordGenerator;
import ru.cproject.karman.conf.security.RoleEnum;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.exception.SmsException;
import ru.cproject.karman.exception.UserAlreadyExistsException;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.account.UserEntity;
import ru.cproject.karman.service.RegistrationService;
import ru.cproject.karman.service.SmsService;
import ru.cproject.karman.validator.FieldValidator;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by ioann on 25.05.16
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private SmsService smsService;
    private PasswordEncoder passwordEncoder;
    private PasswordGenerator passwordGenerator;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setPasswordGenerator(PasswordGenerator passwordGenerator) {
        this.passwordGenerator = passwordGenerator;
    }


    @Override
    @Transactional
    public void register(String phoneNumber, String name, RoleEnum roleEnum,
                         String email, String fullName, Long region_id) {
        //Валидация
        validate(phoneNumber, name, roleEnum, email, fullName);

        //Создание нового юзера
        UserEntity userEntity = new UserEntity();
        userEntity.setName(name.trim());
        userEntity.setPhoneNumber(phoneNumber.trim());
        userEntity.setRegistrationDate(LocalDateTime.now());
        userEntity.setRegion(region_id);

        String password = passwordGenerator.generatePassword();
        userEntity.setPasswordHash(passwordEncoder.encode(password));

        if (RoleEnum.CUSTOMER.equals(roleEnum)) {
            userEntity.setCustomer(new CustomerEntity());
        } else if (RoleEnum.COMPANY.equals(roleEnum)) {
            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.setEmail(email);
            companyEntity.setFullName(fullName);
            userEntity.setCompany(companyEntity);
        }

        userRepository.save(userEntity);

        //Отправка SMS
        try {
            smsService.sendMessage("Ваш пароль для входа в Карман: " + password, phoneNumber);
        } catch (IOException e) {
            throw new SmsException(e.getMessage(), e);
        }
    }


    private void validate(String phoneNumber, String name, RoleEnum roleEnum,
                          String email, String fullName) {

        if (!RoleEnum.CUSTOMER.equals(roleEnum) && !RoleEnum.COMPANY.equals(roleEnum)) {
               throw new FieldValidationException("Недопустимая роль");
        }

        FieldValidator.validatePhoneNumber(phoneNumber);

        if (name == null || name.isEmpty()) {
            throw new FieldValidationException("Не было введено имя");
        }

        UserEntity userEntity = userRepository.findOneByPhoneNumber(phoneNumber.trim());

        if (userEntity != null) {
            throw new UserAlreadyExistsException("Пользователь с таким телефоном уже существует");
        }

        if (RoleEnum.COMPANY.equals(roleEnum)) {

            if (!FieldValidator.validateStringNotEmpty(email)) {
                throw new UserAlreadyExistsException("Не был введен email");
            }

            if (!FieldValidator.validateStringNotEmpty(fullName)) {
                throw new UserAlreadyExistsException("Не были введены Ф.И.О.");
            }

        }

    }

}

