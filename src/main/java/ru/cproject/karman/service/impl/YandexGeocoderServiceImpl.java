package ru.cproject.karman.service.impl;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;
import ru.cproject.karman.domain.yandex.Coordinates;
import ru.cproject.karman.service.YandexGeocoderService;
import ru.cproject.karman.util.HttpUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by ioann on 07.07.16
 */
@Service
public class YandexGeocoderServiceImpl implements YandexGeocoderService {

    private static final Logger LOGGER = Logger.getLogger(YandexGeocoderServiceImpl.class.getName());

    @Override
    public Coordinates getCoordinatesByAddress(String address) {

        try {
            Coordinates coordinates = new Coordinates();

            HttpClient httpClient = HttpClientBuilder.create().build();

            JSONObject httpJsonResponse = HttpUtils.getJsonFromHttpResponse(
                httpClient.execute(prepareGetRequestForGeocoder(address))
            );

            if (httpJsonResponse.containsKey("error")) {
                throw new RuntimeException("Yandex api error: " + httpJsonResponse.toJSONString());
            }

            String fullCoordinatesString = extractCoordinatesFieldFromHttpJsonResponse(httpJsonResponse);

            if (fullCoordinatesString == null) {
                return null;
            }

            String coordinatesArray[] = fullCoordinatesString.split("\\s+");

            coordinates.setLongitude(coordinatesArray[0]);
            coordinates.setLatitude(coordinatesArray[1]);

            return coordinates;
        } catch (Exception e) {

            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    private HttpGet prepareGetRequestForGeocoder(String geocode) throws Exception {

        StringBuilder sb = new StringBuilder("https://geocode-maps.yandex.ru/1.x/");
        sb.append("?geocode=")
            .append(URLEncoder.encode(geocode, StandardCharsets.UTF_8.toString()))
            .append("&format=json");

        HttpGet httpGet = new HttpGet(sb.toString());
        return httpGet;
    }


    private String extractCoordinatesFieldFromHttpJsonResponse(JSONObject httpJsonResponse) {
        JSONObject response = (JSONObject) httpJsonResponse.get("response");
        JSONObject geoObjectCollection = (JSONObject) response.get("GeoObjectCollection");

        //Определяем количество результатов
        JSONObject metaDataProperty = (JSONObject) geoObjectCollection.get("metaDataProperty");
        JSONObject geocoderResponseMetaData = (JSONObject) metaDataProperty.get("GeocoderResponseMetaData");

        int resultsFound = Integer.valueOf((String)geocoderResponseMetaData.get("found"));

        if (resultsFound != 1) {
            return null;
        }

        JSONArray featureMember = (JSONArray) geoObjectCollection.get("featureMember");
        JSONObject resultObject = (JSONObject) featureMember.get(0);
        JSONObject geoObject = (JSONObject) resultObject.get("GeoObject");
        JSONObject point = (JSONObject) geoObject.get("Point");
        return (String)point.get("pos");
    }

}
