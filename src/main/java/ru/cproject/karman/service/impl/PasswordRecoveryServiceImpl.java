package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.conf.security.PasswordGenerator;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.exception.SmsException;
import ru.cproject.karman.model.account.UserEntity;
import ru.cproject.karman.service.PasswordRecoveryService;
import ru.cproject.karman.service.SmsService;
import ru.cproject.karman.validator.FieldValidator;

import java.io.IOException;

/**
 * Created by ioann on 27.05.16
 */
@Service
public class PasswordRecoveryServiceImpl implements PasswordRecoveryService {

    private UserRepository userRepository;
    private PasswordGenerator passwordGenerator;
    private PasswordEncoder passwordEncoder;
    private SmsService smsService;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordGenerator(PasswordGenerator passwordGenerator) {
        this.passwordGenerator = passwordGenerator;
    }

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    @Transactional
    public void recoverPassword(String phoneNumber) {
        validate(phoneNumber);

        UserEntity userEntity = userRepository.findOneByPhoneNumber(phoneNumber);

        if (userEntity == null) {
            throw new RuntimeException("Пользователь с таким номером не зарегистирирован");
        }

        String newPassword = passwordGenerator.generatePassword();
        userEntity.setPasswordHash(passwordEncoder.encode(newPassword));
        userRepository.save(userEntity);

        try {
            smsService.sendMessage("Ваш пароль для входа в Карман: " + newPassword, phoneNumber);
        } catch (IOException e) {
            throw new SmsException(e.getMessage(), e);
        }
    }


    private void validate(String phoneNumber) {
        FieldValidator.validatePhoneNumber(phoneNumber);
    }

}
