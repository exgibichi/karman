package ru.cproject.karman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.query.business.comment.CouponCommentQuery;
import ru.cproject.karman.dao.jpa.repositories.account.CustomerRepository;
import ru.cproject.karman.dao.jpa.repositories.business.CouponCommentRepository;
import ru.cproject.karman.dao.jpa.repositories.business.CouponRepository;
import ru.cproject.karman.dao.jpa.specifications.CouponCommentSpecifications;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponComment;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponCommentEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.service.CouponCommentService;
import ru.cproject.karman.validator.FieldValidator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 05.07.16
 */
@Service
public class CouponCommentServiceImpl implements CouponCommentService {

    private CouponCommentRepository couponCommentRepository;
    private CouponRepository couponRepository;
    private CustomerRepository customerRepository;


    @Autowired
    public void setCouponCommentRepository(CouponCommentRepository couponCommentRepository) {
        this.couponCommentRepository = couponCommentRepository;
    }

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    @Transactional
    public CouponComment addCommentToCoupon(CouponComment couponComment) {

        validateAddingOfComment(couponComment);

        CouponEntity couponEntity = couponRepository.findOne(couponComment.getCoupon().getId());
        CustomerEntity customerEntity = customerRepository.findOne(couponComment.getCustomer().getId());

        CouponCommentEntity commentEntity = new CouponCommentEntity();

        commentEntity.setText(couponComment.getText());
        commentEntity.setCoupon(couponEntity);
        commentEntity.setCustomer(customerEntity);
        commentEntity.setCreated(LocalDateTime.now());

        couponRepository.incCommentsCount(couponEntity);

        return couponCommentRepository.save(commentEntity).toDomain();
    }


    @Override
    @Transactional(readOnly = true)
    public boolean isCouponCommentedByCustomer(Coupon coupon, Customer customer) {
        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        CustomerEntity customerEntity = customerRepository.findOne(customer.getId());

        return couponCommentRepository.isCouponCommentedByCustomer(couponEntity, customerEntity);
    }


    @Override
    @Transactional(readOnly = true)
    public List<CouponComment> getPageByQuery(CouponCommentQuery query) {
        return couponCommentRepository.findAll(
            CouponCommentSpecifications.generateSpecificationsFromQuery(query),
            query.getPageRequest()
        ).getContent()
            .stream()
            .map(CouponCommentEntity::toDomain)
            .collect(Collectors.toList());
    }

    private void validateAddingOfComment(CouponComment couponComment) {

        if (isCouponCommentedByCustomer(couponComment.getCoupon(), couponComment.getCustomer())) {
            throw new RuntimeException("Одну акцию можно комментировать не более одного раза");
        }

        if (!couponRepository.exists(couponComment.getCoupon().getId())) {
            throw new FieldValidationException("Указанной акции не существует");
        }

        if (!FieldValidator.validateStringNotEmpty(couponComment.getText())) {
            throw new FieldValidationException("Не указан текст комментария");
        }
    }

}
