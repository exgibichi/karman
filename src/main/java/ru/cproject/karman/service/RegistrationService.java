package ru.cproject.karman.service;

import ru.cproject.karman.conf.security.RoleEnum;

/**
 * Created by ioann on 25.05.16.
 * Интерфейс сервиса регистрации
 */
public interface RegistrationService {

    void register(String phoneNumber, String name, RoleEnum roleEnum,
                  String email, String fullName, Long region_id);

}
