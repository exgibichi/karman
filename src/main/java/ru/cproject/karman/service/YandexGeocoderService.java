package ru.cproject.karman.service;

import ru.cproject.karman.domain.yandex.Coordinates;

/**
 * Created by ioann on 07.07.16
 */
public interface YandexGeocoderService {

    /**
     * Позволяет получить объект с широтой и долготой указанного адреса от yandex'а
     * @param address адрес
     * @return Coordinates, или null, если число результатов не равно 1 или произошла ошибка
     */
    Coordinates getCoordinatesByAddress(String address);
}
