package ru.cproject.karman.service.statistics.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.business.CouponRepository;
import ru.cproject.karman.dao.jpa.repositories.business.CouponReviewRepository;
import ru.cproject.karman.dao.jpa.repositories.business.FavoriteCouponRepository;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.statistics.LocalDateTimeAndCountStats;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.CouponReviewEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;
import ru.cproject.karman.service.statistics.CouponStatisticsService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 11.07.16
 */
@Service
public class CouponStatisticsServiceImpl implements CouponStatisticsService {

    private CouponRepository couponRepository;
    private FavoriteCouponRepository favoriteCouponRepository;
    private CouponReviewRepository couponReviewRepository;


    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Autowired
    public void setFavoriteCouponRepository(FavoriteCouponRepository favoriteCouponRepository) {
        this.favoriteCouponRepository = favoriteCouponRepository;
    }

    @Autowired
    public void setCouponReviewRepository(CouponReviewRepository couponReviewRepository) {
        this.couponReviewRepository = couponReviewRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocalDateTimeAndCountStats> getFavoriteStatsByLastWeek(Coupon coupon) {

        LocalDateTime beginDate = LocalDateTime.now()
            .truncatedTo(ChronoUnit.DAYS)
            .minusDays(6);

        List<LocalDateTimeAndCountStats> result = prepareLocalDateTimeAndCountStatsList(
            beginDate, null
        );

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        List<FavoriteCouponEntity> favoriteCouponEntities = favoriteCouponRepository
            .findAllByCouponAndCreatedGreaterThanEqualOrderByCreatedAsc(couponEntity, beginDate);


        //Наполнение статистики
        favoriteCouponEntities.forEach(
            f -> {
                LocalDateTime truncatedToDaysCreated = f.getCreated().truncatedTo(ChronoUnit.DAYS);

                result.stream()
                    .filter(l ->
                        truncatedToDaysCreated.equals(l.getLocalDateTime())
                    ).findFirst()
                    .get()
                    .incCount();
            }
        );

        return result;
    }


    @Override
    public List<LocalDateTimeAndCountStats> getReviewStatsByLastWeek(Coupon coupon) {
        LocalDateTime beginDate = LocalDateTime.now()
            .truncatedTo(ChronoUnit.DAYS)
            .minusDays(6);

        List<LocalDateTimeAndCountStats> result = prepareLocalDateTimeAndCountStatsList(
            beginDate, null
        );

        CouponEntity couponEntity = couponRepository.findOne(coupon.getId());
        List<CouponReviewEntity> favoriteCouponEntities = couponReviewRepository
            .findAllByCouponAndDateGreaterThanEqualOrderByDateAsc(couponEntity, beginDate);


        //Наполнение статистики
        favoriteCouponEntities.forEach(
            f -> {
                LocalDateTime truncatedToDaysCreated = f.getDate().truncatedTo(ChronoUnit.DAYS);

                result.stream()
                    .filter(l ->
                        truncatedToDaysCreated.equals(l.getLocalDateTime())
                    ).findFirst()
                    .get()
                    .incCount();
            }
        );

        return result;
    }

    /**
     * Позволяет подготовить список с пустыми статистическими данными за даты,
     * находящиеся в указанном промежутке включительно
     *
     * @param from начало промежутка (начало дня)
     * @param to   конец промежутка (начало дня), может принимать null значение
     * @return List<LocalDateTimeAndCountStats>
     */
    private List<LocalDateTimeAndCountStats> prepareLocalDateTimeAndCountStatsList(
        LocalDateTime from, LocalDateTime to
    ) {

        if (from == null) {
            throw new RuntimeException("From must not be null");
        }

        if (to == null) {
            to = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        }

        List<LocalDateTimeAndCountStats> result = new ArrayList<>();

        int counter = 0;


        while (!from.plusDays(counter).isAfter(to)) {
            result.add(
                new LocalDateTimeAndCountStats(from.plusDays(counter))
            );

            ++counter;
        }


        return result;
    }

}
