package ru.cproject.karman.service.statistics;

import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.statistics.LocalDateTimeAndCountStats;

import java.util.List;

/**
 * Created by ioann on 11.07.16
 */
public interface CouponStatisticsService {


    /**
     * Позволяет получить статистику добавления акции в карман за последнюю неделю
     * @param coupon акция
     * @return List<LocalDateTimeAndCountStats>
     */
    List<LocalDateTimeAndCountStats> getFavoriteStatsByLastWeek(Coupon coupon);


    /**
     * Позволяет получить статистику просмотров акции за последнюю неделю
     * @param coupon акция
     * @return List<LocalDateTimeAndCountStats>
     */
    List<LocalDateTimeAndCountStats> getReviewStatsByLastWeek(Coupon coupon);

}
