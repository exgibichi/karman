package ru.cproject.karman.service.common.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cproject.karman.dao.jpa.repositories.common.ColorRepository;
import ru.cproject.karman.domain.common.Color;
import ru.cproject.karman.exception.FieldValidationException;
import ru.cproject.karman.model.common.ColorEntity;
import ru.cproject.karman.service.common.ColorService;
import ru.cproject.karman.validator.FieldValidator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ioann on 08.07.16
 */
@Service
public class ColorServiceImpl implements ColorService {

    private ColorRepository colorRepository;

    @Autowired
    public void setColorRepository(ColorRepository colorRepository) {
        this.colorRepository = colorRepository;
    }


    @Override
    public Color save(Color color) {
        validateSave(color);
        ColorEntity colorEntity = new ColorEntity(color);

        return colorRepository.save(colorEntity).toDomain();
    }


    @Override
    public void delete(Long id) {
        validateDelete(id);
        colorRepository.delete(id);
    }


    @Override
    public List<Color> getAll() {
        return colorRepository.findAll()
            .stream()
            .map(ColorEntity::toDomain)
            .collect(Collectors.toList());
    }


    private void validateSave(Color color) {

        if (!FieldValidator.validateStringNotEmpty(color.getColorHex())) {
            throw new FieldValidationException("Не указан цвет");
        }

        if (colorRepository.existsByColorHex(color.getColorHex())) {
            throw new FieldValidationException("Такой цвет уже существует");
        }

        FieldValidator.validateHexColor(color.getColorHex());

        if ("#FFFFFF".equals(color.getColorHex().toUpperCase())) {
            throw new FieldValidationException("Данный цвет фона запрещен для создания");
        }
    }


    private void validateDelete(Long id) {

        if (!colorRepository.exists(id)) {
            throw new RuntimeException("Указанного цвета не существует");
        }

    }

}
