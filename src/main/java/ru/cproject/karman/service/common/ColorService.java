package ru.cproject.karman.service.common;

import ru.cproject.karman.domain.common.Color;

import java.util.List;

/**
 * Created by ioann on 08.07.16
 */
public interface ColorService {

    Color save(Color color);

    void delete(Long id);

    List<Color> getAll();



}
