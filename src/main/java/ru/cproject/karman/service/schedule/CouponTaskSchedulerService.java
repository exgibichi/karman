package ru.cproject.karman.service.schedule;

/**
 * Created by ioann on 11.06.16
 */
public interface CouponTaskSchedulerService {

    /**
     * Позволяет деактивировать задачи, у которых истек срок действия
     */
    void deactivateOutDatedCoupons();

    /**
     * Позволяет открепить из топа закрепленные акции, срок закрепления которых истек
     */
    void resetOutDatedFixedCoupons();

}
