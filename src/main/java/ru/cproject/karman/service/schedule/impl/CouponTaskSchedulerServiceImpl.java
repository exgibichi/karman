package ru.cproject.karman.service.schedule.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.business.CouponRepository;
import ru.cproject.karman.service.schedule.CouponTaskSchedulerService;

import java.time.LocalDateTime;

/**
 * Created by ioann on 11.06.16
 */
@Service
public class CouponTaskSchedulerServiceImpl implements CouponTaskSchedulerService {

    private static final Logger LOGGER = Logger.getLogger(CouponTaskSchedulerServiceImpl.class.getName());

    private CouponRepository couponRepository;

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Override
    @Transactional
    @Scheduled(cron = "0 0 * * * ?")  //В начале каждого часа
    public void deactivateOutDatedCoupons() {
        LOGGER.info("Process of coupons deactivation started");

        try {
            couponRepository.deactivateOutDated();
        } catch (Exception e) {
            LOGGER.error("Deactivating scheduler error!", e);
        }
    }


    @Override
    @Transactional
    @Scheduled(cron = "0 0 * * * ?") //В начале каждого часа
    public void resetOutDatedFixedCoupons() {
        LOGGER.info("Process of coupons unfixing started");

        try {
            couponRepository.unfixOutDatedWhereFixingDateLessThan(
                LocalDateTime.now().minusDays(1)
            );
        } catch (Exception e) {
            LOGGER.error("Unfixing scheduler error!", e);
        }
    }

}
