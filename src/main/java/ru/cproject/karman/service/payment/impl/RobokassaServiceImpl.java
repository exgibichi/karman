package ru.cproject.karman.service.payment.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.business.IncomeRobokassaPayment;
import ru.cproject.karman.domain.business.PaymentHistory;
import ru.cproject.karman.service.payment.RobokassaService;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Map;

/**
 * Created by ioann on 19.06.16
 */
@Service
public class RobokassaServiceImpl implements RobokassaService {


    @Value("${robokassa.MerchantLogin}")
    private String merchantLogin;

    @Value("${robokassa.requestEncoding}")
    private String requestEncoding;

    @Value("${robokassa.hashAlgorithm}")
    private String hashAlgorithm;

    @Value("${robokassa.test.hashAlgorithm}")
    private String testHashAlgorithm;

    @Value("${robokassa.password1}")
    private String password1;

    @Value("${robokassa.password2}")
    private String password2;

    @Value("${robokassa.test.password1}")
    private String testPassword1;

    @Value("${robokassa.test.password2}")
    private String testPassword2;

    @Value("${robokassa.isTest}")
    private boolean isTest;


    @Override
    public boolean isIncomeRequestValid(Map<String, Object> params, boolean isResult) {

        String outSum = String.valueOf(params.get("OutSum"));
        String invId = String.valueOf(params.get("InvId"));
        String shpId = String.valueOf(params.get("Shp_id"));
        String sign = String.valueOf(params.get("SignatureValue"));

        boolean isTest = ("1".equals(String.valueOf(params.get("IsTest"))));

        String password;

        if (isResult) {
            password = isTest ? testPassword2 : password2;
        } else {
            password = isTest ? testPassword1 : password1;
        }

        String signBase = MessageFormat.format("{0}:{1}:{2}:{3}",
            outSum, invId, password, "Shp_id=" + shpId);


        return calcSign(signBase, isTest).toUpperCase()
            .equals(sign.toUpperCase());
    }


    @Override
    public IncomeRobokassaPayment buildPaymentFromParams(Map<String, Object> params) {
        IncomeRobokassaPayment payment = new IncomeRobokassaPayment();

        Long companyId = Long.valueOf(
            String.valueOf(params.get("Shp_id"))
        );

        Long invId = Long.valueOf(
            String.valueOf(params.get("InvId"))
        );

        boolean isTest = ("1".equals(String.valueOf(params.get("IsTest"))));

        payment.setAmountOfMoney(new BigDecimal((String)params.get("OutSum")).setScale(2));

        payment.setCompanyId(companyId);
        payment.setInvId(invId);
        payment.setTest(isTest);

        return payment;
    }

    @Override
    public String generatePaymentUrl(PaymentHistory paymentHistory) {
        Company company = paymentHistory.getCompany();

        StringBuilder sb = new StringBuilder();

        String outSum = paymentHistory.getAmountOfMoney().toString();
        String shpId = String.valueOf(company.getId());

        //Добавление параметров
        sb.append("https://auth.robokassa.ru/Merchant/Index.aspx");


        sb.append("?MerchantLogin=").append(merchantLogin);
        sb.append("&OutSum=").append(outSum);
        sb.append("&InvId=").append(paymentHistory.getId());

        StringBuilder descBuilder = new StringBuilder();
        descBuilder
            .append("Пополнение счета: ")
            .append(paymentHistory.getAmountOfMoney())
            .append(" руб");

        try {
            sb.append("&Description=")
                .append(
                    URLEncoder.encode(
                        descBuilder.toString(),
                        requestEncoding
                    )
                );
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }


        sb.append("&Culture=ru");
        sb.append("&Encoding=").append(requestEncoding);
        sb.append("&Shp_id=").append(shpId);

        if (isTest) {
            sb.append("&IsTest=1");
        }

        sb.append("&SignatureValue=").append(generateOutSign(outSum,
            String.valueOf(paymentHistory.getId()), shpId));


        return sb.toString();
    }


    private String generateOutSign(String outSum, String invId, String shpId) {

        String password = isTest ? testPassword1 : password1;

        String signBase = MessageFormat.format("{0}:{1}:{2}:{3}:{4}",
            merchantLogin, outSum, invId, password, "Shp_id=" + shpId);

        return calcSign(signBase, isTest);
    }


    private String calcSign(String signBase, boolean isTest) {
        String hashAlgorithm;

        if (isTest) {
            hashAlgorithm = this.testHashAlgorithm;
        } else {
            hashAlgorithm = this.hashAlgorithm;
        }

        if ("MD5".equalsIgnoreCase(hashAlgorithm)) {
            return DigestUtils.md5Hex(signBase);
        } else {
            throw new RuntimeException("Unsupported hash algorithm!" + hashAlgorithm);
        }

    }

}
