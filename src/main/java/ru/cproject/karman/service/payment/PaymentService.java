package ru.cproject.karman.service.payment;

import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.business.IncomeRobokassaPayment;
import ru.cproject.karman.domain.business.PaymentHistory;

/**
 * Created by ioann on 19.06.16
 */
public interface PaymentService {

    /**
     * Позволяет сохранить историю оплаты внутрисистемной валюты компанией через робокассу.
     * @param payment объект с данными облаты
     */
    PaymentHistory processCompanyRobokassaPayment(IncomeRobokassaPayment payment);

    /**
     * Позволяет инициировать новую платежную операцию для компании
     * @param company компания
     * @param count оплачиваемая сумма
     * @return PaymentHistory, на основании которого будет построен запрос к робокассе
     */
    PaymentHistory createNewPaymentHistoryForCompany(Company company, int count);


    /**
     * Позволяет обработать ситуацию,
     * когда пользователь отказался от оплаты на стороне агрегатора
     * @param id идентификатор платежа
     */
    void rejectPaymentHistoryById(Long id);


    PaymentHistory get(Long id);
}
