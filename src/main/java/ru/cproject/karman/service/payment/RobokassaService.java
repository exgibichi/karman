package ru.cproject.karman.service.payment;

import ru.cproject.karman.domain.business.IncomeRobokassaPayment;
import ru.cproject.karman.domain.business.PaymentHistory;

import java.util.Map;

/**
 * Created by ioann on 19.06.16
 */
public interface RobokassaService {

    /**
     * Позволяет проверить сигнатуру запроса от робокассы
     * @param params
     * @param isResult флаг url'a (result или success/fail)
     * @return
     */
    boolean isIncomeRequestValid(Map<String, Object> params, boolean isResult);

    IncomeRobokassaPayment buildPaymentFromParams(Map<String, Object> params);

    /**
     * Позволяет сгенерировать url для перехода к оплате
     * на стороне Робокассы, исходя из внутрисистемной платежной информации
     * @param paymentHistory платежная информация, на основании которой строится запрос к робокассе
     * @return String - url для редиректа
     */
    String generatePaymentUrl(PaymentHistory paymentHistory);
}
