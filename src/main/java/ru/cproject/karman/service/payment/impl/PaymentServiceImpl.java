package ru.cproject.karman.service.payment.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.account.CompanyRepository;
import ru.cproject.karman.dao.jpa.repositories.business.PaymentHistoryRepository;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.business.IncomeRobokassaPayment;
import ru.cproject.karman.domain.business.PaymentHistory;
import ru.cproject.karman.enums.PaymentStatus;
import ru.cproject.karman.model.business.PaymentHistoryEntity;
import ru.cproject.karman.service.payment.PaymentService;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by ioann on 19.06.16
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    private CompanyRepository companyRepository;
    private PaymentHistoryRepository paymentHistoryRepository;


    @Autowired
    public void setCompanyRepository(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Autowired
    public void setPaymentHistoryRepository(PaymentHistoryRepository paymentHistoryRepository) {
        this.paymentHistoryRepository = paymentHistoryRepository;
    }


    @Override
    @Transactional
    public PaymentHistory processCompanyRobokassaPayment(IncomeRobokassaPayment payment) {

        StringBuilder infoBuilder = new StringBuilder();

        if (!paymentHistoryRepository.exists(payment.getInvId())) {
            throw new RuntimeException("No such order with id: " + payment.getInvId());
        }

        PaymentHistoryEntity paymentHistoryEntity = paymentHistoryRepository.findOne(payment.getInvId());

        if (!PaymentStatus.NEW.equals(paymentHistoryEntity.getStatus())) {
            throw new RuntimeException("Payment with invId= "  + payment.getInvId()
                + " has already processed.");
        }

        if (!paymentHistoryEntity.getCompany().getId().equals(payment.getCompanyId())) {
            infoBuilder.append("Wrong companyId ")
                .append(paymentHistoryEntity.getCompany().getId())
                .append(" expected, but was ").append(payment.getCompanyId());
        }

        if (!paymentHistoryEntity.getAmountOfMoney().equals(payment.getAmountOfMoney())) {
            infoBuilder.append("Wrong amount of money ")
                .append(paymentHistoryEntity.getAmountOfMoney())
                .append(" expected, but was ").append(payment.getAmountOfMoney());
        }

        paymentHistoryEntity.setPaymentDate(LocalDateTime.now());

        if (infoBuilder.length() == 0) {

            if (payment.isTest()) {
                infoBuilder.append("Test payment completed");
                paymentHistoryEntity.setStatus(PaymentStatus.TEST);
                companyRepository.increaseCompanyBalance(paymentHistoryEntity.getCompany(),
                    paymentHistoryEntity.getAmountOfMoney().intValue());
            } else {
                infoBuilder.append("Payment completed");
                paymentHistoryEntity.setStatus(PaymentStatus.SUCCESS);
                companyRepository.increaseCompanyBalance(paymentHistoryEntity.getCompany(),
                    paymentHistoryEntity.getAmountOfMoney().intValue());
            }

        } else {
            paymentHistoryEntity.setStatus(PaymentStatus.FAILED);
        }

        paymentHistoryEntity.setInfo(infoBuilder.toString());

        return paymentHistoryRepository.save(paymentHistoryEntity).toDomain();
    }


    @Override
    @Transactional
    public PaymentHistory createNewPaymentHistoryForCompany(Company company, int count) {
        PaymentHistoryEntity paymentHistoryEntity = new PaymentHistoryEntity();

        paymentHistoryEntity.setStatus(PaymentStatus.NEW);
        paymentHistoryEntity.setCompany(companyRepository.findOne(company.getId()));
        paymentHistoryEntity.setAmountOfMoney(BigDecimal.valueOf(count).setScale(2));

        return paymentHistoryRepository.save(paymentHistoryEntity).toDomain();
    }

    @Override
    @Transactional
    public void rejectPaymentHistoryById(Long id) {
        PaymentHistoryEntity paymentHistoryEntity = paymentHistoryRepository.findOne(id);

        if (!PaymentStatus.NEW.equals(paymentHistoryEntity.getStatus())) {
            return;
        }

        paymentHistoryEntity.setStatus(PaymentStatus.FAILED);
        paymentHistoryEntity.setInfo("Has rejected by user");

        paymentHistoryRepository.save(paymentHistoryEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public PaymentHistory get(Long id) {
        if (!paymentHistoryRepository.exists(id)) {
            throw new RuntimeException("Платежа не существует");
        }

        return paymentHistoryRepository.findOne(id).toDomain();
    }
}
