package ru.cproject.karman.service;

import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;

import java.util.List;

/**
 * Created by ioann on 16.06.16
 */
public interface CustomerService {

    /**
     * Позволяет добавить акцию в карман пользователя (в избранные)
     * @param customer пользователь
     * @param coupon акция
     * @param forceAdding определяет принудительное добавление акции рекламодателем
     * @return true - акция успешно добавлена, false - акция добавлена ранее
     */
    boolean addCouponToFavorites(Customer customer, Coupon coupon, boolean forceAdding);

    /**
     * Позволяет удалить акцию из кармана пользователя (из избранных)
     * @param customer пользователь
     * @param coupon акция
     * @return true - акция успешно удалена false - такой акции нет в избранных
     */
    boolean removeCouponFromFavorites(Customer customer, Coupon coupon);

    boolean isWatched(Customer customer, Coupon coupon);

    /**
     * Позволяет пользователю подписаться на компанию
     * @param customer пользователь
     * @param company компания
     * @return true - произошла подписка, false - подписка была выполнена ранее
     */
    boolean subscribeToCompany(Customer customer, Company company);

    /**
     * Позволяет пользователю отписаться от компании
     * @param customer пользователь
     * @param company компания
     * @return true - отписка произошла успешно, false - пользователь не подписан на компанию
     */
    boolean unsubscribeFromCompany(Customer customer, Company company);

    /**
     * Позволяет лайкнуть акцию и получить актуальное число лайков для акции
     * @param customer пользователь
     * @param coupon акция
     * @return Long
     */
    Long addCouponToLiked(Customer customer, Coupon coupon);

    boolean addCouponToWatched(Customer customer, Coupon coupon);


    /**
     * Позволяет лайкнуть акцию и получить актуальное число лайков для акции
     * @param customer пользователь
     * @param coupon акция
     * @return Long
     */
    Long removeCouponFromLiked(Customer customer, Coupon coupon);


    /**
     * Позволяет узнать, был ли совершен лайк акции пользователем
     * @param customer пользователь
     * @param coupon акция
     * @return true - лайк был совершен, false - иначе
     */
    boolean isLiked(Customer customer, Coupon coupon);

    /**
     * Позволяет узнать, находится ли акция в избранных для указанного пользователя
     * @param customer пользователь
     * @param coupon акция
     * @return true - акция а избранных, false - иначе
     */
    boolean isFavorite(Customer customer, Coupon coupon);


    /**
     * Позволяет получить число акций в кармане (избранном) пользователя
     * @param customer пользователь
     * @return int - число акций в кармане
     */
    int getCountOfFavoriteByCustomer(Customer customer);


    /**
     * Позволяет получить число непоказанных акций в кармане пользователя,
     * которые были добавлены принудительно рекламодателем
     * @param customer пользователь
     * @return int - число новых акций в кармане
     */
    int getCountOfFavoriteByCustomerAndAreNotShowed(Customer customer);

    /**
     * Позволяет узнать, подписан ли пользователь на компанию
     * @param customer пользователь
     * @param company компания
     * @return true - пользователь подписан на компанию, false - иначе
     */
    boolean isCustomerSubscribedToCompany(Customer customer, Company company);
}
