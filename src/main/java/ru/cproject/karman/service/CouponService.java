package ru.cproject.karman.service;

import ru.cproject.karman.dao.jpa.query.business.CouponQuery;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.enums.CouponStatus;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ioann on 01.06.16
 */
public interface CouponService {

    /**
     * Позволяет создать новую акцию
     * @param coupon акция
     * @return Coupon
     */
    Coupon create(Coupon coupon);

    void delete(Coupon coupon);

    /**
     * Позволяет получить акцию по id
     * @param couponId id акции
     * @return Coupon
     */
    Coupon get(Long couponId);

    /**
     * Позволяет отредактировать акцию
     * @param coupon акция
     * @return фоновая версия акции, ожидающая модерации
     */
    Coupon edit(Coupon coupon);


    /**
     * Позволяет принять акцию
     * @param couponId идентификатор акции
     */
    void confirm(Long couponId);

    /**
     * Позволяет отклонить акцию
     * @param couponId идентификатор акции
     * @param rejectionReason причина отклонения
     */
    void reject(Long couponId, String rejectionReason);


    /**
     * Позволяет получить список всех акций с указанным статусом
     * @param status статус акции
     * @return List<Coupon>
     */
    List<Coupon> getAllByStatus(CouponStatus status);

    /**
     * Позволяет получить список всех акций указанной компании
     * @param company компания
     * @return List<Coupon>
     */
    List<Coupon> getAllParentByCompany(Company company);


    /**
     * Позволяет получить список всех акций, ожидающих модерацию
     * @return List<Coupon>
     */
    List<Coupon> getAllToReview();


    /**
     * Позволяет получить список акций, удовлетворяющих критерию запроса
     * @param query объект с критериями
     * @return List<Coupon>
     */
    List<Coupon> getAllByQuery(CouponQuery query);

    /**
     * Позволяет получить список идентификаторов акций,
     * добавленных в карман (избранное) указанным пользователем
     * @param customer пользователь
     * @return List<Long>
     */
    List<Long> getFavoriteIdsByCustomer(Customer customer);

    HashMap<Long,Long> getFIdsByCustomer(Customer customer);

    /**
     * Позволяет получить список идентификаторов акций,
     * которые лайкнул пользователь
     * @param customer пользователь
     * @return List<Long>
     */
    List<Long> getLikedIdsByCustomer(Customer customer);

    List<Long> getWatchedIdsByCustomer(Customer customer);


    /**
     * Позволяет поднять акция в топ ленты акций
     * @param company компания-создатель акции
     * @param coupon акция
     */
    void raise(Company company, Coupon coupon);

    /**
     * Позволяет закрепить акцию в топе.
     * @param company компания
     * @param coupon акция
     */
    void fix(Company company, Coupon coupon);

    void checkFavorite(Long couponId, Long customerId);

    /**
     * Позволяет увеличить счетчик поделившихся акцией
     * @param coupon акция
     */
    void incSharedCount(Coupon coupon);

    /**
     * Позволяет принудительно добавить акцию в карман (избранное)
     * указанному числу случайных пользователей
     * @param coupon акция
     * @param limit предельное число пользователей для добавления акции в карман
     */
    int forceAddCouponInFavoritesToRandomCustomers(Coupon coupon, int limit, Long region_id);

    /**
     * Позволяет продлить акцию до указанного момента
     * @param coupon акция
     * @param dateTo дата, до которой необходимо продлить акцию
     */
    void extendCoupon(Coupon coupon, LocalDateTime dateTo);

    /**
     * Позволяет получить число просмотров акции
     * @param coupon акция
     * @return long
     */
    long getCountOfReviews(Coupon coupon);
}
