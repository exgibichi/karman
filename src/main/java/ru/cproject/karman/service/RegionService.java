package ru.cproject.karman.service;

import ru.cproject.karman.dao.jpa.query.common.RegionQuery;
import ru.cproject.karman.domain.common.Region;

import java.util.List;

/**
 * Created by ioann on 02.06.16
 */
public interface RegionService {
    Region save(Region region);
    Region findOne(Long id);
    void delete(Long id);
    List<Region> getAll(RegionQuery query);
}
