package ru.cproject.karman.service.business;

import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponReview;

/**
 * Created by ioann on 10.07.16
 */
public interface CouponReviewService {

    CouponReview save(CouponReview couponReview);


    /**
     * Позволяет проверить, является ли просмотр акции
     * уникальным(первым с данным токеном) за текущий день
     * @param coupon акция
     * @param token токен просмотра
     * @return true - про
     */
    boolean isTheFirstReviewOfTheDayByToken(Coupon coupon, String token);
}
