package ru.cproject.karman.service.business;

import ru.cproject.karman.enums.PriceType;
import ru.cproject.karman.model.business.PriceEntity;

/**
 * Created by ioann on 25.07.16
 */
public interface PriceService {

    void update(PriceType type, Integer price);
}
