package ru.cproject.karman.service.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.cproject.karman.dao.jpa.repositories.business.PriceRepository;
import ru.cproject.karman.enums.PriceType;
import ru.cproject.karman.model.business.PriceEntity;
import ru.cproject.karman.service.business.PriceService;

/**
 * Created by ioann on 25.07.16
 */
@Service
public class PriceServiceImpl implements PriceService {

    private PriceRepository priceRepository;

    @Autowired
    public void setPriceRepository(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }


    @Override
    public void update(PriceType type, Integer price) {
        PriceEntity priceEntity = priceRepository.findOneByType(type);
        priceEntity.setPrice(price);
        priceRepository.save(priceEntity);
    }

}
