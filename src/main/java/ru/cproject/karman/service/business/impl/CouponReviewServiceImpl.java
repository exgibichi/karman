package ru.cproject.karman.service.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.business.CouponRepository;
import ru.cproject.karman.dao.jpa.repositories.business.CouponReviewRepository;
import ru.cproject.karman.dao.jpa.specifications.business.CouponReviewSpecifications;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponReview;
import ru.cproject.karman.model.business.CouponReviewEntity;
import ru.cproject.karman.service.business.CouponReviewService;

import java.time.LocalDateTime;

/**
 * Created by ioann on 10.07.16
 */
@Service
public class CouponReviewServiceImpl implements CouponReviewService {

    private CouponReviewRepository couponReviewRepository;
    private CouponRepository couponRepository;


    @Autowired
    public void setCouponReviewRepository(CouponReviewRepository couponReviewRepository) {
        this.couponReviewRepository = couponReviewRepository;
    }

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }


    @Override
    @Transactional
    public CouponReview save(CouponReview couponReview) {
        CouponReviewEntity couponReviewEntity = new CouponReviewEntity();

        Coupon coupon = couponReview.getCoupon();

        couponReviewEntity.setCoupon(couponRepository.findOne(coupon.getId()));
        couponReviewEntity.setToken(couponReview.getToken());
        couponReviewEntity.setDate(LocalDateTime.now());

        return couponReviewRepository.save(couponReviewEntity).toShortDomain();
    }



    @Override
    @Transactional(readOnly = true)
    public boolean isTheFirstReviewOfTheDayByToken(Coupon coupon, String token) {

        long countOfReviews = couponReviewRepository.count(
            CouponReviewSpecifications
                .generateSpecsForSearchByCouponAndTokenAtCurrentDay(
                    coupon.getId(),
                    token
                )
        );

        return countOfReviews == 0;
    }


}
