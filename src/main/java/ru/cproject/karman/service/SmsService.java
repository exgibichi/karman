package ru.cproject.karman.service;

import java.io.IOException;

/**
 * Created by ioann on 25.05.16
 */
public interface SmsService {

    /**
     * Позволяет отправить sms на указанный номер телефона
     * @param message текст сообщения
     * @param phoneNumber номер телефона
     */
    void sendMessage(String message, String phoneNumber) throws IOException;

}
