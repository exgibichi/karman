package ru.cproject.karman.service;

import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponEntity;

import java.io.IOException;

/**
 * Created by ioann on 25.05.16
 */
public interface PushService {

    void push(String token, String message, Long couponId);

}
