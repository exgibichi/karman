package ru.cproject.karman.service;

/**
 * Created by ioann on 27.05.16
 */
public interface PasswordRecoveryService {

    /**
     * Позволяет восстановить пароль пользователя
     * @param phoneNumber номер телефона пользователя
     */
    void recoverPassword(String phoneNumber);

}
