package ru.cproject.karman.service;

import ru.cproject.karman.dao.jpa.query.business.comment.CouponCommentQuery;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponComment;

import java.util.List;

/**
 * Created by ioann on 05.07.16
 */
public interface CouponCommentService {

    boolean isCouponCommentedByCustomer(Coupon coupon, Customer customer);

    /**
     * Позволяет пользователю прокомментировать акцию
     * @param couponComment комментарий
     * @return CouponComment
     */
    CouponComment addCommentToCoupon(CouponComment couponComment);


    /**
     * Позволяет получить страницу комментариев в зависимости от указанных криетериев
     * @param query объект с критериями
     * @return List<CouponComment>
     */
    List<CouponComment> getPageByQuery(CouponCommentQuery query);
}
