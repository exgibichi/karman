package ru.cproject.karman.service;

import ru.cproject.karman.domain.account.User;

/**
 * Created by ioann on 01.06.16
 */
public interface UserService {

    User getByPhoneNumber(String phoneNumber);

    User edit(User user);

}
