package ru.cproject.karman.dao.jpa.repositories.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;

import java.util.List;

/**
 * Created by ioann on 25.05.16.
 */
@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long>,
    JpaSpecificationExecutor<CustomerEntity> {




    @Query("SELECT count(c) > 0 from CustomerEntity c " +
        " WHERE c = :customer AND :coupon MEMBER OF c.likedCoupons")
    boolean couponExistInLiked(
        @Param(value = "customer") CustomerEntity customer,
        @Param(value = "coupon") CouponEntity coupon
    );

    @Query("SELECT count(c) > 0 from CustomerEntity c " +
        " WHERE c = :customer AND :company MEMBER OF c.subscriptions")
    boolean isCustomerSubscribedToCompany(
        @Param(value = "customer") CustomerEntity customer,
        @Param(value = "company") CompanyEntity company
    );

    @Query("SELECT count(c) > 0 from CustomerEntity c " +
        " WHERE c = :customer AND :coupon MEMBER OF c.watchedCoupons")
    boolean couponExistInWatched(
        @Param(value = "customer") CustomerEntity customer,
        @Param(value = "coupon") CouponEntity coupon
    );

    /* SELECT * FROM customers c WHERE c.id
    NOT IN (SELECT f.customer_id FROM  favorite_coupons f WHERE f.coupon_id = ?) */
    @Query("SELECT c FROM CustomerEntity c " +
        " WHERE c NOT IN (SELECT f.customer FROM FavoriteCouponEntity f " +
        " WHERE f.coupon = :coupon)")
    List<CustomerEntity> findAllNotFavored(
        @Param(value = "coupon") CouponEntity coupon
    );

    /* SELECT * FROM customers c WHERE c.id
    NOT IN (SELECT f.customer_id FROM  favorite_coupons f WHERE f.coupon_id = ?) */
    @Query("SELECT c FROM CustomerEntity c " +
        " WHERE c.user.region_id = :region_id AND " +
        " c NOT IN (SELECT f.customer FROM FavoriteCouponEntity f " +
        " WHERE f.coupon = :coupon)")
    List<CustomerEntity> findAllNotFavoredInRegion(
        @Param(value = "coupon") CouponEntity coupon,
        @Param(value = "region_id") Long region_id
    );

    /* SELECT * FROM customers c WHERE c.id
    NOT IN (SELECT f.customer_id FROM  favorite_coupons f WHERE f.coupon_id = ?) */
    @Query("SELECT c FROM CustomerEntity c " +
        " WHERE c.user.region_id IN (:region_ids) AND " +
        " c NOT IN (SELECT f.customer FROM FavoriteCouponEntity f " +
        " WHERE f.coupon = :coupon)")
    List<CustomerEntity> findAllNotFavoredInRegions(
        @Param(value = "coupon") CouponEntity coupon,
        @Param(value = "region_ids") List<Long> region_ids
    );

}
