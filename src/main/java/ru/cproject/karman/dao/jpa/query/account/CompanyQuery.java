package ru.cproject.karman.dao.jpa.query.account;

import org.springframework.data.domain.Pageable;
import ru.cproject.karman.dao.jpa.CprojectPageRequest;

/**
 * Created by ioann on 06.07.16
 */
public class CompanyQuery {

    private String search;
    private boolean isActive = true;

    private Pageable pageRequest;


    public CompanyQuery(String search, int skip, int limit) {
        this.search = search;

        this.pageRequest = new CprojectPageRequest(
            skip, limit);
    }


    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Pageable getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(Pageable pageRequest) {
        this.pageRequest = pageRequest;
    }
}
