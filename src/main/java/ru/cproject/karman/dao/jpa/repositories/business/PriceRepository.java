package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.enums.PriceType;
import ru.cproject.karman.model.business.PriceEntity;

/**
 * Created by ioann on 25.07.16
 */
@Repository
public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

    PriceEntity findOneByType(PriceType type);
}
