package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.cproject.karman.model.business.PaymentHistoryEntity;

/**
 * Created by ioann on 19.06.16
 */
public interface PaymentHistoryRepository extends JpaRepository<PaymentHistoryEntity, Long> {
}
