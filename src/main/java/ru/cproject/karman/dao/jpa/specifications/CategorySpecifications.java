package ru.cproject.karman.dao.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.dao.jpa.query.common.CategoryQuery;
import ru.cproject.karman.model.common.CategoryEntity;
import ru.cproject.karman.model.common.CategoryEntity_;

/**
 * Created by ioann on 15.06.16
 */
public class CategorySpecifications {

    private CategorySpecifications() {
        throw new AssertionError();
    }

    public static Specification<CategoryEntity> titleStartsLike(String title) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (title == null || title.trim().isEmpty()) {
                return criteriaBuilder.conjunction();
            }

            String titlePatter = title.trim().toLowerCase() + "%";


            return criteriaBuilder.like(
                criteriaBuilder.lower(root.get(CategoryEntity_.title)),
                titlePatter
            );
        };
    }


    public static Specifications<CategoryEntity> generateSearchSpecifications(CategoryQuery query) {
        return Specifications.where(titleStartsLike(query.getTitle()));
    }

}
