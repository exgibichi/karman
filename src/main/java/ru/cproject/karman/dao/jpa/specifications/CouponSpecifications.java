package ru.cproject.karman.dao.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.dao.jpa.query.business.CouponQuery;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.model.account.CompanyEntity_;
import ru.cproject.karman.model.account.UserEntity_;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.CouponEntity_;
import ru.cproject.karman.model.common.CategoryEntity_;
import ru.cproject.karman.model.common.RegionEntity_;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ioann on 11.06.16
 */
public class CouponSpecifications {

    private CouponSpecifications() {
        throw new AssertionError();
    }

    /**
     * Истекшие акции
     *
     * @return
     */
    public static Specification<CouponEntity> outDated() {
        return (root, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.lessThan(root.get(CouponEntity_.dateTo), LocalDateTime.now());
    }


    /**
     * Акции, не имеющие указанный статус
     *
     * @param status
     * @return
     */
    public static Specification<CouponEntity> statusNot(CouponStatus status) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (status == null) {
                return criteriaBuilder.conjunction();
            }

            return criteriaBuilder.notEqual(root.get(CouponEntity_.status), status);
        };
    }

    /**
     * С указанным статусом
     *
     * @param status
     * @return
     */
    public static Specification<CouponEntity> statusEq(CouponStatus status) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (status == null) {
                return criteriaBuilder.conjunction();
            }

            return criteriaBuilder.equal(root.get(CouponEntity_.status), status);
        };
    }

    public static Specification<CouponEntity> companyIdEq(Long companyId) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (companyId == null) {
                return criteriaBuilder.conjunction();
            }

            return criteriaBuilder.equal(
                root.get(CouponEntity_.company)
                    .get(CompanyEntity_.id),
                companyId);
        };
    }

    public static Specification<CouponEntity> categoryIdEq(Long categoryId) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (categoryId == null) {
                return criteriaBuilder.conjunction();
            }

            return criteriaBuilder.equal(
                root.get(CouponEntity_.category)
                    .get(CategoryEntity_.id),
                categoryId);
        };
    }

    public static Specification<CouponEntity> regionIdEq(Long regionId, Long regionParentId) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (regionId == null) {
                return criteriaBuilder.conjunction();
            }

            Predicate childPredicate = criteriaBuilder.equal(
                root.get(CouponEntity_.region)
                    .get(RegionEntity_.id),
                regionId);

            Predicate parentPredicate = criteriaBuilder.equal(
                root.get(CouponEntity_.region)
                    .get(RegionEntity_.parent)
                    .get(RegionEntity_.id),
                regionId
            );

            if(regionParentId == null) {
                return criteriaBuilder.or(parentPredicate, childPredicate);
            }

            Predicate nparentPredicate = criteriaBuilder.or(
                    criteriaBuilder.equal(
                root.get(CouponEntity_.region)
                    .get(RegionEntity_.parent)
                    .get(RegionEntity_.id),
                regionId
            ),criteriaBuilder.equal(
                root.get(CouponEntity_.region)
                    .get(RegionEntity_.id),
                          regionParentId
            ));

            return criteriaBuilder.or(nparentPredicate, childPredicate);
        };
    }


    public static Specification<CouponEntity> textFieldsLike(String search) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (search == null || search.trim().isEmpty()) {
                return criteriaBuilder.conjunction();
            }

            String searchPattern = "%" + search.trim().toLowerCase() + "%";

            Predicate titlePredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(CouponEntity_.title)),
                searchPattern);

            Predicate descriptionPredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(CouponEntity_.description)),
                searchPattern);

            Predicate giftDescriptionPredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(CouponEntity_.giftDescription)),
                searchPattern);

            Predicate companyNamePredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(CouponEntity_.company)
                    .get(CompanyEntity_.user).get(UserEntity_.name)),
                searchPattern);

            return criteriaBuilder.or(titlePredicate,
                descriptionPredicate,
                giftDescriptionPredicate,
                companyNamePredicate);
        };
    }


    public static Specification<CouponEntity> inFavoritesForCustomer(List<Long> favoriteIds) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (favoriteIds == null) {
                return criteriaBuilder.conjunction();
            } else if (favoriteIds.isEmpty()) {
                return criteriaBuilder.disjunction();
            }

            return criteriaBuilder.isTrue(root.get(CouponEntity_.id).in(favoriteIds));
        };
    }

    public static Specification<CouponEntity> isPublisherCompanyIdIn(List<Long> publisherCompanyIds) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (publisherCompanyIds == null) {
                return criteriaBuilder.conjunction();
            } else if (publisherCompanyIds.isEmpty()) {
                return criteriaBuilder.disjunction();
            }

            return criteriaBuilder.isTrue(
                root.get(CouponEntity_.company).get(CompanyEntity_.id)
                    .in(publisherCompanyIds)
            );
        };
    }


    public static Specifications<CouponEntity> generateSearchSpecifications(CouponQuery query) {
        return Specifications.where(statusEq(query.getStatus()))
            .and(companyIdEq(query.getCompanyId()))
            .and(categoryIdEq(query.getCategoryId()))
            .and(regionIdEq(query.getRegionId(), query.getRegionParentId()))
            .and(textFieldsLike(query.getSearch()))
            .and(inFavoritesForCustomer(query.getFavoriteIds()))
            .and(isPublisherCompanyIdIn(query.getSubscriptionIds()));
    }

}
