package ru.cproject.karman.dao.jpa.repositories.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.common.ColorEntity;

/**
 * Created by ioann on 08.07.16
 */
@Repository
public interface ColorRepository extends JpaRepository<ColorEntity, Long> {


    @Query("SELECT COUNT(c) > 0 FROM ColorEntity c " +
        " WHERE c.colorHex = :colorHex")
    boolean existsByColorHex(
        @Param(value = "colorHex") String colorHex
    );

}
