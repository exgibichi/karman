package ru.cproject.karman.dao.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.dao.jpa.query.account.CompanyQuery;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CompanyEntity_;
import ru.cproject.karman.model.account.UserEntity_;
import ru.cproject.karman.model.common.CategoryEntity_;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

/**
 * Created by ioann on 06.07.16
 */
public class CompanySpecifications {

    private CompanySpecifications() {
        throw new AssertionError();
    }

    public static Specification<CompanyEntity> namePrefixLike(String search) {
        return (root, query, cb) -> {

            if (search == null || search.isEmpty()) {
                return cb.conjunction();
            }

            String searchPattern = search.trim().toLowerCase() + "%";

            return cb.like(
                    cb.lower(root.get(CompanyEntity_.user)
                            .get(UserEntity_.name)),
                    searchPattern);
        };
    }

    public static Specification<CompanyEntity> nameLike(String search) {
        return (root, query, cb) -> {

            if (search == null || search.isEmpty()) {
                return cb.conjunction();
            }

            String searchPattern = "%" + search.trim().toLowerCase() + "%";

            return cb.like(
                cb.lower(root.get(CompanyEntity_.user)
                    .get(UserEntity_.name)),
                searchPattern);
        };
    }


    public static Specification<CompanyEntity> categoryTitleLike(String search) {
        return (root, query, cb) -> {

            if (search == null || search.isEmpty()) {
                return cb.conjunction();
            }

            root.join(CompanyEntity_.category, JoinType.LEFT);

            String searchPattern = "%" + search + "%";

            return cb.like(
                root.get(CompanyEntity_.category)
                    .get(CategoryEntity_.title),
                searchPattern);
        };
    }


    public static Specification<CompanyEntity> isActive(boolean isActive) {
        return (root, query, cb) -> cb.equal(root.get(CompanyEntity_.isActive), isActive);
    }

    public static Specifications<CompanyEntity> generateSearchSpecifications(CompanyQuery query) {
        return Specifications
            .where(
                isActive(query.isActive())
            )
            .and(Specifications
                .where(
                    namePrefixLike(query.getSearch())
                )
            );
    }

}
