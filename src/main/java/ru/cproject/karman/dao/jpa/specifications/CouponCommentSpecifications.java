package ru.cproject.karman.dao.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.dao.jpa.query.business.comment.CouponCommentQuery;
import ru.cproject.karman.model.business.CouponCommentEntity;
import ru.cproject.karman.model.business.CouponCommentEntity_;
import ru.cproject.karman.model.business.CouponEntity_;

/**
 * Created by ioann on 05.07.16
 */
public class CouponCommentSpecifications {

    private CouponCommentSpecifications() {
        throw new AssertionError();
    }


    public static Specification<CouponCommentEntity> couponIdEq(Long id) {
        return (root, query, cb) -> {

            if (id == null) {
                return cb.conjunction();
            }

            return cb.equal(
                root.get(CouponCommentEntity_.coupon)
                    .get(CouponEntity_.id),
                id);
        };
    }


    public static Specifications<CouponCommentEntity> generateSpecificationsFromQuery(
        CouponCommentQuery query
    ) {
        return Specifications.where(
            couponIdEq(query.getCouponId())
        );
    }

}
