package ru.cproject.karman.dao.jpa.query.business;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.cproject.karman.dao.jpa.CprojectPageRequest;
import ru.cproject.karman.enums.CouponStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 15.06.16
 */
public class CouponQuery {

    private static final Sort.Direction DEFAULT_SORT_DIRECTION = Sort.Direction.DESC;
    private static final String DEFAULT_SORT_FIELD = "created";

    private CouponStatus status = CouponStatus.ACTIVE;
    private Long companyId;
    private Long regionId;
    private Long regionParentId;
    private Long categoryId;
    private String search;
    private List<Long> favoriteIds; //идентификаторы акций в кармане (избранных) потребителя
    private List<Long> subscriptionIds; //идентификаторы компаний, на которые подписан потребитель
    private Pageable pageRequest;


    /**
     * Конструктор
     *
     * @param companyId
     * @param regionId
     * @param categoryId
     * @param skip
     * @param limit
     * @param sortParams параметры сортировки, где четные индексы - направление, а нечетные - поле сортировки.
     *                   Если число этих параметров не кратно двум, то используется сортировка по умолчанию.
     */
    public CouponQuery(Long companyId, Long regionId, Long regionParentId, Long categoryId,
                       int skip, int limit, String... sortParams) {
        this.companyId = companyId;
        this.regionId = regionId;
        this.regionParentId = regionParentId;
        this.categoryId = categoryId;

        Sort sort = prepareSort(sortParams);

        this.pageRequest = new CprojectPageRequest(
            skip,
            limit,
            sort
        );

    }


    /**
     * Конструктор
     *
     * @param search
     * @param regionId
     * @param skip
     * @param limit
     * @param favoriteIds
     * @param subscriptionIds
     * @param sortParams параметры сортировки, где четные индексы - направление, а нечетные - поле сортировки.
     *                   Если число этих параметров не кратно двум, то используется сортировка по умолчанию.
     */
    public CouponQuery(String search, Long regionId, int skip, int limit,
                       List<Long> favoriteIds, List<Long> subscriptionIds,
                       String... sortParams) {
        this.search = search;
        this.regionId = regionId;
        this.favoriteIds = favoriteIds;
        this.subscriptionIds = subscriptionIds;

        Sort sort = prepareSort(sortParams);

        this.pageRequest = new CprojectPageRequest(
            skip,
            limit,
            sort
        );
    }

    public CouponStatus getStatus() {
        return status;
    }

    public void setStatus(CouponStatus status) {
        this.status = status;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getRegionParentId() {
        return regionParentId;
    }

    public void setRegionParentId(Long regionParentId) {
        this.regionParentId = regionParentId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public List<Long> getFavoriteIds() {
        return favoriteIds;
    }

    public void setFavoriteIds(List<Long> favoriteIds) {
        this.favoriteIds = favoriteIds;
    }

    public List<Long> getSubscriptionIds() {
        return subscriptionIds;
    }

    public void setSubscriptionIds(List<Long> subscriptionIds) {
        this.subscriptionIds = subscriptionIds;
    }

    public Pageable getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(Sort sort) {
        this.pageRequest = pageRequest;
    }


    /**
     * Позволяет подготовить условия сортировки из массива строк
     * @param sortParams параметры сортировки
     * @return
     */
    private Sort prepareSort(String... sortParams) {
        Sort sort;
        if (sortParams != null && sortParams.length != 0 && sortParams.length % 2 == 0) {

            List<Sort.Order> orderList = new ArrayList<>();

            for (int i = 0; i < sortParams.length; i += 2) {
                Sort.Order order = new Sort.Order(
                    Sort.Direction.fromString(sortParams[i]),
                    sortParams[i + 1]
                );

                orderList.add(order);
            }

            sort = new Sort(orderList);
        } else {
            sort = new Sort(
                DEFAULT_SORT_DIRECTION,
                DEFAULT_SORT_FIELD
            );
        }

        return sort;
    }

}
