package ru.cproject.karman.dao.jpa.repositories.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.account.CompanyEntity;

import java.util.List;

/**
 * Created by ioann on 25.05.16.
 */
@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long>,
    JpaSpecificationExecutor<CompanyEntity> {

    @Modifying
    @Query("UPDATE CompanyEntity c SET c.balance = c.balance + :addingBalance " +
        " WHERE c = :company")
    void increaseCompanyBalance(
        @Param(value = "company") CompanyEntity company,
        @Param(value = "addingBalance") int addingBalance);

    @Modifying
    @Query("UPDATE CompanyEntity c SET c.balance = c.balance - :deductingBalance " +
        " WHERE c = :company")
    void decreaseCompanyBalance(
        @Param(value = "company") CompanyEntity company,
        @Param(value = "deductingBalance") int deductingBalance);


    @Query("SELECT c FROM CompanyEntity c " +
        "WHERE (c.isActive = FALSE AND c.moderatorsComment IS NULL) " +
        "OR c.newName IS NOT NULL " +
        "OR c.newEmail IS NOT NULL " +
        "OR c.newFullName IS NOT NULL")
    List<CompanyEntity> findAllToReview();
}
