package ru.cproject.karman.dao.jpa.query.common;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.cproject.karman.dao.jpa.CprojectPageRequest;

/**
 * Created by ioann on 15.06.16
 */
public class CategoryQuery {

    private String title;
    private Pageable pageRequest;

    public CategoryQuery(String title, int offset, int limit) {
        this.title = title;
        this.pageRequest = new CprojectPageRequest(offset, limit,
            new Sort(Sort.Direction.ASC, "title"));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Pageable getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(Pageable pageRequest) {
        this.pageRequest = pageRequest;
    }
}
