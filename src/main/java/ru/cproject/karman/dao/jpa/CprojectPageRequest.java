package ru.cproject.karman.dao.jpa;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by ioann on 15.06.16
 */
public class CprojectPageRequest implements Pageable {

    private int offset;
    private int limit;
    private Sort sort;


    /**
     * Constructor
     * @param offset
     * @param limit
     */
    public CprojectPageRequest(int offset, int limit) {
        this(offset, limit, null);
    }

    /**
     * Constructor
     * @param offset
     * @param limit
     * @param sort
     */
    public CprojectPageRequest(int offset, int limit, Sort sort) {
        if (offset < 0) {
            offset = 0;
        }

        if (limit < 0) {
            limit = 0;
        }

        this.offset = offset;
        this.limit = limit;

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "id");
        }

        this.sort = sort;
    }

    @Override
    public int getPageNumber() {
        return 0;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return this;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

}


