package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponCommentEntity;
import ru.cproject.karman.model.business.CouponEntity;

/**
 * Created by ioann on 04.07.16
 */
@Repository
public interface CouponCommentRepository extends JpaRepository<CouponCommentEntity, Long>,
    JpaSpecificationExecutor<CouponCommentEntity> {

    @Query("SELECT COUNT(c) > 0 FROM CouponCommentEntity c " +
           " WHERE c.coupon = :coupon AND c.customer = :customer")
    boolean isCouponCommentedByCustomer(@Param(value = "coupon") CouponEntity coupon,
                                        @Param(value = "customer") CustomerEntity customer);


    @Modifying
    @Query("DELETE FROM CouponCommentEntity c WHERE c.coupon.id = :couponId")
    void deleteAllByCouponId(
            @Param(value = "couponId") Long couponId
    );
}
