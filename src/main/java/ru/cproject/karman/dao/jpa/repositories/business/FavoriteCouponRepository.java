package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ioann on 09.07.16
 */
@Repository
public interface FavoriteCouponRepository extends JpaRepository<FavoriteCouponEntity, Long> {

    @Query("SELECT count(f) > 0 from FavoriteCouponEntity f " +
        " WHERE f.customer = :customer AND f.coupon = :coupon")
    boolean couponExistsInFavorites(
        @Param(value = "customer") CustomerEntity customer,
        @Param(value = "coupon") CouponEntity coupon
    );


    FavoriteCouponEntity findByCouponAndCustomer(CouponEntity coupon, CustomerEntity customer);


    List<FavoriteCouponEntity> findAllByCouponAndCreatedGreaterThanEqualOrderByCreatedAsc(
        CouponEntity couponEntity, LocalDateTime dateTime
    );


    @Query("SELECT count(f) from FavoriteCouponEntity f " +
        " WHERE f.customer = :customer AND f.coupon.status = ru.cproject.karman.enums.CouponStatus.ACTIVE")
    int countOfFavoriteByCustomer(
        @Param(value = "customer") CustomerEntity customer
    );


    @Query("SELECT count(f) from FavoriteCouponEntity f " +
        " WHERE f.customer = :customer AND f.coupon.status = ru.cproject.karman.enums.CouponStatus.ACTIVE" +
        " AND f.isShowed = FALSE")
    int countOfFavoriteByCustomerAndAreNotShowed(
        @Param(value = "customer") CustomerEntity customer
    );

    @Modifying
    @Query("UPDATE FavoriteCouponEntity f SET f.isShowed = TRUE " +
        " WHERE f.customer = :customer AND f.coupon = :coupon AND f.isShowed = FALSE")
    int updateFavoritesByUserSetShowedWhereIsNotShowed(
            @Param(value = "customer") CustomerEntity customer,
            @Param(value = "coupon") CouponEntity coupon
    );


    @Modifying
    @Query("DELETE FROM FavoriteCouponEntity c WHERE c.coupon.id = :couponId")
    void deleteAllByCouponId(
            @Param(value = "couponId") Long couponId
    );
}
