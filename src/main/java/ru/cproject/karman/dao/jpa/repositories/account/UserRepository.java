package ru.cproject.karman.dao.jpa.repositories.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.account.UserEntity;

/**
 * Created by ioann on 25.05.16.
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findOneByPhoneNumber(String phoneNumber);
}
