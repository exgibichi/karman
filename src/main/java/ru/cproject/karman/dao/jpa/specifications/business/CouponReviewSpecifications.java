package ru.cproject.karman.dao.jpa.specifications.business;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.model.business.CouponEntity_;
import ru.cproject.karman.model.business.CouponReviewEntity;
import ru.cproject.karman.model.business.CouponReviewEntity_;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by ioann on 10.07.16
 */
public class CouponReviewSpecifications {

    private CouponReviewSpecifications() {
        throw new AssertionError();
    }


    public static Specification<CouponReviewEntity> couponIdEq(Long couponId) {
        return (root, query, cb) -> {

            if (couponId == null) {
                return cb.conjunction();
            }

            return cb.equal(
                root.get(CouponReviewEntity_.coupon)
                .get(CouponEntity_.id),
                couponId
            );

        };
    }


    public static Specification<CouponReviewEntity> tokenEq(String token) {
        return (root, query, cb) -> {

            if (token == null || token.trim().isEmpty()) {
                return cb.conjunction();
            }


            return cb.equal(
                root.get(CouponReviewEntity_.token),
                token
            );
        };
    }


    public static Specification<CouponReviewEntity> dateGe(LocalDateTime date) {
        return (root, query, cb) -> {

            if (date == null) {
                return cb.conjunction();
            }

            return cb.greaterThanOrEqualTo(
                root.get(CouponReviewEntity_.date),
                date
            );

        };
    }


    /**
     * Генерирует спецификации для поиска просмотра акции под указанным токеном
     * в тот день, когда вызывается метод
     * @param couponId id акции
     * @param token токен просмотра
     * @return Specifications<CouponReviewEntity>
     */
    public static Specifications<CouponReviewEntity> generateSpecsForSearchByCouponAndTokenAtCurrentDay(
        Long couponId,
        String token
    ) {
        LocalDateTime startOfCurrentDay = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);

        return Specifications.where(
            couponIdEq(couponId)
        ).and(
            tokenEq(token)
        ).and(
            dateGe(startOfCurrentDay)
        );
    }

}
