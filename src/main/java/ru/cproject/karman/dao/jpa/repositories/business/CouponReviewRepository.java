package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.CouponReviewEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ioann on 10.07.16
 */
@Repository
public interface CouponReviewRepository extends JpaRepository<CouponReviewEntity, Long>,
    JpaSpecificationExecutor<CouponReviewEntity> {

    List<CouponReviewEntity> findAllByCouponAndDateGreaterThanEqualOrderByDateAsc(
        CouponEntity couponEntity, LocalDateTime dateTime
    );


    @Query("SELECT COUNT(c.id) FROM CouponReviewEntity c WHERE c.coupon = :coupon")
    long countByCoupon(@Param(value = "coupon") CouponEntity coupon);


    @Modifying
    @Query("DELETE FROM CouponReviewEntity c WHERE c.coupon.id = :couponId")
    void deleteAllByCouponId(
            @Param(value = "couponId") Long couponId
    );
}
