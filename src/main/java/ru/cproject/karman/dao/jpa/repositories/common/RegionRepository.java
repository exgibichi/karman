package ru.cproject.karman.dao.jpa.repositories.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.common.RegionEntity;

/**
 * Created by ioann on 31.05.16
 */
@Repository
public interface RegionRepository extends JpaRepository<RegionEntity, Long>,
    JpaSpecificationExecutor<RegionEntity> {

    RegionEntity findOneByTitleAndParent(String title, RegionEntity parent);
    RegionEntity findOne(Long id);
}
