package ru.cproject.karman.dao.jpa.repositories.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.common.CategoryEntity;

/**
 * Created by ioann on 30.05.16
 */
@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long>,
    JpaSpecificationExecutor<CategoryEntity> {

    CategoryEntity findOneByTitle(String title);
    CategoryEntity findOneById(Long id);

}
