package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.business.CouponEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ioann on 02.06.16
 */
@Repository
public interface CouponRepository extends JpaRepository<CouponEntity, Long>,
    JpaSpecificationExecutor {


    List<CouponEntity> findAllByStatus(CouponStatus status);

    @Query("SELECT c FROM CouponEntity c " +
        "WHERE c.status = :status AND c.company.isActive = TRUE")
    List<CouponEntity> findAllByStatusAndActiveCompany(
        @Param(value = "status") CouponStatus status
    );

    List<CouponEntity> findAllByCompanyAndParentIsNullOrderByCreatedDesc(CompanyEntity company);


    @Modifying
    @Query("UPDATE CouponEntity c SET c.status = ru.cproject.karman.enums.CouponStatus.FINISHED," +
        " c.isBackgroundColorHexPayed = false " +
        " WHERE c.dateTo < CURRENT_TIMESTAMP " +
        " AND (c.status = ru.cproject.karman.enums.CouponStatus.ACTIVE " +
        " OR c.status = ru.cproject.karman.enums.CouponStatus.PAUSED) " +
        " AND c.parent IS NULL")
    void deactivateOutDated();


    @Modifying
    @Query("UPDATE CouponEntity c SET c.fixingDate = NULL " +
        " WHERE c.fixingDate < :date")
    void unfixOutDatedWhereFixingDateLessThan(
        @Param(value = "date") LocalDateTime date
    );

    @Modifying
    @Query("UPDATE CouponEntity c SET c.likesCount = c.likesCount + 1 " +
        "WHERE c = :coupon")
    void incLike(@Param(value = "coupon") CouponEntity coupon);


    @Modifying
    @Query("UPDATE CouponEntity c SET c.likesCount = c.likesCount - 1 " +
        "WHERE c = :coupon AND c.likesCount > 0")
    void decLike(@Param(value = "coupon") CouponEntity coupon);

    @Modifying
    @Query("UPDATE CouponEntity c SET c.commentsCount = (" +
        " SELECT (COUNT(cm.id) + 1) FROM CouponCommentEntity cm " +
        " WHERE cm.coupon = :coupon " +
        " ) " +
        " WHERE c = :coupon")
    void incCommentsCount(@Param(value = "coupon") CouponEntity coupon);

    @Modifying
    @Query("UPDATE CouponEntity c SET c.sharedCount = c.sharedCount + 1 " +
        "WHERE c = :coupon")
    void incSharedCount(@Param(value = "coupon") CouponEntity coupon);


    @Override
    @Modifying
    @Query("DELETE FROM CouponEntity c WHERE c.id = :couponId")
    void delete(
            @Param(value = "couponId") Long couponId
    );


    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM coupon_images WHERE coupon_id = :couponId")
    void deleteImages(
            @Param(value = "couponId") Long couponId);
}
