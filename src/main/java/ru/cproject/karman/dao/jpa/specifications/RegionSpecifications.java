package ru.cproject.karman.dao.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.cproject.karman.dao.jpa.query.common.RegionQuery;
import ru.cproject.karman.model.common.RegionEntity;
import ru.cproject.karman.model.common.RegionEntity_;

/**
 * Created by ioann on 16.06.16
 */
public class RegionSpecifications {

    private RegionSpecifications() {
        throw new AssertionError();
    }


    public static Specification<RegionEntity> titleStartsLike(String title) {
        return (root, criteriaQuery, criteriaBuilder) -> {

            if (title == null || title.trim().isEmpty()) {
                return criteriaBuilder.conjunction();
            }

            String titlePattern = title.trim().toLowerCase() + "%";


            return criteriaBuilder.like(
                criteriaBuilder.lower(root.get(RegionEntity_.title)),
                titlePattern
            );
        };
    }

    public static Specification<RegionEntity> categoryIsParent(Boolean isParent) {
        return (root, query, cb) -> {

            if (isParent == null || !isParent) {
                return cb.conjunction();
            }

            return cb.isNull(root.get(RegionEntity_.parent));
        };
    }


    public static Specifications<RegionEntity> generateSearchSpecifications(RegionQuery query) {
        return Specifications
            .where(titleStartsLike(query.getTitle()))
            .and(categoryIsParent(query.isParent()));
    }

}
