package ru.cproject.karman.dao.jpa.query.common;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.cproject.karman.dao.jpa.CprojectPageRequest;

/**
 * Created by ioann on 16.06.16
 */
public class RegionQuery {

    private String title;
    private Pageable pageRequest;
    private Boolean isParent;

    public RegionQuery(String title, Boolean isParent, int offset, int limit) {
        this.title = title;
        this.isParent = isParent;
        this.pageRequest = new CprojectPageRequest(offset, limit,
            new Sort(Sort.Direction.ASC, "title"));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isParent() {
        return isParent;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

    public Pageable getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(Pageable pageRequest) {
        this.pageRequest = pageRequest;
    }

}
