package ru.cproject.karman.dao.jpa.repositories.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cproject.karman.model.business.CouponContactEntity;

/**
 * Created by ioann on 07.07.16
 */
@Repository
public interface CouponContactRepository extends JpaRepository<CouponContactEntity, Long> {

    @Modifying
    @Query("DELETE FROM CouponContactEntity c WHERE c.coupon.id = :couponId")
    void deleteAllByCouponId(
        @Param(value = "couponId") Long couponId
    );
}
