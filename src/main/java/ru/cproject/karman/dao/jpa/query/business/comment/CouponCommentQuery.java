package ru.cproject.karman.dao.jpa.query.business.comment;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.cproject.karman.dao.jpa.CprojectPageRequest;

/**
 * Created by ioann on 05.07.16
 */
public class CouponCommentQuery {

    private static final String DEFAULT_SORT_FIELD = "created";

    private Long couponId;
    private Pageable pageRequest;

    public CouponCommentQuery(Long couponId, int skip, int limit) {
        this.couponId = couponId;

        this.pageRequest = new CprojectPageRequest(
            skip,
            limit,
            new Sort(
                Sort.Direction.ASC,
                DEFAULT_SORT_FIELD
            )
        );
    }


    public Long getCouponId() {
        return couponId;
    }

    public Pageable getPageRequest() {
        return pageRequest;
    }
}
