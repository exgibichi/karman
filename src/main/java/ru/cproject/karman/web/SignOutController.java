package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.SecurityConstants;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ioann on 26.05.16
 */
@RestController
public class SignOutController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(SignOutController.class.getName());

    @RequestMapping(value = "/sign.out", method = RequestMethod.POST)
    public CustomDto signOut(HttpServletResponse response) {
        Cookie cookie = new Cookie(SecurityConstants.X_AUTH_TOKEN, null);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);

        return StatusDtoFactory.buildSuccess();
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
