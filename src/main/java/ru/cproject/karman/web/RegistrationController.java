package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.converter.PhoneNumberConverter;
import ru.cproject.karman.dto.account.RegistrationDto;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.exception.SmsException;
import ru.cproject.karman.service.RegistrationService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 26.05.16
 */
@RestController
public class RegistrationController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(RegistrationController.class.getName());

    private RegistrationService registrationService;

    @Autowired
    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(value = "/sign.up", method = RequestMethod.POST)
    public CustomDto signUp(@RequestBody RegistrationDto dto) {
        registrationService.register(
            PhoneNumberConverter.convert(dto.phoneNumber),
            dto.name,
            dto.role,
            dto.email,
            dto.fullName,
            dto.region_id
        );
        return StatusDtoFactory.buildSuccess();
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }

    @ExceptionHandler(SmsException.class)
    public CustomDto handleSmsError(SmsException e) {
        LOGGER.error(e.getMessage(), e);
        return StatusDtoFactory.buildError("Операция не может быть выполнена");
    }

}
