package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.CompanyService;
import ru.cproject.karman.service.CustomerService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 27.06.16
 */
@RestController
@RequestMapping("/subscriptions")
public class SubsciptionController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(SubsciptionController.class.getName());


    private CustomerService customerService;
    private CompanyService companyService;
    private CustomSecurityContext customSecurityContext;


    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }


    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/subscribe", method = RequestMethod.POST)
    public CustomDto subscribe(@RequestBody IdDto dto) {
        Company company = companyService.get(dto.id);
        Customer customer = customSecurityContext.getCurrentPrincipal().getCustomer();

        return StatusDtoFactory.buildBoolean(
            customerService.subscribeToCompany(customer, company)
        );
    }


    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/unsubscribe", method = RequestMethod.POST)
    public CustomDto unsubscribe(@RequestBody IdDto dto) {
        Company company = companyService.get(dto.id);
        Customer customer = customSecurityContext.getCurrentPrincipal().getCustomer();

        return StatusDtoFactory.buildBoolean(
            customerService.unsubscribeFromCompany(customer, company)
        );
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
