package ru.cproject.karman.web.statistics;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.statistics.ChartDto;
import ru.cproject.karman.service.CouponService;
import ru.cproject.karman.service.statistics.CouponStatisticsService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.time.format.DateTimeFormatter;

/**
 * Created by ioann on 11.07.16
 */
@RestController
@RequestMapping("/coupons/stats")
public class CouponStatisticsController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(CouponStatisticsController.class.getName());
    private static final DateTimeFormatter D_MM_FORMATTER = DateTimeFormatter.ofPattern("d.MM");

    private CouponService couponService;
    private CouponStatisticsService couponStatisticsService;
    private CustomSecurityContext customSecurityContext;


    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setCouponStatisticsService(CouponStatisticsService couponStatisticsService) {
        this.couponStatisticsService = couponStatisticsService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }



    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/favorite/lastWeek/{id}", method = RequestMethod.GET)
    public CustomDto getFavoriteStatsByLastWeek(@PathVariable(value = "id") Long couponId) {
        ChartDto result = new ChartDto();

        User user = customSecurityContext.getCurrentPrincipal();

        Coupon coupon = couponService.get(couponId);

        if (!user.getCompany().getId().equals(coupon.getCompany().getId())) {
            throw new RuntimeException("Попытка просмотреть статистику чужой акции");
        }

        if (coupon.getParent() != null) {
            throw new RuntimeException("Указанной акции не существует");
        }

        couponStatisticsService.getFavoriteStatsByLastWeek(coupon)
            .forEach(
                s -> {
                    result.yCoords.add(s.getCount());
                    result.xLabels.add(D_MM_FORMATTER.format(s.getLocalDateTime()));
                }
            );

        return result;
    }



    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/review/lastWeek/{id}", method = RequestMethod.GET)
    public CustomDto getReviewStatsByLastWeek(@PathVariable(value = "id") Long couponId) {
        ChartDto result = new ChartDto();

        User user = customSecurityContext.getCurrentPrincipal();

        Coupon coupon = couponService.get(couponId);

        if (!user.getCompany().getId().equals(coupon.getCompany().getId())) {
            throw new RuntimeException("Попытка просмотреть статистику чужой акции");
        }

        if (coupon.getParent() != null) {
            throw new RuntimeException("Указанной акции не существует");
        }

        couponStatisticsService.getReviewStatsByLastWeek(coupon)
            .forEach(
                s -> {
                    result.yCoords.add(s.getCount());
                    result.xLabels.add(D_MM_FORMATTER.format(s.getLocalDateTime()));
                }
            );

        return result;
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
