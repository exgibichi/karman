package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.dao.jpa.query.common.RegionQuery;
import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.dto.common.RegionDto;
import ru.cproject.karman.service.RegionService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 02.06.16
 */
@RestController
@RequestMapping("/regions")
public class RegionController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(RegionController.class.getName());

    private RegionService regionService;


    @Autowired
    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<RegionDto> all(
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "isParent", required = false) Boolean isParent,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit
    ) {
        return regionService.getAll(new RegionQuery(title, isParent, skip, limit))
            .stream()
            .map(Region::toDto)
            .collect(Collectors.toList());
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
