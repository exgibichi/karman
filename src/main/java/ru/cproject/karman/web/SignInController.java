package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.conf.security.AuthTokenService;
import ru.cproject.karman.conf.security.SecurityConstants;
import ru.cproject.karman.conf.security.RoleEnum;
import ru.cproject.karman.conf.security.SignInService;
import ru.cproject.karman.converter.PhoneNumberConverter;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.dto.security.SignInCredentialsDto;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.security.SuccessSignInDto;
import ru.cproject.karman.service.UserService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ioann on 26.05.16
 */
@RestController
public class SignInController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(SignInController.class.getName());

    private SignInService signInService;
    private AuthTokenService authTokenService;
    private UserService userService;

    @Autowired
    public void setSignInService(SignInService signInService) {
        this.signInService = signInService;
    }

    @Autowired
    public void setAuthTokenService(AuthTokenService authTokenService) {
        this.authTokenService = authTokenService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/sign.in", method = RequestMethod.POST)
    public CustomDto signIn(@RequestBody SignInCredentialsDto dto,
                            HttpServletResponse response) {

        Authentication auth = signInService.signIn(
            PhoneNumberConverter.convert(dto.phoneNumber),
            dto.password
        );

        if (auth == null || !auth.isAuthenticated()) {
            throw new RuntimeException("Неверный номер или пароль");
        }

        SuccessSignInDto resultDto = new SuccessSignInDto();

        UserDetails userDetails = (UserDetails) auth.getDetails();
        resultDto.authToken = authTokenService.createTokenForUser(userDetails);

        User user = userService.getByPhoneNumber(userDetails.getUsername());
        resultDto.phoneNumber = user.getPhoneNumber();
        resultDto.name = user.getName();

        if (user.getCustomer() != null) {
            resultDto.role = RoleEnum.CUSTOMER;
        } else if (user.getCompany() != null) {
            resultDto.role = RoleEnum.COMPANY;
        }

        Cookie cookie = new Cookie(SecurityConstants.X_AUTH_TOKEN, resultDto.authToken);
        cookie.setPath("/");
        cookie.setMaxAge(365 * 24 * 60 * 60);
        response.addCookie(cookie);

        return resultDto;
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }

}
