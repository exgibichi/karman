package ru.cproject.karman.web.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.domain.common.Color;
import ru.cproject.karman.dto.common.color.ColorDto;
import ru.cproject.karman.service.common.ColorService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 08.07.16
 */
@RestController
@RequestMapping("/colors")
public class ColorController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(ColorController.class.getName());


    private ColorService colorService;

    @Autowired
    public void setColorService(ColorService colorService) {
        this.colorService = colorService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ColorDto> getAll() {
        return colorService.getAll()
            .stream()
            .map(Color::toDto)
            .collect(Collectors.toList());
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
