package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.dao.jpa.query.common.CategoryQuery;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.CategoryService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 30.05.16
 */
@RestController
@RequestMapping("/categories")
public class CategoryController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(CategoryController.class.getName());


    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<FullCategoryDto> getAll(
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit) {

        return categoryService.getAll(new CategoryQuery(title, skip, limit))
            .stream()
            .map(Category::toDto)
            .collect(Collectors.toList());

    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
