package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.converter.PhoneNumberConverter;
import ru.cproject.karman.dto.account.PasswordRecoveryDto;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.exception.SmsException;
import ru.cproject.karman.service.PasswordRecoveryService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 27.05.16
 */
@RestController
public class PasswordRecoveryController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(PasswordRecoveryController.class.getName());

    private PasswordRecoveryService passwordRecoveryService;

    @Autowired
    public void setPasswordRecoveryService(PasswordRecoveryService passwordRecoveryService) {
        this.passwordRecoveryService = passwordRecoveryService;
    }

    @RequestMapping(value = "/password.recovery", method = RequestMethod.POST)
    public CustomDto recoverPassword(@RequestBody PasswordRecoveryDto dto) {
        passwordRecoveryService.recoverPassword(
            PhoneNumberConverter.convert(dto.phoneNumber)
        );
        return StatusDtoFactory.buildSuccess();
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }

    @ExceptionHandler(SmsException.class)
    public CustomDto handleSmsError(SmsException e) {
        logger().error(e.getMessage(), e);
        return StatusDtoFactory.buildError("Операция не может быть выполнена");
    }

}
