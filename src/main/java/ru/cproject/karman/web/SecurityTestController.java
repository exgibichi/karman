package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 26.05.16
 */
@RestController
public class SecurityTestController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(SecurityTestController.class.getName());


    public class SecurityTestDto implements CustomDto {
        public String message;
    }

    @RequestMapping("/openApi")
    public CustomDto openApi() {
        SecurityTestDto dto = new SecurityTestDto();
        dto.message = "Это открытый метод";
        return dto;
    }

    @Secured(Roles.IS_AUTHENTICATED_FULLY)
    @RequestMapping("/authApi")
    public CustomDto authApi() {
        SecurityTestDto dto = new SecurityTestDto();
        dto.message = "Вы прошли аутентификацию";
        return dto;
    }

    @Secured(Roles.CUSTOMER)
    @RequestMapping("/customerApi")
    public CustomDto customerApi() {
        SecurityTestDto dto = new SecurityTestDto();
        dto.message = "Вы пользователь";
        return dto;
    }

    @Secured(Roles.COMPANY)
    @RequestMapping("/companyApi")
    public CustomDto companyApi() {
        SecurityTestDto dto = new SecurityTestDto();
        dto.message = "Вы представитель компании";
        return dto;
    }

    @Secured(Roles.ADMIN)
    @RequestMapping("/admin/api")
    public CustomDto adminApi() {
        SecurityTestDto dto = new SecurityTestDto();
        dto.message = "Вы админ";
        return dto;
    }

    @ExceptionHandler(Exception.class)
    public CustomDto handleError(Exception e) {
        return StatusDtoFactory.buildError(e.getMessage());
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
