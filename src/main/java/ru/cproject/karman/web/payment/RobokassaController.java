package ru.cproject.karman.web.payment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.constants.CookieConstants;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.business.IncomeRobokassaPayment;
import ru.cproject.karman.domain.business.PaymentHistory;
import ru.cproject.karman.enums.PaymentStatus;
import ru.cproject.karman.service.payment.PaymentService;
import ru.cproject.karman.service.payment.RobokassaService;
import ru.cproject.karman.util.HttpUtils;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by ioann on 18.06.16
 */
@RestController
@RequestMapping("/robokassa")
public class RobokassaController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(RobokassaController.class.getName());


    private RobokassaService robokassaService;
    private PaymentService paymentService;
    private CustomSecurityContext customSecurityContext;
    private HttpServletRequest request;


    @Autowired
    public void setRobokassaService(RobokassaService robokassaService) {
        this.robokassaService = robokassaService;
    }

    @Autowired
    public void setPaymentService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }


    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public void result(HttpServletResponse response) {

        Map<String, Object> params = HttpUtils.getParamsMapFromRequest(request);

        //Проверка sign
        if (!robokassaService.isIncomeRequestValid(params, true)) {
            HttpUtils.writeTextToResponse("bad sign", response);
            return;
        }

        //Обновление платежной информации
        IncomeRobokassaPayment payment = robokassaService.buildPaymentFromParams(params);
        PaymentHistory paymentHistory = paymentService.processCompanyRobokassaPayment(payment);

        //Ответ робокассе
        if (PaymentStatus.FAILED.equals(paymentHistory.getStatus())) {
            HttpUtils.writeTextToResponse(paymentHistory.getId() + " failed: "
                + paymentHistory.getInfo() , response);
        } else {
            HttpUtils.writeTextToResponse("OK" + params.get("InvId"), response);
        }
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public void toPayment(@RequestParam(value = "count") int count,
                          HttpServletResponse response)
        throws IOException {

        Company company = customSecurityContext.getCurrentPrincipal().getCompany();
        PaymentHistory paymentHistory = paymentService.createNewPaymentHistoryForCompany(company, count);
        String url = robokassaService.generatePaymentUrl(paymentHistory);

        response.sendRedirect(url);
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public void success(HttpServletResponse response) throws IOException, ServletException {
        Map<String, Object> params = HttpUtils.getParamsMapFromRequest(request);

        if (!robokassaService.isIncomeRequestValid(params, false)) {
            HttpUtils.writeTextToResponse("bad sign", response);
            return;
        }

        String cookieName = CookieConstants.SUCCESS_ROBOKASSA_PAYMENT;
        String cookieValue = String.valueOf(params.get("InvId"));
        Cookie successPaymentCookie = new Cookie(cookieName, cookieValue);
        successPaymentCookie.setPath("/");
        successPaymentCookie.setMaxAge(100);

        response.addCookie(successPaymentCookie);

        response.sendRedirect("/#/purse");
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/fail", method = RequestMethod.GET)
    public void fail(HttpServletResponse response) throws IOException, ServletException {
        Map<String, Object> params = HttpUtils.getParamsMapFromRequest(request);

        String invIdStr = String.valueOf(params.get("InvId"));

        paymentService.rejectPaymentHistoryById(Long.valueOf(invIdStr));

        String cookieName = CookieConstants.FAIL_ROBOKASSA_PAYMENT;
        String cookieValue = String.valueOf(invIdStr);
        Cookie failPaymentCookie = new Cookie(cookieName, cookieValue);
        failPaymentCookie.setPath("/");
        failPaymentCookie.setMaxAge(100);

        response.addCookie(failPaymentCookie);

        response.sendRedirect("/#/purse");
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }

}
