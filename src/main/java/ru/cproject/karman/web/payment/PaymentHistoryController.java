package ru.cproject.karman.web.payment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.domain.business.PaymentHistory;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.CountDto;
import ru.cproject.karman.service.payment.PaymentService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 20.06.16
 */
@RestController
@RequestMapping("/paymentHistory")
public class PaymentHistoryController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(PaymentHistoryController.class.getName());

    private PaymentService paymentService;


    @Autowired
    public void setPaymentService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CustomDto getOne(@PathVariable(value = "id") Long id) {
        PaymentHistory paymentHistory = paymentService.get(id);

        return new CountDto(
            paymentHistory.getAmountOfMoney().intValue()
        );
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
