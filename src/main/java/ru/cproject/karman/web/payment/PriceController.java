package ru.cproject.karman.web.payment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.component.PriceCalculator;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.business.coupon.ExtendCouponDto;
import ru.cproject.karman.dto.common.LocalDateTimeDto;
import ru.cproject.karman.dto.payment.BackgroundColorPriceDto;
import ru.cproject.karman.dto.payment.PricesDto;
import ru.cproject.karman.service.CouponService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


/**
 * Created by ioann on 23.07.16
 */
@RestController
@RequestMapping("/prices")
public class PriceController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(PriceController.class.getName());

    private CouponService couponService;
    private PriceCalculator priceCalculator;


    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setPriceCalculator(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public CustomDto getAll() {
        PricesDto dto = new PricesDto();

        dto.backgroundColorPerDayPrice = priceCalculator.getBackgroundColorPerDayPrice();
        dto.raisingPrice = priceCalculator.getRaisingPrice();
        dto.fixingPrice = priceCalculator.getFixingPrice();
        dto.forceAddingToFavoritesPerCustomerPrice = priceCalculator.getForceAddingToFavoritesPerCustomerPrice();

        return dto;
    }


    @RequestMapping(value = "/backgroundColor", method = RequestMethod.POST)
    public CustomDto calcPriceForBackgroundColor(@RequestBody  LocalDateTimeDto dateTimeDto) {
        BackgroundColorPriceDto dto = new BackgroundColorPriceDto();

        dto.price = priceCalculator.calcPriceForBackgroundColor(dateTimeDto.date);

        LocalDateTime tempDateTime = LocalDateTime.from(LocalDateTime.now());

        //Целое число дней до конца акции
        long days = tempDateTime.until(dateTimeDto.date, ChronoUnit.DAYS);
        tempDateTime.plusDays(days);

        long minutesOfLastDay = tempDateTime.until(dateTimeDto.date, ChronoUnit.MINUTES);

        if (minutesOfLastDay > 0) {
            ++days;
        }

        dto.days = Long.valueOf(days).intValue();

        return dto;
    }


    @RequestMapping(value = "/extending", method = RequestMethod.POST)
    public CustomDto calcExtendingOfCoupon(@RequestBody ExtendCouponDto extendCouponDto) {
        Coupon coupon = couponService.get(extendCouponDto.id);

        if ("#FFFFFF".equals(coupon.getBackgroundColorHex())) {
            return null;
        }

        BackgroundColorPriceDto dto = new BackgroundColorPriceDto();

        dto.price = priceCalculator.calcPriceForBackgroundColor(extendCouponDto.dateTo);
        dto.days = dto.price / priceCalculator.getBackgroundColorPerDayPrice();

        return dto;
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
