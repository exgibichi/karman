package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.resources.ImageDto;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by ioann on 30.05.16
 */
@RestController
public class ImageUploadController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(ImageUploadController.class.getName());

    @Value("${saleband.imagepath}")
    private String imagePath;

    @Value("${saleband.maxImageSizeMb}")
    private long maxImageSizeMb;

    @Secured(Roles.IS_AUTHENTICATED_FULLY)
    @RequestMapping(value = "/image.upload", method = RequestMethod.POST)
    public CustomDto uploadFile(@RequestParam("image") MultipartFile image) throws Exception {
        validate(image);

        String realImagePath;

        File f = new File(imagePath);
        if (!f.exists() || !f.isDirectory()) {
            f.mkdirs();
        }

        if (imagePath.startsWith("file:")) {
            realImagePath = imagePath.substring("file:".length());
        } else {
            realImagePath = imagePath;
        }

        //Генерируем уникальное имя для нового файла
        File file = File.createTempFile("img_", ".jpg", new File(realImagePath));

        //Сохраняем изображение
        BufferedOutputStream stream = null;

        try {
            stream = new BufferedOutputStream(
                new FileOutputStream(file)
            );

            FileCopyUtils.copy(image.getInputStream(), stream);
        } finally {
            if (stream != null) {
                stream.flush();
                stream.close();
            }
        }

        ImageDto imageDto = new ImageDto();
        imageDto.fileName = file.getName();

        return imageDto;
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }


    private void validate(MultipartFile image) {

        if (image.isEmpty()) {
            throw new RuntimeException("Не задано изображение");
        }

        if (!image.getContentType().equals("image/jpeg") &&
            !image.getContentType().equals("image/png")) {
            throw new RuntimeException("Допустима загрузка только изображений форматов jpg, png");
        }

        if (image.getSize() > 1024 * 1024 * maxImageSizeMb) {
            throw new RuntimeException("Максимальный допустимый размер изображения - " + maxImageSizeMb + "МБ");
        }

    }

}
