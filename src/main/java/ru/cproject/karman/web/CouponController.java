package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.dao.jpa.query.business.CouponQuery;
import ru.cproject.karman.dao.jpa.query.business.comment.CouponCommentQuery;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.domain.business.CouponComment;
import ru.cproject.karman.domain.business.CouponReview;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.business.coupon.*;
import ru.cproject.karman.dto.business.coupon.comment.CouponCommentDto;
import ru.cproject.karman.dto.business.coupon.comment.CouponCommentPageDto;
import ru.cproject.karman.dto.business.coupon.comment.IncomeCouponCommentDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.statistics.ChartDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.service.*;
import ru.cproject.karman.service.business.CouponReviewService;
import ru.cproject.karman.service.statistics.CouponStatisticsService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;


import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 02.06.16
 */
@RestController
@RequestMapping("/coupons")
public class CouponController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(CouponController.class.getName());
    private static final DateTimeFormatter D_MM_FORMATTER = DateTimeFormatter.ofPattern("d.MM");

    private CouponService couponService;
    private CustomerService customerService;
    private CompanyService companyService;
    private CouponCommentService couponCommentService;
    private CouponReviewService couponReviewService;
    private CouponStatisticsService couponStatisticsService;
    private CustomSecurityContext customSecurityContext;
    private RegionService regionService;

    @Autowired
    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }

    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Autowired
    public void setCouponCommentService(CouponCommentService couponCommentService) {
        this.couponCommentService = couponCommentService;
    }

    @Autowired
    public void setCouponReviewService(CouponReviewService couponReviewService) {
        this.couponReviewService = couponReviewService;
    }


    @Autowired
    public void setCouponStatisticsService(CouponStatisticsService couponStatisticsService) {
        this.couponStatisticsService = couponStatisticsService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CustomDto createCoupon(@RequestBody IncomeCouponDto dto) {
        couponService.create(new Coupon(dto));
        return StatusDtoFactory.buildSuccess();
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public List<OutcomeCouponFullDto> getHistory() {
        User user = customSecurityContext.getCurrentPrincipal();

        return couponService.getAllParentByCompany(user.getCompany())
            .stream()
            .map(c -> {
                OutcomeCouponFullDto couponFullDto = c.toFullDto();
                couponFullDto.reviewsCount = couponService.getCountOfReviews(c);

                ChartDto favoriteDto = new ChartDto();

                couponStatisticsService.getFavoriteStatsByLastWeek(c)
                    .forEach(
                        s -> {
                            favoriteDto.yCoords.add(s.getCount());
                            favoriteDto.xLabels.add(D_MM_FORMATTER.format(s.getLocalDateTime()));
                        }
                    );

                ChartDto reviewDto = new ChartDto();

                couponStatisticsService.getReviewStatsByLastWeek(c)
                    .forEach(
                        s -> {
                            reviewDto.yCoords.add(s.getCount());
                            reviewDto.xLabels.add(D_MM_FORMATTER.format(s.getLocalDateTime()));
                        }
                    );

                couponFullDto.chartsData.add(favoriteDto);
                couponFullDto.chartsData.add(reviewDto);

                return couponFullDto;
            })
            .collect(Collectors.toList());
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/my/{id}", method = RequestMethod.GET)
    public CustomDto getCouponForEditing(@PathVariable(value = "id") Long id) {
        User user = customSecurityContext.getCurrentPrincipal();
        Coupon coupon = couponService.get(id);

        if (!user.getCompany().getId().equals(coupon.getCompany().getId())) {
            throw new RuntimeException("Попытка отредактировать чужую акцию");
        }

        if (coupon.getParent() != null) {
            throw new RuntimeException("Указанной акции не существует");
        }

        if (coupon.getChild() != null
            && CouponStatus.REVIEW.equals(coupon.getChild().getStatus())) {
            throw new RuntimeException("Данная акция уже находится в модерации");
        }

        return coupon.toFullDto();
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public CustomDto edit(@RequestBody IncomeCouponDto dto) {
        User user = customSecurityContext.getCurrentPrincipal();
        Coupon coupon = couponService.get(dto.id);

        if (!user.getCompany().getId().equals(coupon.getCompany().getId())) {
            throw new RuntimeException("Попытка отредактировать чужую акцию");
        }

        couponService.edit(new Coupon(dto));

        return StatusDtoFactory.buildSuccess();
    }


    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<ClientCouponFullDto> getAllByQuery(
        @RequestParam(name = "companyId", required = false) Long companyId,
        @RequestParam(name = "regionId", required = false) Long regionId,
        @RequestParam(name = "categoryId", required = false) Long categoryId,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit
    ) {

        User user = customSecurityContext.getCurrentPrincipal();

        Long regionParentId = null;
        if(regionId != null) {
            if (regionService.findOne(regionId) != null) {
                if (regionService.findOne(regionId).getParent() != null) {
                    regionParentId = regionService.findOne(regionId).getParent().getId();
                }
            }
        }
        List<ClientCouponFullDto> result = couponService.getAllByQuery(
            new CouponQuery(companyId, regionId, regionParentId, categoryId, skip, limit,
                Sort.Direction.ASC.toString(), "fixingDate",
                Sort.Direction.DESC.toString(), "raisingDate")
        )
            .stream()
            .map(Coupon::toClientFullDto)
            .collect(Collectors.toList());

        fillPageByIndividualInfo(result, user, false);

        return result;
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<ClientCouponFullDto> search(
        @RequestParam(name = "search", required = false) String search,
        @RequestParam(name = "regionId", required = false) Long regionId,
        @RequestParam(name = "sort", required = false, defaultValue = "all") String sort,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit
    ) {

        User user = customSecurityContext.getCurrentPrincipal();
        //Идетификаторы акций в кармане (избранном) пользователей
        List<Long> favoriteIdsToSearch = null;
        //Идентификаторы компаний, на которые подписан пользователь
        List<Long> subscriptionIdsToSearch = null;
        String[] sortParams = {
            Sort.Direction.ASC.toString(), "fixingDate",
            Sort.Direction.DESC.toString(), "raisingDate"
        };

        if (sort != null) {

            if ("my".equals(sort)) {

                if (user != null && user.getCustomer() != null) {
                    Customer customer = user.getCustomer();
                    favoriteIdsToSearch = couponService.getFavoriteIdsByCustomer(customer);
                    sortParams = null;
                }
            } else if ("subscriptions".equals(sort)) {
                if (user != null && user.getCustomer() != null) {
                    Customer customer = user.getCustomer();
                    subscriptionIdsToSearch = companyService.getSubscriptionIdsByCustomer(customer);
                    sortParams = null;
                }
            }
        }


        List<ClientCouponFullDto> result = couponService.getAllByQuery(
            new CouponQuery(search, regionId, skip, limit,
                favoriteIdsToSearch, subscriptionIdsToSearch, sortParams)
        )
            .stream()
            .map(Coupon::toClientFullDto)
            .collect(Collectors.toList());

        fillPageByIndividualInfo(result, user,false);

        return result;
    }


    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<ClientCouponFullDto> favorite(
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit
    ) {
        User user = customSecurityContext.getCurrentPrincipal();
        Customer customer = user.getCustomer();
        List<Long> favoriteIds = couponService.getFavoriteIdsByCustomer(customer);

        List<ClientCouponFullDto> result = couponService.getAllByQuery(
            new CouponQuery(null, null, skip, limit, favoriteIds, null, "desc", "created")
        )
            .stream()
            .map(Coupon::toClientFullDto)
            .collect(Collectors.toList());

        fillPageByIndividualInfo(result, user, true);

        return result;
    }


    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public CustomDto getOne(
        @PathVariable(value = "id") Long id,
        @RequestParam(name = "token", required = false) String token
    ) {
        Coupon coupon = couponService.get(id);
        ClientCouponFullDto result = coupon.toClientFullDto();

        User user = customSecurityContext.getCurrentPrincipal();

        if (user != null && user.getCustomer() != null) {
            Customer customer = user.getCustomer();
            result.canLike = !customerService.isLiked(customer, coupon);
            result.isFavorite = customerService.isFavorite(customer, coupon);
            result.canSubscribe = !customerService.isCustomerSubscribedToCompany(customer,
                companyService.get(result.companyId));
            result.canComment = !couponCommentService
                .isCouponCommentedByCustomer(coupon, customer);
        }
        if (user != null && user.getCustomer() != null) {
            customerService.addCouponToWatched(user.getCustomer(), coupon);
            couponService.checkFavorite(coupon.getId(), user.getCustomer().getId());
        }
        if (token != null && !token.isEmpty()) {
            //Просмотр засчитывается только раз за день
            if (couponReviewService.isTheFirstReviewOfTheDayByToken(coupon, token)) {
                couponReviewService.save(new CouponReview(coupon, token));
            }
        }

        return result;
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/raise", method = RequestMethod.POST)
    public CustomDto raiseCoupon(@RequestBody IdDto dto) {

        Coupon coupon = couponService.get(dto.id);
        Company company = customSecurityContext.getCurrentPrincipal().getCompany();

        couponService.raise(company, coupon);

        return StatusDtoFactory.buildSuccess();
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/fix", method = RequestMethod.POST)
    public CustomDto fixCoupon(@RequestBody IdDto dto) {

        Coupon coupon = couponService.get(dto.id);
        Company company = customSecurityContext.getCurrentPrincipal().getCompany();

        couponService.fix(company, coupon);

        return StatusDtoFactory.buildSuccess();
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/forceAddToFavorites", method = RequestMethod.POST)
    public CustomDto forceAddCouponToFavorites(@RequestBody ForceAddingDto dto) {

        Coupon coupon = couponService.get(dto.id);
        Company company = customSecurityContext.getCurrentPrincipal().getCompany();

        if (!coupon.getCompany().getId().equals(company.getId())) {
            throw new RuntimeException("Попытка выполнить операцию с чужой акцией");
        }

        int countOffAddings = couponService.forceAddCouponInFavoritesToRandomCustomers(coupon, dto.count, dto.region_id);

        return new CustomDto() {
            public int count = countOffAddings;
        };
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/extend", method = RequestMethod.POST)
    public CustomDto extend(@RequestBody ExtendCouponDto dto) {
        Coupon coupon = couponService.get(dto.id);
        couponService.extendCoupon(coupon, dto.dateTo);

        return StatusDtoFactory.buildSuccess();
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public CustomDto delete(@RequestBody IdDto dto) {

        Coupon coupon = couponService.get(dto.id);
        Company company = customSecurityContext.getCurrentPrincipal().getCompany();

        if (!coupon.getCompany().getId().equals(company.getId())) {
            throw new RuntimeException("Попытка выполнить операцию с чужой акцией");
        }


        couponService.delete(coupon);

        return StatusDtoFactory.buildSuccess();
    }


    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public CustomDto commentCoupon(@RequestBody IncomeCouponCommentDto dto) {

        Coupon coupon = couponService.get(dto.couponId);
        Customer customer = customSecurityContext.getCurrentPrincipal().getCustomer();

        CouponComment couponComment = new CouponComment();

        couponComment.setText(dto.text);
        couponComment.setCoupon(coupon);
        couponComment.setCustomer(customer);

        return couponCommentService.addCommentToCoupon(couponComment)
            .toCreatedDto();
    }


    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET)
    public CustomDto getCommentPageByCoupon(
        @PathVariable(value = "id") Long couponId,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit) {

        User user = customSecurityContext.getCurrentPrincipal();
        Coupon coupon = couponService.get(couponId);

        CouponCommentPageDto commentPage = new CouponCommentPageDto();

        List<CouponCommentDto> comments = couponCommentService.getPageByQuery(
            new CouponCommentQuery(couponId, skip, limit)
        ).stream()
            .map(CouponComment::toDto)
            .collect(Collectors.toList());

        commentPage.comments = comments;

        if (user != null && user.getCustomer() != null) {
            commentPage.canComment = !couponCommentService
                .isCouponCommentedByCustomer(coupon, user.getCustomer());
        }

        return commentPage;
    }


    @RequestMapping(value = "/share", method = RequestMethod.POST)
    public CustomDto share(@RequestBody IdDto dto) {
        Coupon coupon = couponService.get(dto.id);
        couponService.incSharedCount(coupon);

        return StatusDtoFactory.buildSuccess();
    }


    /**
     * Позволяет заполнить страницу акция индивидуальными данными,
     * например информацией о лайках, подписках и избранных
     *
     * @param dtoList
     * @param user
     */
    private void fillPageByIndividualInfo(List<ClientCouponFullDto> dtoList, User user, boolean isUser) {
        if (user != null && user.getCustomer() != null) {
            Customer customer = user.getCustomer();

            List<Long> watchesIds = couponService.getWatchedIdsByCustomer(customer);
            List<Long> likedIds = couponService.getLikedIdsByCustomer(customer);
            List<Long> favoriteIds = couponService.getFavoriteIdsByCustomer(customer);
            List<Long> subscriptionIds = companyService.getSubscriptionIdsByCustomer(customer);
            HashMap<Long,Long> fids = couponService.getFIdsByCustomer(customer);

            dtoList.forEach(c -> {
                    c.canLike = !likedIds.contains(c.id);
                    c.isFavorite = favoriteIds.contains(c.id);
                    c.canSubscribe = !subscriptionIds.contains(c.companyId);
                    c.is_new = !watchesIds.contains(c.id);
                    c.fid = fids.get(c.id);
                }
            );

            if(isUser) {
                Collections.sort(dtoList, new Comparator<ClientCouponFullDto>() {
                    @Override
                    public int compare(ClientCouponFullDto lhs, ClientCouponFullDto rhs) {
                        // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                        return lhs.fid > rhs.fid ? -1 : (lhs.fid < rhs.fid ? 1 : 0);
                    }
                });
            }
        }
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }



}
