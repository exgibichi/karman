package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ioann on 20.06.16
 */
@Controller
@RequestMapping("/")
public class ViewController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(ViewController.class.getName());


    private HttpServletRequest request;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String companyView() {
        return "company";
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
