package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.RoleEnum;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.dto.account.CompanyPrincipalDto;
import ru.cproject.karman.dto.account.PrincipalDto;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.web.base.AbstractCprojectRestController;
import ru.cproject.karman.service.UserService;

/**
 * Created by ioann on 31.05.16
 */
@RestController
@RequestMapping("/user")
public class UserController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());

    private CustomSecurityContext customSecurityContext;
    private UserService userService;

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/gcmtoken", method = RequestMethod.POST)
    public CustomDto gcmToken(@RequestBody String token) {
        User user = customSecurityContext.getCurrentPrincipal();

        if (user == null) {
            throw new RuntimeException("Нет такого юзера");
        }
        user.setToken(token);
        userService.edit(user);
        return StatusDtoFactory.buildSuccess();
    }

    @RequestMapping(value = "/setRegion", method = RequestMethod.POST)
    public CustomDto setRegion(@RequestBody Long region_id) {
        User user = customSecurityContext.getCurrentPrincipal();

        if (user == null) {
            throw new RuntimeException("Нет такого юзера");
        }
        user.setRegion(region_id);
        userService.edit(user);
        return StatusDtoFactory.buildSuccess();
    }

    @RequestMapping(value = "/principal", method = RequestMethod.GET)
    public CustomDto principal() {
        User user = customSecurityContext.getCurrentPrincipal();

        if (user == null) {
            return null;
        }

        PrincipalDto dto = null;

        if (user.getCustomer() != null) {
            dto = new PrincipalDto();
            dto.role = RoleEnum.CUSTOMER;
        } else if (user.getCompany() != null) {
            dto = new CompanyPrincipalDto();
            dto.role = RoleEnum.COMPANY;
            ((CompanyPrincipalDto) dto).balance = user.getCompany().getBalance();
            ((CompanyPrincipalDto) dto).logo = user.getCompany().getLogo();
            ((CompanyPrincipalDto) dto).isActive = user.getCompany().isActive();
            ((CompanyPrincipalDto) dto).moderatorsComment = user.getCompany().getModeratorsComment();
            ((CompanyPrincipalDto) dto).inModeration = user.getCompany().inModeration();
        }

        if (dto != null) {
            dto.phoneNumber = user.getPhoneNumber();
            dto.name = user.getName();
            dto.region_id = user.getRegion();
        }

        return dto;
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
