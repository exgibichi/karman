package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.CountDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.CouponService;
import ru.cproject.karman.service.CustomerService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 16.06.16
 */
@RestController
@RequestMapping("/favorite")
public class FavoriteCouponsController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(FavoriteCouponsController.class.getName());


    private CustomerService customerService;
    private CouponService couponService;
    private CustomSecurityContext customSecurityContext;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }



    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CustomDto addCouponToFavorites(@RequestBody IdDto dto) {

        Customer customer = customSecurityContext.getCurrentPrincipal().getCustomer();
        Coupon coupon = couponService.get(dto.id);

        return StatusDtoFactory.buildBoolean(
            customerService.addCouponToFavorites(customer, coupon, false)
        );
    }



    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public CustomDto removeCouponFromFavorites(@RequestBody IdDto dto) {

        Customer customer = customSecurityContext.getCurrentPrincipal().getCustomer();
        Coupon coupon = couponService.get(dto.id);

        return StatusDtoFactory.buildBoolean(
            customerService.removeCouponFromFavorites(customer, coupon)
        );
    }


    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/my/count", method = RequestMethod.GET)
    public CustomDto getCountOfFavoriteByUser() {
        Customer customer = customSecurityContext.getCurrentPrincipal()
            .getCustomer();

        return new CountDto(
            customerService.getCountOfFavoriteByCustomer(customer)
        );
    }





    @Secured(Roles.CUSTOMER)
    @RequestMapping(value = "/my/new", method = RequestMethod.GET)
    public CustomDto getCountOfNewFavoriteByCurrentUser() {
        Customer customer = customSecurityContext.getCurrentPrincipal()
            .getCustomer();

        return new CountDto(
            customerService.getCountOfFavoriteByCustomerAndAreNotShowed(customer)
        );
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
