package ru.cproject.karman.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.dao.jpa.query.account.CompanyQuery;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.dto.account.AvatarDto;
import ru.cproject.karman.dto.account.CompanyClientDto;
import ru.cproject.karman.dto.account.CompanyDto;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.CompanyService;
import ru.cproject.karman.service.CustomerService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 06.06.16
 */
@RestController
@RequestMapping("/companies")
public class CompanyController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(CompanyController.class.getName());

    private CompanyService companyService;
    private CustomerService customerService;
    private CustomSecurityContext customSecurityContext;


    @Autowired
    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public CustomDto getCurrent() {
        User user = customSecurityContext.getCurrentPrincipal();
        Company company = user.getCompany();

        return companyService.get(company.getId()).toDto();
    }


    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public Integer getBalance() {
        return customSecurityContext.getCurrentPrincipal()
            .getCompany().getBalance();
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public CustomDto edit(@RequestBody CompanyDto dto) {
        User user = customSecurityContext.getCurrentPrincipal();
        Company company = user.getCompany();

        Company editCompany = new Company(dto, company.getId(), user.getId());

        return companyService.edit(editCompany).toDto();
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/updateAvatar", method = RequestMethod.POST)
    public AvatarDto updateAvatar(@RequestBody AvatarDto dto) {
        User user = customSecurityContext.getCurrentPrincipal();
        Company company = user.getCompany();

        dto.avatar = companyService.updateAvatar(dto.avatar, company.getId());

        return dto;
    }

    @Secured(Roles.COMPANY)
    @RequestMapping(value = "/deleteAvatar", method = RequestMethod.POST)
    public CustomDto deleteAvatar() {
        User user = customSecurityContext.getCurrentPrincipal();
        Company company = user.getCompany();
        companyService.deleteAvatar(company);

        return StatusDtoFactory.buildSuccess();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CustomDto getCompany(@PathVariable(value = "id") Long id) {

        Company company = companyService.get(id);
        CompanyClientDto result = company.toClientDto();

        User user = customSecurityContext.getCurrentPrincipal();

        if (user != null && user.getCustomer() != null) {
            result.canSubscribe = !customerService.
                isCustomerSubscribedToCompany(user.getCustomer(), company);
        }

        return result;
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<CompanyClientDto> search(
        @RequestParam(name = "search", required = false) String search,
        @RequestParam(name = "skip", required = false, defaultValue = "0") int skip,
        @RequestParam(name = "limit", required = false, defaultValue = "0") int limit
    ) {
        CompanyQuery companyQuery = new CompanyQuery(search, skip, limit);

        return companyService.getPageByQuery(companyQuery)
            .stream()
            .map(Company::toClientDto)
            .collect(Collectors.toList());
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
