package ru.cproject.karman.web.base;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.enums.ErrorCode;

/**
 * Created by ioann on 27.05.16.
 * Абстрактный предок для всех RestController'ов
 */
public abstract class AbstractCprojectRestController {

    @ExceptionHandler(Exception.class)
    public CustomDto handleError(Exception e) {

        if (e instanceof DataIntegrityViolationException
            && e.getMessage().contains("constraint [not_negative_balance]")) {

            return StatusDtoFactory.buildError(
                "Недостаточно средств для осуществления операции",
                ErrorCode.NOT_ENOUGH_MONEY
            );
        }

        logger().error(e.getMessage(), e);
        return StatusDtoFactory.buildError(e.getMessage());
    }

    protected abstract Logger logger();

}
