package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.dto.account.CompanyModerationDto;
import ru.cproject.karman.dto.business.coupon.ModerateRejectionDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.service.CompanyService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 28.06.16
 */
@RestController
@RequestMapping("/admin/companies")
public class AdminCompanyController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminCompanyController.class.getName());


    private CompanyService companyService;


    @Autowired
    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }


    @RequestMapping("/allReview")
    public List<CompanyModerationDto> getAllToReview() {
        return companyService.getAllToReview()
            .stream()
            .map(Company::toModerationDto)
            .collect(Collectors.toList());
    }


    @RequestMapping(value = "/rejectModeration", method = RequestMethod.POST)
    public void rejectModeration(@RequestBody ModerateRejectionDto dto) {
        companyService.rejectModeration(dto.id, dto.rejectionReason);
    }

    @RequestMapping(value = "/confirmModeration", method = RequestMethod.POST)
    public void confirmModeration(@RequestBody IdDto dto) {
        companyService.confirmModeration(dto.id);
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
