package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.dto.business.IncomePriceDto;
import ru.cproject.karman.service.business.PriceService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 25.07.16
 */
@RestController
@RequestMapping("/admin/prices")
public class AdminPriceController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminPriceController.class.getName());


    private PriceService priceService;

    @Autowired
    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void update(@RequestBody IncomePriceDto dto) {
        priceService.update(dto.type, dto.price);
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
