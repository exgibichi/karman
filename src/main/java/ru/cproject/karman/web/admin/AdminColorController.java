package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.domain.common.Color;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.common.color.IncomeColorDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.common.ColorService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 08.07.16
 */
@RestController
@RequestMapping("/admin/colors")
public class AdminColorController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminColorController.class.getName());


    private ColorService colorService;

    @Autowired
    public void setColorService(ColorService colorService) {
        this.colorService = colorService;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CustomDto save(@RequestBody IncomeColorDto dto) {
        Color color = new Color(dto);

        return colorService.save(color).toDto();
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public CustomDto delete(@RequestBody IdDto dto) {
        colorService.delete(dto.id);
        return StatusDtoFactory.buildSuccess();
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
