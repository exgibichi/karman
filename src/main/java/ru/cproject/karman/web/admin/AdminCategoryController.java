package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.common.category.IncomeCategoryDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.CategoryService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 30.05.16
 */
@RestController
@RequestMapping("/admin/categories")
public class AdminCategoryController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminCategoryController.class.getName());


    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CustomDto save(@RequestBody IncomeCategoryDto dto) {
        return categoryService.save(new Category(dto)).toDto();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CustomDto edit(@RequestBody IncomeCategoryDto dto) {
        categoryService.update(dto.id, dto.title);
        return StatusDtoFactory.buildSuccess();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public CustomDto delete(@RequestBody IdDto dto) {
        categoryService.delete(dto.id);
        return StatusDtoFactory.buildSuccess();
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
