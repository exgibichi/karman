package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.business.coupon.CouponFilterDto;
import ru.cproject.karman.dto.business.coupon.ModerateRejectionDto;
import ru.cproject.karman.dto.business.coupon.OutcomeCouponShortDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.service.CouponService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ioann on 03.06.16
 */
@RestController
@RequestMapping("/admin/coupons")
public class AdminCouponController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminCouponController.class.getName());

    private CouponService couponService;

    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.POST)
    public List<OutcomeCouponShortDto> getAllToReview(@RequestBody CouponFilterDto dto) {
        return couponService.getAllToReview()
            .stream()
            .map(Coupon::toShortDto)
            .collect(Collectors.toList());
    }


    @RequestMapping(path = "/{id}")
    public CustomDto getOne(@PathVariable Long id) {
        return couponService.get(id).toFullDto();
    }


    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public CustomDto confirm(@RequestBody IdDto dto) {
        couponService.confirm(dto.id);
        return StatusDtoFactory.buildSuccess();
    }

    @RequestMapping(value = "/reject", method = RequestMethod.POST)
    public CustomDto reject(@RequestBody ModerateRejectionDto dto) {
        couponService.reject(dto.id, dto.rejectionReason);
        return StatusDtoFactory.buildSuccess();
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
