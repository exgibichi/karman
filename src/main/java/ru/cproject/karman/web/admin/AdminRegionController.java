package ru.cproject.karman.web.admin;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.IdDto;
import ru.cproject.karman.dto.common.IncomeRegionDto;
import ru.cproject.karman.dto.status.StatusDtoFactory;
import ru.cproject.karman.service.RegionService;
import ru.cproject.karman.web.base.AbstractCprojectRestController;

/**
 * Created by ioann on 08.07.16
 */
@RestController
@RequestMapping("/admin/regions")
public class AdminRegionController extends AbstractCprojectRestController {

    private static final Logger LOGGER = Logger.getLogger(AdminRegionController.class.getName());


    private RegionService regionService;

    @Autowired
    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CustomDto save(@RequestBody IncomeRegionDto dto) {
        Region region = new Region(dto);

        return regionService.save(region).toDto();
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public CustomDto delete(@RequestBody IdDto dto) {
        regionService.delete(dto.id);
        return StatusDtoFactory.buildSuccess();
    }


    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
