package ru.cproject.karman.constants;

/**
 * Created by ioann on 20.06.16
 */
public class CookieConstants {

    public static final String SUCCESS_ROBOKASSA_PAYMENT = "SUCCESS_ROBOKASSA_PAYMENT";
    public static final String FAIL_ROBOKASSA_PAYMENT = "FAIL_ROBOKASSA_PAYMENT";

    private CookieConstants() {
        throw new AssertionError();
    }

}
