package ru.cproject.karman.enums;

/**
 * Created by ioann on 18.07.16
 */
public enum ErrorCode {

    NOT_ENOUGH_MONEY(900);

    private int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
