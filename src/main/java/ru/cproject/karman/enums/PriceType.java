package ru.cproject.karman.enums;

/**
 * Created by ioann on 25.07.16
 */
public enum PriceType {

    BACKGROUND_COLOR, // За день цветного фона
    RAISING, // Поднятие
    FIXING, // закрепление
    TO_POCKETS //Раскидать по карманам

}
