package ru.cproject.karman.enums;

/**
 * Created by ioann on 01.06.16
 */
public enum CouponStatus {

    REVIEW,
    REJECTED,
    ACTIVE,
    PAUSED,
    FINISHED

}
