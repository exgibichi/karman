package ru.cproject.karman.enums;

/**
 * Created by ioann on 19.06.16
 */
public enum PaymentStatus {

    NEW,
    SUCCESS,
    FAILED,
    TEST

}
