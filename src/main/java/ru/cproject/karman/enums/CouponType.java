package ru.cproject.karman.enums;

/**
 * Created by ioann on 04.07.16
 * Перечисление возможных типов акции
 */
public enum CouponType {

    DISCOUNT,
    GIFT,
    DISCOUNT_GIFT,
    PRICE

}
