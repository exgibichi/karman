package ru.cproject.karman.validator;

import org.springframework.util.StringUtils;
import ru.cproject.karman.exception.FieldValidationException;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ioann on 27.05.16
 */
public class FieldValidator {

    private FieldValidator() {
        throw new AssertionError();
    }

    public static void validatePhoneNumber(String phoneNumber) {
        
        if (StringUtils.isEmpty(phoneNumber)) {
            throw new FieldValidationException("Не был введен номер телефона");
        }

        Pattern phoneNumberPattern = Pattern.compile("^\\+7\\d{10}$");
        Matcher phoneNumberMatcher = phoneNumberPattern.matcher(phoneNumber.trim());

        if (!phoneNumberMatcher.matches()) {
            throw new FieldValidationException("Неверный формат номера телефона");
        }

    }


    public static boolean validateStringNotEmpty(String string) {
        if (string == null || string.trim().isEmpty()) {
            return false;
        }

        return true;
    }

    public static void validateHexColor(String hexColor) {


        Pattern hexColorPattern = Pattern.compile("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
        Matcher hexColorMatcher = hexColorPattern.matcher(hexColor);

        if (!hexColorMatcher.matches()) {
            throw new FieldValidationException("Некорректное значение цвета фона");
        }
    }

}
