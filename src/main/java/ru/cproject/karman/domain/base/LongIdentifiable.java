package ru.cproject.karman.domain.base;

/**
 * Created by ioann on 25.05.16.
 */
public class LongIdentifiable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
