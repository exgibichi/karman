package ru.cproject.karman.domain.business;

import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.enums.PaymentStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by ioann on 19.06.16
 */
public class PaymentHistory extends LongIdentifiable {

    private LocalDateTime paymentDate;
    private PaymentStatus status;
    private Company company;
    private BigDecimal amountOfMoney;
    private String info;

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(BigDecimal amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
