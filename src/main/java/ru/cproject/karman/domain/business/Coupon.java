package ru.cproject.karman.domain.business;

import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.dto.business.coupon.*;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.enums.CouponType;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

/**
 * Created by ioann on 01.06.16
 */
public class Coupon extends LongIdentifiable {

    private Company company;
    private Region region;
    private Category category;
    private String title;
    private List<String> images;
    private Integer discount;
    private Integer price;
    private String giftTitle;
    private String giftImage;
    private String giftCondition;
    private String giftDescription;
    private String description;
    private LocalDateTime created;
    private LocalDateTime dateTo;
    private LocalDateTime fixingDate;
    private CouponStatus status;
    private String backgroundColorHex;
    private boolean isBackgroundColorHexPayed;
    private String flagColorHex;
    private String rejectionReason;
    private Set<CouponContact> contacts = new LinkedHashSet<>();
    private Coupon parent;
    private Coupon child;
    private Long likesCount;
    private Long commentsCount;
    private Long sharedCount;


    public Coupon() { }

    public Coupon(IncomeCouponDto dto) {
        setId(dto.id);
        region = new Region();
        region.setId(dto.regionId);

        category = new Category();
        category.setId(dto.categoryId);

        title = dto.title;
        images = dto.images;
        discount = dto.discount;
        price = dto.price;
        giftTitle = dto.giftTitle;
        giftImage = dto.giftImage;
        giftCondition = dto.giftCondition;
        giftDescription = dto.giftDescription;
        description = dto.description;

        if (dto.backgroundColorHex != null) {
            backgroundColorHex = dto.backgroundColorHex.toUpperCase().trim();
        }

        flagColorHex = dto.flagColorHex;

        dateTo = dto.dateTo;

        dto.contacts
            .forEach(c -> contacts.add(new CouponContact(c)));
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public String getGiftTitle() {
        return giftTitle;
    }

    public void setGiftTitle(String giftTitle) {
        this.giftTitle = giftTitle;
    }

    public String getGiftImage() {
        return giftImage;
    }

    public void setGiftImage(String giftImage) {
        this.giftImage = giftImage;
    }

    public String getGiftCondition() {
        return giftCondition;
    }

    public void setGiftCondition(String giftCondition) {
        this.giftCondition = giftCondition;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGiftDescription() {
        return giftDescription;
    }

    public void setGiftDescription(String giftDescription) {
        this.giftDescription = giftDescription;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public CouponStatus getStatus() {
        return status;
    }

    public void setStatus(CouponStatus status) {
        this.status = status;
    }

    public String getBackgroundColorHex() {
        return backgroundColorHex;
    }

    public void setBackgroundColorHex(String backgroundColorHex) {
        this.backgroundColorHex = backgroundColorHex;
    }

    public boolean isBackgroundColorHexPayed() {
        return isBackgroundColorHexPayed;
    }

    public void setBackgroundColorHexPayed(boolean backgroundColorHexPayed) {
        isBackgroundColorHexPayed = backgroundColorHexPayed;
    }

    public String getFlagColorHex() {
        return flagColorHex;
    }

    public void setFlagColorHex(String flagColorHex) {
        this.flagColorHex = flagColorHex;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDateTime getFixingDate() {
        return fixingDate;
    }

    public void setFixingDate(LocalDateTime fixingDate) {
        this.fixingDate = fixingDate;
    }

    public Set<CouponContact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<CouponContact> contacts) {
        this.contacts = contacts;
    }

    public Coupon getParent() {
        return parent;
    }

    public void setParent(Coupon parent) {
        this.parent = parent;
    }

    public Coupon getChild() {
        return child;
    }

    public void setChild(Coupon child) {
        this.child = child;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    public Long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Long commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Long getSharedCount() {
        return sharedCount;
    }

    public void setSharedCount(Long sharedCount) {
        this.sharedCount = sharedCount;
    }



    public OutcomeCouponShortDto toShortDto() {
        OutcomeCouponShortDto dto = new OutcomeCouponShortDto();

        dto.id = getId();
        dto.companyName = company.getUser().getName();
        dto.title = title;

        if (description.length() > 10) {
            dto.description = description.substring(0, 10).concat("...");
        } else {
            dto.description = description;
        }

        dto.created = created;

        return dto;
    }


    public OutcomeCouponFullDto toFullDto() {
        OutcomeCouponFullDto dto = new OutcomeCouponFullDto();

        dto.id = getId();
        dto.companyName = company.getUser().getName();

        if (region != null && region.getTitle() != null) {
            dto.region = region.toDto();
        }

        if (category != null && category.getTitle() != null) {
            dto.category = category.toDto();
        }

        dto.title = title;
        dto.images = images;

        dto.couponType = toCouponTypeDto();

        dto.description = description;

        dto.created = created.toInstant(ZoneOffset.UTC).toEpochMilli();
        dto.dateTo = dateTo.toInstant(ZoneOffset.UTC).toEpochMilli();

        if (fixingDate != null) {
            dto.fixingDate = fixingDate.toInstant(ZoneOffset.UTC).toEpochMilli();
        }

        dto.status = status;
        dto.backgroundColorHex = backgroundColorHex;
        dto.isBackgroundColorHexPayed = isBackgroundColorHexPayed;
        dto.flagColorHex = flagColorHex;
        dto.rejectionReason = rejectionReason;

        contacts.forEach(
            c -> dto.contacts.add(c.toDto())
        );

        if (child != null) {
            dto.hasShadow = true;

            if (CouponStatus.REJECTED.equals(child.getStatus())) {
                dto.rejectionReason = child.getRejectionReason();
            }

        }

        dto.likesCount = likesCount;
        dto.commentsCount = commentsCount;
        dto.sharedCount = sharedCount;

        return dto;
    }

    public ClientCouponFullDto toClientFullDto() {
        ClientCouponFullDto dto = new ClientCouponFullDto();

        dto.id = getId();
        dto.companyId = company.getId();
        dto.companyName = company.getUser().getName();

        if (region != null && region.getTitle() != null) {
            dto.region = region.toDto();
        }

        if (category != null && category.getTitle() != null) {
            dto.category = category.toDto();
        }

        dto.title = title;
        dto.images = images;

        dto.couponType = toCouponTypeDto();

        dto.description = description;
        dto.created = created.toInstant(ZoneOffset.UTC).toEpochMilli();
        dto.dateTo = dateTo.toInstant(ZoneOffset.UTC).toEpochMilli();
        dto.status = status;
        dto.backgroundColorHex = backgroundColorHex;
        dto.flagColorHex = flagColorHex;
        dto.rejectionReason = rejectionReason;


        dto.contacts = new ContactsDto();

        contacts.forEach(
            c -> {
                dto.contacts.phoneNumbers.add(c.getPhoneNumber());
                dto.contacts.addresses.add(c.toDto());
            }
        );

        if (child != null) {
            dto.hasShadow = true;

            if (CouponStatus.REJECTED.equals(child.getStatus())) {
                dto.rejectionReason = child.getRejectionReason();
            }

        }

        dto.likesCount = likesCount;
        dto.commentsCount = commentsCount;
        dto.sharedCount = sharedCount;

        return dto;
    }


    public CouponTypeDto toCouponTypeDto() {

        CouponTypeDto dto = new CouponTypeDto();

        if (discount != null & giftDescription != null) {
            dto.type = CouponType.DISCOUNT_GIFT;
        } else if (discount != null) {
            dto.type = CouponType.DISCOUNT;
        } else if (giftDescription != null) {
            dto.type = CouponType.GIFT;
        } else if (price != null) {
            dto.type = CouponType.PRICE;
        }

        dto.discount = discount;
        dto.price = price;
        dto.giftTitle = giftTitle;
        dto.giftImage = giftImage;
        dto.giftDescription = giftDescription;
        dto.giftCondition = getGiftCondition();

        if (CouponType.GIFT.equals(dto.type) || CouponType.DISCOUNT_GIFT.equals(dto.type)) {
            dto.giftImages = Arrays.asList(giftImage);
        }

        return dto;
    }

}
