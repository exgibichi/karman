package ru.cproject.karman.domain.business;

import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.dto.business.coupon.comment.CouponCommentCreatedDto;
import ru.cproject.karman.dto.business.coupon.comment.CouponCommentDto;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Created by ioann on 05.07.16
 */
public class CouponComment extends LongIdentifiable {

    private String text;
    private Coupon coupon;
    private Customer customer;
    private LocalDateTime created;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }


    public CouponCommentCreatedDto toCreatedDto() {
        CouponCommentCreatedDto dto = new CouponCommentCreatedDto();

        dto.created = created.toInstant(ZoneOffset.UTC).toEpochMilli();

        return dto;
    }

    public CouponCommentDto toDto() {
        CouponCommentDto dto = new CouponCommentDto();

        dto.couponId = coupon.getId();
        dto.authorId = customer.getId();
        dto.authorName = customer.getUser().getName();
        dto.text = text;
        dto.created = created.toInstant(ZoneOffset.UTC).toEpochMilli();

        return dto;
    }
}
