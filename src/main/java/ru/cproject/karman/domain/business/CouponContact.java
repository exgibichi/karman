package ru.cproject.karman.domain.business;

import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.dto.business.coupon.ContactDto;

/**
 * Created by ioann on 07.07.16
 */
public class CouponContact extends LongIdentifiable {

    private String address;
    private String phoneNumber;
    private String latitude;
    private String longitude;


    public CouponContact() { }

    public CouponContact(ContactDto dto) {
        this.address = dto.address;
        this.phoneNumber = dto.phoneNumber;
        this.longitude = dto.longitude;
        this.latitude = dto.latitude;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ContactDto toDto() {
        ContactDto dto = new ContactDto();

        dto.address = address;
        dto.phoneNumber = phoneNumber;
        dto.latitude = latitude;
        dto.longitude = longitude;

        return dto;
    }
}


