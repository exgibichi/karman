package ru.cproject.karman.domain.business;

import java.math.BigDecimal;

/**
 * Created by ioann on 19.06.16
 */
public class IncomeRobokassaPayment {

    private Long companyId;
    //Номер счета в магазине робокассы
    private Long invId;
    private boolean isTest;
    private BigDecimal amountOfMoney;


    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public boolean isTest() {
        return isTest;
    }

    public void setTest(boolean test) {
        isTest = test;
    }

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(BigDecimal amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }
}
