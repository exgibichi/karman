package ru.cproject.karman.domain.business;

import ru.cproject.karman.domain.base.LongIdentifiable;

import java.time.LocalDateTime;

/**
 * Created by ioann on 10.07.16
 */
public class CouponReview extends LongIdentifiable {

    private Coupon coupon;
    private String token;
    private LocalDateTime date;


    public CouponReview() { }

    public CouponReview(Coupon coupon, String token) {
        this.coupon = coupon;
        this.token = token;
    }


    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
