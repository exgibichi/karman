package ru.cproject.karman.domain.statistics;

import java.time.LocalDateTime;

/**
 * Created by ioann on 11.07.16
 */
public class LocalDateTimeAndCountStats {

    private LocalDateTime localDateTime;
    private int count;


    public LocalDateTimeAndCountStats(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }


    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public int getCount() {
        return count;
    }

    public void incCount() {
        this.count++;
    }


    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (obj instanceof LocalDateTimeAndCountStats) {
            LocalDateTimeAndCountStats that = (LocalDateTimeAndCountStats)obj;

            return this.localDateTime.equals(that.localDateTime);
        }

        return false;
    }


    @Override
    public int hashCode() {

        int result = 17;

        result = result * 31 + localDateTime.getYear();
        result = result * 31 + localDateTime.getMonthValue();
        result = result * 31 + localDateTime.getDayOfMonth();

        return result;
    }

}
