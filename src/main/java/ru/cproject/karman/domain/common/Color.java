package ru.cproject.karman.domain.common;

import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.dto.common.color.ColorDto;
import ru.cproject.karman.dto.common.color.IncomeColorDto;

/**
 * Created by ioann on 08.07.16
 */
public class Color extends LongIdentifiable {

    private String colorHex;


    public Color() { }

    public Color(IncomeColorDto dto) {
        this.colorHex = dto.colorHex;
    }

    public String getColorHex() {
        return colorHex;
    }

    public void setColorHex(String colorHex) {
        this.colorHex = colorHex;
    }


    public ColorDto toDto() {
        ColorDto dto = new ColorDto();

        dto.id = getId();
        dto.colorHex = colorHex;

        return dto;
    }
}
