package ru.cproject.karman.domain.common;

import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.dto.common.IncomeRegionDto;
import ru.cproject.karman.dto.common.RegionDto;

/**
 * Created by ioann on 31.05.16
 */
public class Region {

    private String title;
    private String parent_title;
    private Region parent;

    private Long id;

    public Region() { }

    public Region(IncomeRegionDto dto) {
        parent = new Region();
        parent.setId(dto.parentId);
        title = dto.title;
    }

    public String getTitle() {
        return title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentTitle() {
        return parent_title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setParentTitle(String title) {
        this.parent_title = title;
    }

    public Region getParent(){
        return parent;
    }

    public void setParent(Region parent) {
        this.parent = parent;
    }

    public Region toDomain() {
        Region region = new Region();
        region.setId(getId());
        region.setTitle(getTitle());
        region.setParent(getParent());
        return region;
    }

    public RegionDto toDto() {
        RegionDto dto = new RegionDto();

        dto.id = getId();
        dto.title = title;
        dto.parent_title = parent_title;
        return dto;
    }
}
