package ru.cproject.karman.domain.common;

import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.dto.common.category.IncomeCategoryDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;

/**
 * Created by ioann on 30.05.16
 */
public class Category extends LongIdentifiable {

    private String title;


    public Category() { }

    public Category(IncomeCategoryDto dto) {
        title = dto.title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public FullCategoryDto toDto() {
        FullCategoryDto dto = new FullCategoryDto();

        dto.id = getId();
        dto.title = title;

        return dto;
    }
}
