package ru.cproject.karman.domain.account;

import ru.cproject.karman.domain.base.LongIdentifiable;

import java.time.LocalDateTime;

/**
 * Created by ioann on 25.05.16.
 */
public class User extends LongIdentifiable {

    private String name;
    private String phoneNumber;
    private String passwordHash;
    private String token;
    private Long region_id;
    private LocalDateTime created;
    private Customer customer;
    private Company company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRegion() {
        return region_id;
    }

    public void setRegion(Long region_id) {
        this.region_id = region_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
