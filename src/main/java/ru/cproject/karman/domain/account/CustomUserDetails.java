package ru.cproject.karman.domain.account;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.cproject.karman.conf.security.Roles;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ioann on 25.05.16.
 */
public class CustomUserDetails implements UserDetails {

    private String userName;
    private String password;
    private Set<GrantedAuthority> authorities = new HashSet<>();

    public CustomUserDetails(User user) {
        userName = user.getPhoneNumber();
        password = user.getPasswordHash();

        if (user.getCustomer() != null) {
            authorities.add(new SimpleGrantedAuthority(Roles.CUSTOMER));
        }
        if (user.getCompany() != null) {
            authorities.add(new SimpleGrantedAuthority(Roles.COMPANY));
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
