package ru.cproject.karman.domain.account;

import ru.cproject.karman.domain.base.LongIdentifiable;

/**
 * Created by ioann on 25.05.16.
 */
public class Customer extends LongIdentifiable {

    private User user;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
