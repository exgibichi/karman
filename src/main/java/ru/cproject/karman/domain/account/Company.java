package ru.cproject.karman.domain.account;

import ru.cproject.karman.domain.base.LongIdentifiable;
import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.dto.account.CompanyClientDto;
import ru.cproject.karman.dto.account.CompanyDto;
import ru.cproject.karman.dto.account.CompanyModerationDto;
import ru.cproject.karman.dto.business.coupon.ContactDto;
import ru.cproject.karman.dto.business.coupon.ContactsDto;

/**
 * Created by ioann on 25.05.16.
 */
public class Company extends LongIdentifiable {

    private User user;
    private String email;
    private String fullName;
    private int balance;
    private Category category;
    private String description;
    private String logo;
    private String siteUrl;
    private String address;
    private String longitude;
    private String latitude;
    private String contactPhoneNumber;
    private boolean isActive;
    private String moderatorsComment;
    private String newName;
    private String newEmail;
    private String newFullName;


    public Company() { }

    public Company(CompanyDto dto, Long id, Long userId) {
        setId(id);

        user = new User();
        user.setId(userId);
        user.setName(dto.name);

        category = new Category();

        if (dto.category != null) {
            category.setId(dto.category.id);
        }

        description = dto.description;

        siteUrl = dto.siteUrl;
        address = dto.address;
        longitude = dto.longitude;
        latitude = dto.latitude;
        contactPhoneNumber = dto.contactPhoneNumber;
        email = dto.email;
        fullName = dto.fullName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getModeratorsComment() {
        return moderatorsComment;
    }

    public void setModeratorsComment(String moderatorsComment) {
        this.moderatorsComment = moderatorsComment;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getNewFullName() {
        return newFullName;
    }

    public void setNewFullName(String newFullName) {
        this.newFullName = newFullName;
    }


    /**
     * Позволяет узнать, находится ли учетная запись компании в модерации
     * Метод нужно вызывать на объектах, полученных из Entity
     * @return
     */
    public boolean inModeration() {

        //неактивная учетная запись без комментариев админа означает модерацию
        if (!isActive && moderatorsComment == null) {
            return true;
        }

        //Ненулевая ссылка на одно из редактируемых полей решистрационных данных означает модерацию
        if (newName != null || newEmail != null || newFullName != null) {
            return true;
        }

        return false;
    }


    public CompanyDto toDto() {
        CompanyDto dto = new CompanyDto();

        dto.id = getId();

        if (category != null) {
            dto.category = category.toDto();
        }

        dto.description = description;
        dto.name = user.getName();
        dto.siteUrl = siteUrl;
        dto.address = address;
        dto.longitude = longitude;
        dto.latitude = latitude;
        dto.contactPhoneNumber = contactPhoneNumber;
        dto.email = email;
        dto.fullName = fullName;

        if (inModeration()) {
            dto.inModeration = true;
        }

        return dto;
    }

    public CompanyClientDto toClientDto() {
        CompanyClientDto dto = new CompanyClientDto();

        dto.id = getId();
        dto.title = user.getName();
        dto.image = logo;
        dto.description = description;
        dto.siteUrl = siteUrl;

        ContactsDto contactsDto = new ContactsDto();

        if (address != null && !address.trim().isEmpty()) {
            ContactDto contactDto = new ContactDto();
            contactDto.address = address;
            contactDto.longitude = longitude;
            contactDto.latitude = latitude;
            contactsDto.addresses.add(contactDto);
        }

        if (contactPhoneNumber != null && !contactPhoneNumber.isEmpty()) {
            contactsDto.phoneNumbers.add(contactPhoneNumber);
        }

        dto.contacts = contactsDto;

        if (category != null) {
            dto.category = category.toDto();
        }

        return dto;
    }

    public CompanyModerationDto toModerationDto() {
        CompanyModerationDto dto = new CompanyModerationDto();

        dto.id = getId();
        dto.title = newName != null ? newName : user.getName();
        dto.phoneNumber = user.getPhoneNumber();
        dto.fullName = newFullName != null ? newFullName : fullName;
        dto.email = newEmail != null ? newEmail : email;

        return dto;
    }

}
