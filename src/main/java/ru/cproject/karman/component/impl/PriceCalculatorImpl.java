package ru.cproject.karman.component.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.cproject.karman.component.PriceCalculator;
import ru.cproject.karman.dao.jpa.repositories.business.PriceRepository;
import ru.cproject.karman.enums.PriceType;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by ioann on 18.07.16
 */
@Component
public class PriceCalculatorImpl implements PriceCalculator {


    private PriceRepository priceRepository;


    @Autowired
    public void setPriceRepository(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }


    @Override
    public int calcPriceForBackgroundColor(LocalDateTime dateTo) {
        LocalDateTime now = LocalDateTime.now();

        if (now.isAfter(dateTo)) {
            throw new RuntimeException("Дата не может быть указана в прошедшем времени");
        }

        LocalDateTime tempDateTime = LocalDateTime.from(now);

        //Целое число дней до конца акции
        long days = tempDateTime.until(dateTo, ChronoUnit.DAYS);
        tempDateTime.plusDays(days);

        long minutesOfLastDay = tempDateTime.until(dateTo, ChronoUnit.MINUTES);

        if (minutesOfLastDay > 0) {
            ++days;
        }


        return getBackgroundColorPerDayPrice() * (int)days;
    }


    @Override
    public int calcPriceForRaising() {
        return getRaisingPrice();
    }


    @Override
    public int calcPriceForFixing() {
        return getFixingPrice();
    }


    @Override
    public int calcPriceForForceAddingToFavorites(int countOfAdditions) {
        return getForceAddingToFavoritesPerCustomerPrice() * countOfAdditions;
    }

    @Override
    public int getBackgroundColorPerDayPrice() {
        return priceRepository.findOneByType(PriceType.BACKGROUND_COLOR).getPrice();
    }

    @Override
    public int getRaisingPrice() {
        return priceRepository.findOneByType(PriceType.RAISING).getPrice();
    }

    @Override
    public int getFixingPrice() {
        return priceRepository.findOneByType(PriceType.FIXING).getPrice();
    }

    @Override
    public int getForceAddingToFavoritesPerCustomerPrice() {
        return priceRepository.findOneByType(PriceType.TO_POCKETS).getPrice();
    }
}
