package ru.cproject.karman.component;

import java.time.LocalDateTime;

/**
 * Created by ioann on 18.07.16
 */
public interface PriceCalculator {

    /**
     * Позволяет вычислить стоимость размещения акции (в рублях) с платным цветом фона до указанной даты
     * @param dateTo дата окончания публикации
     * @return
     */
    int calcPriceForBackgroundColor(LocalDateTime dateTo);


    /**
     * Позволяет вычислить стоимость поднятия акции (в рублях)
     * @return
     */
    int calcPriceForRaising();

    /**
     * Позволяет вычислить стоимость закрепления акции (в рублях)
     * @return
     */
    int calcPriceForFixing();

    /**
     * Позволяет вычислить стоимость принудительного добавления акции (в рублях)
     * в карманы указанному числу пользователей
     * @param countOfAdditions число принудительных добавлений
     * @return
     */
    int calcPriceForForceAddingToFavorites(int countOfAdditions);

    int getBackgroundColorPerDayPrice();
    int getRaisingPrice();
    int getFixingPrice();
    int getForceAddingToFavoritesPerCustomerPrice();
}
