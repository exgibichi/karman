package ru.cproject.karman.exception;

/**
 * Created by ioann on 27.05.16
 */
public class SmsException extends RuntimeException {

    public SmsException(String message) {
        super(message);
    }

    public SmsException(String message, Throwable cause) {
        super(message, cause);
    }

}
