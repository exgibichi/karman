package ru.cproject.karman.exception;

import ru.cproject.karman.exception.base.CustomException;

/**
 * Created by ioann on 25.05.16
 */
public class FieldValidationException extends RuntimeException
    implements CustomException {

    public FieldValidationException(String message) {
        super(message);
    }

}
