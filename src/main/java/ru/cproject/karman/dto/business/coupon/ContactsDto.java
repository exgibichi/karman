package ru.cproject.karman.dto.business.coupon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 10.07.16
 */
public class ContactsDto {

    public List<String> phoneNumbers = new ArrayList<>();
    public List<ContactDto> addresses = new ArrayList<>();

}
