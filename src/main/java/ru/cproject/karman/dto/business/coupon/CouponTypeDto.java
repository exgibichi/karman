package ru.cproject.karman.dto.business.coupon;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.enums.CouponType;

import java.util.List;

/**
 * Created by ioann on 04.07.16
 */
public class CouponTypeDto {

    public CouponType type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer discount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer price;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String giftTitle;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String giftImage;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<String> giftImages;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public  String giftCondition;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String giftDescription;


}
