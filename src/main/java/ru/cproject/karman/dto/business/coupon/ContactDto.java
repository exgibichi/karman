package ru.cproject.karman.dto.business.coupon;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 15.06.16
 */
public class ContactDto implements CustomDto {

    public String address;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String phoneNumber;

    public String latitude;
    public String longitude;

}
