package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 05.06.16
 */
public class CouponRateDto implements CustomDto {

    public Integer count;
    public Integer cost;
    public String description;

}
