package ru.cproject.karman.dto.business.coupon;

/**
 * Created by ioann on 03.06.16
 */
public class ModerateRejectionDto {

    public Long id;
    public String rejectionReason;

}
