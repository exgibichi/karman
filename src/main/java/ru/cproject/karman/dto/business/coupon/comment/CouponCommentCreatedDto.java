package ru.cproject.karman.dto.business.coupon.comment;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 05.07.16
 */
public class CouponCommentCreatedDto implements CustomDto {

    public Long created;
}
