package ru.cproject.karman.dto.business.coupon.comment;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 05.07.16
 */
public class CouponCommentPageDto implements CustomDto {

    public List<CouponCommentDto> comments = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean canComment;

}
