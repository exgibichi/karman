package ru.cproject.karman.dto.business;

import ru.cproject.karman.enums.PriceType;

/**
 * Created by ioann on 25.07.16
 */
public class IncomePriceDto {

    public PriceType type;
    public Integer price;

}
