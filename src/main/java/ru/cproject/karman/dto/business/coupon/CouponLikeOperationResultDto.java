package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 24.06.16
 */
public class CouponLikeOperationResultDto implements CustomDto {

    public boolean success;
    public Long likesCount;

}
