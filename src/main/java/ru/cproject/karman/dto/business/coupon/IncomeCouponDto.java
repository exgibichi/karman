package ru.cproject.karman.dto.business.coupon;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ioann on 02.06.16
 */
public class IncomeCouponDto {

    public Long id;
    public Long regionId;
    public Long categoryId;
    public String title;
    public List<String> images = new ArrayList<>();
    public Integer discount;
    public Integer price;
    public String giftTitle;
    public String giftImage;
    public String giftCondition;
    public String giftDescription;
    public String description;
    public String backgroundColorHex;
    public String flagColorHex;
    public LocalDateTime dateTo;
    public Set<ContactDto> contacts = new LinkedHashSet<>();

}
