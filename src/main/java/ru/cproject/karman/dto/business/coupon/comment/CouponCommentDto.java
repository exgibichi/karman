package ru.cproject.karman.dto.business.coupon.comment;

/**
 * Created by ioann on 05.07.16
 */
public class CouponCommentDto {

    public Long couponId;
    public Long authorId;
    public String authorName;
    public String text;
    public Long created;

}
