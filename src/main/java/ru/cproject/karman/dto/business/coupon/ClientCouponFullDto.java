package ru.cproject.karman.dto.business.coupon;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.RegionDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;
import ru.cproject.karman.enums.CouponStatus;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ioann on 15.06.16
 */
public class ClientCouponFullDto implements CustomDto {

    public Long id;
    public Long companyId;
    public String companyName;
    public RegionDto region;
    public FullCategoryDto category;
    public String title;
    public List<String> images = new ArrayList<>();
    public CouponTypeDto couponType;
    public String description;
    public Long created;
    public Long dateTo;
    public Long fid;
    public CouponStatus status;
    public String backgroundColorHex;
    public String flagColorHex;
    public String rejectionReason;
    public ContactsDto contacts;
    public boolean hasShadow;
    public boolean is_new;
    public Long likesCount;
    public Long commentsCount;
    public Long sharedCount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean canLike;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean isFavorite;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean canSubscribe;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean canComment;

}
