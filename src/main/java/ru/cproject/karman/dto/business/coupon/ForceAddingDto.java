package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.common.IdDto;

/**
 * Created by ioann on 23.07.16
 */
public class ForceAddingDto extends IdDto {

    public int count;
    public Long region_id;
}
