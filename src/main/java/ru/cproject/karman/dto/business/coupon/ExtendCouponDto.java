package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.common.IdDto;

import java.time.LocalDateTime;

/**
 * Created by ioann on 08.07.16
 */
public class ExtendCouponDto extends IdDto {

    public LocalDateTime dateTo;

}
