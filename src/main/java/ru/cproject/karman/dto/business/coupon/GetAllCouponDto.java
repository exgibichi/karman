package ru.cproject.karman.dto.business.coupon;

/**
 * Created by ioann on 15.06.16
 */
public class GetAllCouponDto {

    public Long companyId;
    public Long regionId;
    public Long categoryId;
    public int limit;
    public int skip;

}
