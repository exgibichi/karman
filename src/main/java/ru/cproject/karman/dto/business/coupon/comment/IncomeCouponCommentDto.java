package ru.cproject.karman.dto.business.coupon.comment;

/**
 * Created by ioann on 05.07.16
 */
public class IncomeCouponCommentDto {

    public Long couponId;
    public String text;

}
