package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.RegionDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;
import ru.cproject.karman.dto.statistics.ChartDto;
import ru.cproject.karman.enums.CouponStatus;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ioann on 03.06.16
 */
public class OutcomeCouponFullDto implements CustomDto {

    public Long id;
    public String companyName;
    public RegionDto region;
    public FullCategoryDto category;
    public String title;
    public List<String> images;
    public CouponTypeDto couponType;
    public String description;
    public Long created;
    public Long fixingDate;
    public Long dateTo;
    public CouponStatus status;
    public String backgroundColorHex;
    public boolean isBackgroundColorHexPayed;
    public String flagColorHex;
    public String rejectionReason;
    public Set<ContactDto> contacts = new LinkedHashSet<>();
    public boolean hasShadow;
    public Long likesCount;
    public Long commentsCount;
    public Long sharedCount;
    public Long reviewsCount;
    public List<ChartDto> chartsData = new ArrayList<>();

}
