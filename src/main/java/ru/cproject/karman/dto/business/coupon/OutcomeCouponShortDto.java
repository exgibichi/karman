package ru.cproject.karman.dto.business.coupon;

import ru.cproject.karman.dto.base.CustomDto;

import java.time.LocalDateTime;

/**
 * Created by ioann on 03.06.16
 */
public class OutcomeCouponShortDto implements CustomDto {

    public Long id;
    public String companyName;
    public String title;
    public String description;
    public LocalDateTime created;

}
