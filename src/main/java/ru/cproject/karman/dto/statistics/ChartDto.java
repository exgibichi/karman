package ru.cproject.karman.dto.statistics;

import ru.cproject.karman.domain.statistics.LocalDateTimeAndCountStats;
import ru.cproject.karman.dto.base.CustomDto;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 11.07.16
 */
public class ChartDto implements CustomDto {
    public List<Integer> yCoords = new ArrayList<>();
    public List<String> xLabels = new ArrayList<>();
}
