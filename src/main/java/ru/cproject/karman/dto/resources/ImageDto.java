package ru.cproject.karman.dto.resources;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 30.05.16
 */
public class ImageDto implements CustomDto {

    public String fileName;

}
