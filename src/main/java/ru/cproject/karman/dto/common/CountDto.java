package ru.cproject.karman.dto.common;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 10.07.16
 */
public class CountDto implements CustomDto {

    public int count;

    public CountDto() { }

    public CountDto(int count) {
        this.count = count;
    }
}
