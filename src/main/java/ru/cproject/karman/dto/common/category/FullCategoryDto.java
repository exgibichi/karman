package ru.cproject.karman.dto.common.category;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 30.05.16
 */
public class FullCategoryDto implements CustomDto {

    public Long id;
    public String title;

}
