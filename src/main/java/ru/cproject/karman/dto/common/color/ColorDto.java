package ru.cproject.karman.dto.common.color;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 08.07.16
 */
public class ColorDto implements CustomDto {

    public Long id;
    public String colorHex;

}
