package ru.cproject.karman.dto.common;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 02.06.16
 */
public class IncomeRegionDto implements CustomDto {
    public Long parentId;
    public String title;
}
