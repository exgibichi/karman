package ru.cproject.karman.dto.common;

import java.time.LocalDateTime;

/**
 * Created by ioann on 23.07.16
 */
public class LocalDateTimeDto {
    public LocalDateTime date;
}
