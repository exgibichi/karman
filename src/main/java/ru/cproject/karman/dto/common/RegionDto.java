package ru.cproject.karman.dto.common;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 02.06.16
 */
public class RegionDto implements CustomDto {

    public Long id;
    public Long parentId;
    public String title;
    public String parent_title;

}
