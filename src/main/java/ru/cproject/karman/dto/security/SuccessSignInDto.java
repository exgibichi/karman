package ru.cproject.karman.dto.security;

import ru.cproject.karman.conf.security.RoleEnum;
import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 26.05.16.
 */
public class SuccessSignInDto implements CustomDto {

    public String authToken;
    public String phoneNumber;
    public String name;
    public RoleEnum role;

}
