package ru.cproject.karman.dto.payment;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 23.07.16
 */
public class PricesDto implements CustomDto {

    public int backgroundColorPerDayPrice;
    public int raisingPrice;
    public int fixingPrice;
    public int forceAddingToFavoritesPerCustomerPrice;

}
