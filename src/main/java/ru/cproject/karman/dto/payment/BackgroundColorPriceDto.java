package ru.cproject.karman.dto.payment;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 23.07.16
 */
public class BackgroundColorPriceDto implements CustomDto {
    public int price;
    public int days;
}
