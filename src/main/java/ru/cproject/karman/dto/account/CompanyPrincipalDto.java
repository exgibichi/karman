package ru.cproject.karman.dto.account;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ioann on 05.06.16
 */
public class CompanyPrincipalDto extends PrincipalDto {

    public int balance;
    public String logo;
    public boolean isActive;
    public String moderatorsComment;
    public Boolean inModeration;

}
