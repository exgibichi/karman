package ru.cproject.karman.dto.account;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;

/**
 * Created by ioann on 06.06.16
 */
public class CompanyDto implements CustomDto {

    public Long id;
    public String name;
    public String description;
    public FullCategoryDto category;
    public String siteUrl;
    public String address;
    public String longitude;
    public String latitude;
    public String contactPhoneNumber;
    public String email;
    public String fullName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean inModeration;
}
