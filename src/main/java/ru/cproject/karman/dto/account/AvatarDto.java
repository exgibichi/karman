package ru.cproject.karman.dto.account;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 07.06.16
 */
public class AvatarDto implements CustomDto {

    public String avatar;

}
