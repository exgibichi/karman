package ru.cproject.karman.dto.account;

import ru.cproject.karman.conf.security.RoleEnum;

/**
 * Created by ioann on 26.05.16.
 */
public class RegistrationDto {

    public String name;
    public String phoneNumber;
    public RoleEnum role;
    public String email;
    public String fullName;
    public Long region_id;
}
