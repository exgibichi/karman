package ru.cproject.karman.dto.account;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;
import ru.cproject.karman.dto.business.coupon.ContactsDto;
import ru.cproject.karman.dto.common.category.FullCategoryDto;

/**
 * Created by ioann on 16.06.16
 */
public class CompanyClientDto implements CustomDto {

    public Long id;
    public String title;
    public String image;
    public String description;
    public String siteUrl;
    public FullCategoryDto category;
    public ContactsDto contacts;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Boolean canSubscribe;

}
