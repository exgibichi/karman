package ru.cproject.karman.dto.account;

/**
 * Created by ioann on 01.07.16
 */
public class CompanyModerationDto {

    public Long id;
    public String title;
    public String phoneNumber;
    public String fullName;
    public String email;

}
