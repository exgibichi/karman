package ru.cproject.karman.dto.account;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by admin on 02.02.2017.
 */
public class TokenDto implements CustomDto {
   public String token;
}
