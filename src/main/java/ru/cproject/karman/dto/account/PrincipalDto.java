package ru.cproject.karman.dto.account;

import ru.cproject.karman.conf.security.RoleEnum;
import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 31.05.16
 */
public class PrincipalDto implements CustomDto {

    public String phoneNumber;
    public String name;
    public RoleEnum role;
    public Long region_id;

}
