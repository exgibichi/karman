package ru.cproject.karman.dto.status;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 26.05.16.
 */
public class ErrorDto implements CustomDto {
    public final StatusType status = StatusType.ERROR;
    public String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer code;
}
