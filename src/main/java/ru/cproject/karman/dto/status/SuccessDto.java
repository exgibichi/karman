package ru.cproject.karman.dto.status;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 26.05.16.
 */
public class SuccessDto implements CustomDto {

    public final StatusType status = StatusType.OK;

}
