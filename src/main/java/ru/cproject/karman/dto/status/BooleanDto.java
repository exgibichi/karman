package ru.cproject.karman.dto.status;

import ru.cproject.karman.dto.base.CustomDto;

/**
 * Created by ioann on 16.06.16
 */
public class BooleanDto implements CustomDto {

    public boolean success;

}
