package ru.cproject.karman.dto.status;

import ru.cproject.karman.enums.ErrorCode;

/**
 * Created by ioann on 26.05.16.
 */
public class StatusDtoFactory {

    private StatusDtoFactory() {
        throw  new AssertionError();
    }

    public static ErrorDto buildError(String message) {
        return buildError(message, null);
    }

    public static ErrorDto buildError(String message, ErrorCode errorCode) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.message = message;

        if (errorCode != null) {
            errorDto.code = errorCode.getCode();
        }

        return errorDto;
    }

    public static SuccessDto buildSuccess() {
        return new SuccessDto();
    }


    public static BooleanDto buildBoolean(boolean b) {
        BooleanDto dto = new BooleanDto();
        dto.success = b;
        return dto;
    }

}
