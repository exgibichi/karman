package ru.cproject.karman.dto.status;

/**
 * Created by ioann on 26.05.16.
 */
public enum StatusType {
    OK,
    ERROR
}
