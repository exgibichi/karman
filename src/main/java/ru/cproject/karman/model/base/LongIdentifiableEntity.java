package ru.cproject.karman.model.base;

import org.hibernate.Hibernate;

import javax.persistence.*;

/**
 * Created by ioann on 24.05.16.
 *
 * Базовый класс для всех JPA сущностей
 */
@MappedSuperclass
public class LongIdentifiableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return ((getId() == null) ? super.hashCode() : getId().hashCode());
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof LongIdentifiableEntity)) {
            return false;
        }

        if (obj == null || Hibernate.getClass(getClass()) != Hibernate.getClass(obj.getClass())) {
            return false;
        }

        LongIdentifiableEntity other = (LongIdentifiableEntity) obj;
        return getId() != null && other.getId() != null && getId().equals(other.getId());
    }

}
