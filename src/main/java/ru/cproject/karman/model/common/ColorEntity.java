package ru.cproject.karman.model.common;

import ru.cproject.karman.domain.common.Color;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by ioann on 08.07.16
 */
@Entity
@Table(name = "colors")
public class ColorEntity extends LongIdentifiableEntity {

    @Column(name = "color_hex", length = 7, unique = true)
    private String colorHex;

    public ColorEntity() { }

    public ColorEntity(Color color) {
        this.colorHex = color.getColorHex();
    }

    public String getColorHex() {
        return colorHex;
    }

    public void setColorHex(String colorHex) {
        this.colorHex = colorHex;
    }


    public Color toDomain() {
        Color color = new Color();

        color.setId(getId());
        color.setColorHex(getColorHex());

        return color;
    }
}
