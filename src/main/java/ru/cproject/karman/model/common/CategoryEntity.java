package ru.cproject.karman.model.common;

import ru.cproject.karman.domain.common.Category;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by ioann on 30.05.16
 */
@Entity
@Table(name = "categories")
public class CategoryEntity extends LongIdentifiableEntity {

    @Column(name = "title", nullable = false, unique = true)
    private String title;


    public CategoryEntity() { }

    public CategoryEntity(Category category) {
        setId(category.getId());
        title = category.getTitle().trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Category toDomain() {
        Category category = new Category();

        category.setId(getId());
        category.setTitle(getTitle());

        return category;
    }
}
