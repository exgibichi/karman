package ru.cproject.karman.model.common;

import ru.cproject.karman.domain.common.Region;
import ru.cproject.karman.model.base.LongIdentifiableEntity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ioann on 31.05.16
 */
@Entity
@Table(name = "regions",
    uniqueConstraints = {
        @UniqueConstraint(name = "region_city_constraint",
            columnNames = {"title", "parent_id"})
    },
    indexes = { @Index(name = "title_index", columnList = "title") }
)
public class RegionEntity extends LongIdentifiableEntity {

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private RegionEntity parent;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private List<RegionEntity> childs;

    public String getTitle() {
        return title;
    }

    public List<RegionEntity> getChilds() {
        return childs;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RegionEntity getParent() {
        return parent;
    }

    public void setParent(RegionEntity parent) {
        this.parent = parent;
    }


    public List<Long> getChildsId() {
        List<Long> ids = new ArrayList<Long>();
        List<RegionEntity> childsList = getChilds();
        for (int i = 0; i < childsList.size(); i++) {
            ids.add(childsList.get(i).getId());
        }
        return ids;
    }

    public Region toDomain() {
        Region region = new Region();

        region.setId(getId());
        region.setTitle(getTitle());

        if (getParent() != null) {
            region.setParentTitle(getParent().getTitle());
            region.setParent(getParent().toDomain());
        }

        return region;
    }

}
