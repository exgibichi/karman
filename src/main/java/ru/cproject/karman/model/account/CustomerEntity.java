package ru.cproject.karman.model.account;

import ru.cproject.karman.domain.account.Customer;
import ru.cproject.karman.model.base.LongIdentifiableEntity;
import ru.cproject.karman.model.business.CouponCommentEntity;
import ru.cproject.karman.model.business.CouponEntity;
import ru.cproject.karman.model.business.FavoriteCouponEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by ioann on 25.05.16.
 */
@Entity
@Table(name = "customers")
public class CustomerEntity extends LongIdentifiableEntity {

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "customer")
    private UserEntity user;


    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<FavoriteCouponEntity> favoriteCoupons;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
        name="customers_liked_coupons",
        joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false),
        inverseJoinColumns = @JoinColumn(name = "coupon_id", referencedColumnName = "id", nullable = false),
        uniqueConstraints = @UniqueConstraint(
            columnNames = {"customer_id", "coupon_id"},
            name = "customer_id-coupon_id")
    )
    private Set<CouponEntity> likedCoupons;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
        name = "customers_subscriptions_to_companies",
        joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false),
        inverseJoinColumns = @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false),
        uniqueConstraints = @UniqueConstraint(
            columnNames = {"customer_id", "company_id"},
            name = "customer_id-company_id"
        )
    )
    private Set<CompanyEntity> subscriptions;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
        name = "customers_watches_to_coupons",
        joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false),
        inverseJoinColumns = @JoinColumn(name = "coupon_id", referencedColumnName = "id", nullable = false),
        uniqueConstraints = @UniqueConstraint(
            columnNames = {"customer_id", "coupon_id"},
            name = "customer_id-coupon_id"
        )
    )
    private Set<CouponEntity> watchedCoupons;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<CouponCommentEntity> couponComments;


    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Set<FavoriteCouponEntity> getFavoriteCoupons() {
        return favoriteCoupons;
    }

    public void setFavoriteCoupons(Set<FavoriteCouponEntity> favoriteCoupons) {
        this.favoriteCoupons = favoriteCoupons;
    }

    public Set<CouponEntity> getLikedCoupons() {
        return likedCoupons;
    }

    public void setLikedCoupons(Set<CouponEntity> likedCoupons) {
        this.likedCoupons = likedCoupons;
    }

    public Set<CompanyEntity> getSubscriptions() {
        return subscriptions;
    }

    public Set<CouponEntity> getWatchedCoupons() {
        return watchedCoupons;
    }

    public void setWatchedCoupons(Set<CouponEntity> watches) {
        this.watchedCoupons = watches;
    }

    public void setSubscriptions(Set<CompanyEntity> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<CouponCommentEntity> getCouponComments() {
        return couponComments;
    }

    public Customer toDomain() {
        Customer customer = new Customer();
        customer.setId(getId());
        customer.setUser(user.toIncludedDomain());
        return customer;
    }
}
