package ru.cproject.karman.model.account;

import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by ioann on 25.05.16
 */
@Entity
@Table(name = "users")
public class UserEntity extends LongIdentifiableEntity {

    @Column(name="name", length = 80, nullable = false)
    private String name;

    @Column(name="phone_number", length = 16, unique = true, nullable = false)
    private String phoneNumber;

    @Column(name="password_hash", length = 60, nullable = false)
    private String passwordHash;

    @Column(name="region_id")
    private Long region_id;

    @Column(name="token")
    private String token;

    @Column(name = "registration_date")
    private LocalDateTime registrationDate;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="customer_id" ,referencedColumnName = "id", unique = true)
    private CustomerEntity customer;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="company_id", referencedColumnName = "id", unique = true)
    private CompanyEntity company;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRegion() {
        return region_id;
    }

    public void setRegion(Long region_id) {
        this.region_id = region_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }


    public User toDomain() {
        User user = new User();

        user.setId(getId());
        user.setName(getName());
        user.setPhoneNumber(getPhoneNumber());
        user.setToken(getToken());
        user.setRegion(getRegion());
        user.setPasswordHash(getPasswordHash());
        user.setCreated(getRegistrationDate());

        if (customer != null) {
            user.setCustomer(getCustomer().toDomain());
        } else if (company != null) {
            user.setCompany(getCompany().toIncludedDomain());
        }

        return user;
    }


    public User toIncludedDomain() {
        User user = new User();

        user.setId(getId());
        user.setName(getName());
        user.setRegion(getRegion());
        user.setPhoneNumber(phoneNumber);

        return user;
    }
}
