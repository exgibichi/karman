package ru.cproject.karman.model.account;

import org.hibernate.annotations.Check;
import ru.cproject.karman.domain.account.Company;
import ru.cproject.karman.model.base.LongIdentifiableEntity;
import ru.cproject.karman.model.common.CategoryEntity;

import javax.persistence.*;
import javax.validation.constraints.Min;

/**
 * Created by ioann on 25.05.16.
 */
@Entity
@Table(name = "companies")
public class CompanyEntity extends LongIdentifiableEntity {

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "company")
    private UserEntity user;

    @Column(name = "email", length = 260, nullable = false)
    private String email;

    @Column(name = "full_name", length = 100, nullable = false)
    private String fullName;

    @Column(name = "description", length = 1500)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    @Column(name = "logo")
    private String logo;

    @Min(value = 0)
    @Column(name = "balance", nullable = false)
    private int balance;

    @Column(name = "site_url", length = 300)
    private String siteUrl;

    @Column(name = "address", length = 300)
    private String address;

    @Column(name = "longitude", length = 50)
    private String longitude;
    @Column(name = "latitude", length = 50)
    private String latitude;

    @Column(name = "contact_phone_number", length = 30)
    private String contactPhoneNumber;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "moderators_comment")
    private String moderatorsComment;

    @Column(name = "new_name", length = 80)
    private String newName;

    @Column(name = "new_email", length = 260)
    private String newEmail;

    @Column(name = "new_full_name", length = 100)
    private String newFullName;


    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getModeratorsComment() {
        return moderatorsComment;
    }

    public void setModeratorsComment(String moderatorsComment) {
        this.moderatorsComment = moderatorsComment;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getNewFullName() {
        return newFullName;
    }

    public void setNewFullName(String newFullName) {
        this.newFullName = newFullName;
    }

    public Company toIncludedDomain() {
        Company company = new Company();

        company.setId(getId());
        company.setEmail(getEmail());
        company.setFullName(getFullName());
        company.setDescription(getDescription());

        if (getCategory() != null) {
            company.setCategory(getCategory().toDomain());
        }

        company.setLogo(getLogo());
        company.setBalance(getBalance());

        company.setActive(isActive());
        company.setModeratorsComment(getModeratorsComment());
        company.setNewName(getNewName());
        company.setNewEmail(getNewEmail());
        company.setNewFullName(getNewFullName());

        return company;
    }

    public Company toDomain() {
        Company company = toIncludedDomain();
        company.setUser(user.toIncludedDomain());
        company.setDescription(getDescription());

        if (getCategory() != null) {
            company.setCategory(getCategory().toDomain());
        }

        company.setLogo(getLogo());
        company.setBalance(getBalance());
        company.setSiteUrl(getSiteUrl());
        company.setAddress(getAddress());
        company.setLongitude(getLongitude());
        company.setLatitude(getLatitude());
        company.setContactPhoneNumber(getContactPhoneNumber());

        return company;
    }

}
