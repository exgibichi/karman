package ru.cproject.karman.model.business;

import ru.cproject.karman.enums.PriceType;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;

/**
 * Created by ioann on 25.07.16
 */
@Entity
@Table(name = "prices")
public class PriceEntity extends LongIdentifiableEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private PriceType type;

    @Column(name = "price", nullable = false)
    private Integer price;


    public PriceType getType() {
        return type;
    }

    public void setType(PriceType type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
