package ru.cproject.karman.model.business;

import ru.cproject.karman.domain.business.Coupon;
import ru.cproject.karman.enums.CouponStatus;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.base.LongIdentifiableEntity;
import ru.cproject.karman.model.common.CategoryEntity;
import ru.cproject.karman.model.common.RegionEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by ioann on 01.06.16
 * <p>
 * Сущность, олицетворяющая акцию в бд
 */
@Entity
@Table(name = "coupons")
public class CouponEntity extends LongIdentifiableEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", nullable = false)
    private CompanyEntity company;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id")
    private RegionEntity region;

    @Column(name = "title", length = 100, nullable = false)
    private String title;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "coupon_images",
        joinColumns = @JoinColumn(name = "coupon_id", referencedColumnName = "id"))
    @Column(name = "image")
    private List<String> images = new ArrayList<>();

    //Размер скидки
    @Column(name = "discount")
    private Integer discount;

    //Ценник
    @Column(name = "price")
    private Integer price;

    @Column(name = "gift_title", length = 100)
    private String giftTitle;

    @Column(name = "gift_image")
    private String giftImage;

    @Column(name = "gift_description", length = 1000)
    private String giftDescription;

    @Column(name = "gift_condition", length = 1000)
    private String giftCondition;

    @Column(name = "description", length = 1000, nullable = false)
    private String description;

    @Column(name = "created", nullable = false)
    private LocalDateTime created;

    /* Дата поднятия акции в списке (используется для сортировки).
       По умолчанию равна дате создания
      */
    @Column(name = "raising_date", nullable = false)
    private LocalDateTime raisingDate;

    @Column(name = "date_to")
    private LocalDateTime dateTo;

    @Column(name = "fixing_date")
    private LocalDateTime fixingDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private CouponStatus status;

    @Column(name = "background_color_hex", length = 7)
    private String backgroundColorHex;

    @Column(name ="is_background_color_hex_payed", nullable = false)
    private boolean isBackgroundColorHexPayed;

    @Column(name = "flag_color_hex", length = 7)
    private String flagColorHex;

    @Column(name = "rejection_reason", length = 300)
    private String rejectionReason;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "coupon", orphanRemoval = true)
    private Set<CouponContactEntity> contacts = new LinkedHashSet<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private CouponEntity parent;

    @OneToOne(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private CouponEntity child;

    @Column(name = "likes_count", nullable = false)
    private Long likesCount;

    @Column(name = "comments_count", nullable = false)
    private Long commentsCount;

    @Column(name = "shared_count", nullable = false)
    private Long sharedCount;

    @OneToMany(mappedBy = "coupon", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<CouponCommentEntity> couponComments;

    @OneToMany(mappedBy = "coupon", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<CouponReviewEntity> couponReviews;

    @OneToMany(mappedBy = "coupon", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<FavoriteCouponEntity> favorites;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "likedCoupons", cascade = CascadeType.REMOVE)
    private Set<CustomerEntity> likers;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "watchedCoupons", cascade = CascadeType.REMOVE)
    private Set<CustomerEntity> watchers;

    public String getGiftImage() {
        return giftImage;
    }

    public void setGiftImage(String giftImage) {
        this.giftImage = giftImage;
    }

    public String getGiftTitle() {
        return giftTitle;
    }

    public void setGiftTitle(String giftTitle) {
        this.giftTitle = giftTitle;
    }

    public String getGiftCondition() {
        return giftCondition;
    }

    public void setGiftCondition(String giftCondition) {
        this.giftCondition = giftCondition;
    }

    public String getGiftDescription() {
        return giftDescription;
    }

    public void setGiftDescription(String giftDescription) {
        this.giftDescription = giftDescription;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public RegionEntity getRegion() {
        return region;
    }

    public void setRegion(RegionEntity region) {
        this.region = region;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public CouponStatus getStatus() {
        return status;
    }

    public void setStatus(CouponStatus status) {
        this.status = status;
    }

    public String getBackgroundColorHex() {
        return backgroundColorHex;
    }

    public void setBackgroundColorHex(String backgroundColorHex) {
        this.backgroundColorHex = backgroundColorHex;
    }

    public String getFlagColorHex() {
        return flagColorHex;
    }

    public void setFlagColorHex(String flagColorHex) {
        this.flagColorHex = flagColorHex;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }

    public Set<CouponContactEntity> getContacts() {
        return contacts;
    }

    public void setContacts(Set<CouponContactEntity> contacts) {
        this.contacts = contacts;
    }

    public CouponEntity getParent() {
        return parent;
    }

    public void setParent(CouponEntity parent) {
        this.parent = parent;
    }

    public CouponEntity getChild() {
        return child;
    }

    public void setChild(CouponEntity child) {
        this.child = child;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    public Long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Long commentsCount) {
        this.commentsCount = commentsCount;
    }

    public LocalDateTime getRaisingDate() {
        return raisingDate;
    }

    public void setRaisingDate(LocalDateTime raisingDate) {
        this.raisingDate = raisingDate;
    }

    public LocalDateTime getFixingDate() {
        return fixingDate;
    }

    public void setFixingDate(LocalDateTime fixingDate) {
        this.fixingDate = fixingDate;
    }

    public Long getSharedCount() {
        return sharedCount;
    }

    public void setSharedCount(Long sharedCount) {
        this.sharedCount = sharedCount;
    }

    public boolean isBackgroundColorHexPayed() {
        return isBackgroundColorHexPayed;
    }

    public void setBackgroundColorHexPayed(boolean backgroundColorHexPayed) {
        isBackgroundColorHexPayed = backgroundColorHexPayed;
    }

    public List<CouponCommentEntity> getCouponComments() {
        return couponComments;
    }

    public List<CouponReviewEntity> getCouponReviews() {
        return couponReviews;
    }

    public List<FavoriteCouponEntity> getFavorites() {
        return favorites;
    }

    public Coupon toDomain() {
        Coupon coupon = toShortDomain();

        if (getParent() != null) {
            coupon.setParent(getParent().toShortDomain());
        }

        if (getChild() != null) {
            coupon.setChild(getChild().toShortDomain());
        }

        return coupon;
    }

    /**
     * Возвращает domain объект без циклических ссылок
     *
     * @return
     */
    public Coupon toShortDomain() {
        Coupon coupon = new Coupon();

        coupon.setId(getId());
        coupon.setCompany(getCompany().toDomain());

        if (region != null) {
            coupon.setRegion(getRegion().toDomain());
        }

        if (category != null) {
            coupon.setCategory(getCategory().toDomain());
        }

        coupon.setTitle(getTitle());
        coupon.setImages(getImages());
        coupon.setDiscount(getDiscount());
        coupon.setPrice(getPrice());
        coupon.setGiftTitle(getGiftTitle());
        coupon.setGiftImage(getGiftImage());
        coupon.setGiftCondition(getGiftCondition());
        coupon.setGiftDescription(getGiftDescription());
        coupon.setDescription(getDescription());
        coupon.setCreated(getCreated());
        coupon.setDateTo(getDateTo());
        coupon.setFixingDate(getFixingDate());
        coupon.setStatus(getStatus());
        coupon.setBackgroundColorHex(getBackgroundColorHex());
        coupon.setBackgroundColorHexPayed(isBackgroundColorHexPayed());
        coupon.setFlagColorHex(getFlagColorHex());
        coupon.setRejectionReason(getRejectionReason());

        getContacts().forEach(
            c -> coupon.getContacts().add(c.toDomain())
        );

        coupon.setLikesCount(getLikesCount());
        coupon.setCommentsCount(getCommentsCount());
        coupon.setSharedCount(getSharedCount());

        return coupon;
    }
}
