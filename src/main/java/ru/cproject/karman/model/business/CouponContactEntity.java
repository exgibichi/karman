package ru.cproject.karman.model.business;

import ru.cproject.karman.domain.business.CouponContact;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;

/**
 * Created by ioann on 07.07.16
 */
@Entity
@Table(name = "coupon_contacts")
public class CouponContactEntity extends LongIdentifiableEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coupon_id", nullable = false, referencedColumnName = "id")
    private CouponEntity coupon;

    @Column(name = "address", nullable = false, length = 500)
    private String address;

    @Column(name = "phone_number", nullable = false, length = 100)
    private String phoneNumber;

    //широта
    @Column(name = "latitude", length = 50)
    private String latitude;

    //долгота
    @Column(name = "longitude", length = 50)
    private String longitude;


    public CouponContactEntity() { }


    public CouponContactEntity(CouponContact contact) {
        this.address = contact.getAddress();
        this.phoneNumber = contact.getPhoneNumber();
    }


    public CouponEntity getCoupon() {
        return coupon;
    }

    public void setCoupon(CouponEntity coupon) {
        this.coupon = coupon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public CouponContact toDomain() {
        CouponContact couponContact = new CouponContact();

        couponContact.setId(getId());
        couponContact.setAddress(getAddress());
        couponContact.setPhoneNumber(getPhoneNumber());
        couponContact.setLatitude(getLatitude());
        couponContact.setLongitude(getLongitude());

        return couponContact;
    }

}
