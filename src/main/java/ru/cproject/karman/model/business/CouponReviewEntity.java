package ru.cproject.karman.model.business;

import ru.cproject.karman.domain.business.CouponReview;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by ioann on 10.07.16
 */
@Entity
@Table(name = "coupon_reviews")
public class CouponReviewEntity extends LongIdentifiableEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coupon_id", nullable = false, referencedColumnName = "id")
    private CouponEntity coupon;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;


    public CouponEntity getCoupon() {
        return coupon;
    }

    public void setCoupon(CouponEntity coupon) {
        this.coupon = coupon;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }



    public CouponReview toDomain() {
        CouponReview couponReview = toShortDomain();
        couponReview.setCoupon(getCoupon().toDomain());

        return couponReview;
    }


    public CouponReview toShortDomain() {
        CouponReview couponReview = new CouponReview();

        couponReview.setToken(getToken());
        couponReview.setDate(getDate());

        return couponReview;
    }
}
