package ru.cproject.karman.model.business;

import ru.cproject.karman.domain.business.CouponComment;
import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by ioann on 04.07.16
 */
@Entity
@Table(name = "coupon_comments",
    uniqueConstraints = @UniqueConstraint(columnNames = {"coupon_id", "customer_id"})
)
public class CouponCommentEntity extends LongIdentifiableEntity {

    @Column(name = "text", length = 1000)
    private String text;

    @JoinColumn(name = "coupon_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CouponEntity coupon;

    @JoinColumn(name = "customer_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerEntity customer;

    @Column(name = "created", nullable = false)
    private LocalDateTime created;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CouponEntity getCoupon() {
        return coupon;
    }

    public void setCoupon(CouponEntity coupon) {
        this.coupon = coupon;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }


    public CouponComment toDomain() {
        CouponComment comment = new CouponComment();

        comment.setId(getId());
        comment.setText(getText());
        comment.setCoupon(getCoupon().toDomain());
        comment.setCustomer(getCustomer().toDomain());
        comment.setCreated(getCreated());

        return comment;
    }
}
