package ru.cproject.karman.model.business;

import ru.cproject.karman.domain.business.PaymentHistory;
import ru.cproject.karman.enums.PaymentStatus;
import ru.cproject.karman.model.account.CompanyEntity;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by ioann on 19.06.16
 */
@Entity
@Table(name = "payment_histories")
public class PaymentHistoryEntity extends LongIdentifiableEntity {

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

    @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private CompanyEntity company;

    @Column(name = "amount_of_money", precision = 10, scale = 2, nullable = false)
    private BigDecimal amountOfMoney;

    @Column(name = "info")
    private String info;

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(BigDecimal amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public PaymentHistory toDomain() {
        PaymentHistory paymentHistory = new PaymentHistory();

        paymentHistory.setId(getId());
        paymentHistory.setPaymentDate(getPaymentDate());
        paymentHistory.setStatus(getStatus());
        paymentHistory.setCompany(getCompany().toIncludedDomain());
        paymentHistory.setAmountOfMoney(getAmountOfMoney());

        return paymentHistory;
    }
}
