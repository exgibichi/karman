package ru.cproject.karman.model.business;

import ru.cproject.karman.model.account.CustomerEntity;
import ru.cproject.karman.model.base.LongIdentifiableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Сущность олицетворяющая избранную акцию пользователя
 * Created by ioann on 09.07.16
 */
@Entity
@Table(
    name = "favorite_coupons",
    uniqueConstraints = @UniqueConstraint(columnNames = {"customer_id", "coupon_id"})
)
public class FavoriteCouponEntity extends LongIdentifiableEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false, referencedColumnName = "id")
    private CustomerEntity customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coupon_id", nullable = false, referencedColumnName = "id")
    private CouponEntity coupon;

    @Column(name = "created", nullable = false)
    private LocalDateTime created;

    @Column(name = "is_showed", nullable = false)
    private boolean isShowed;


    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public CouponEntity getCoupon() {
        return coupon;
    }

    public void setCoupon(CouponEntity coupon) {
        this.coupon = coupon;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public boolean isShowed() {
        return isShowed;
    }

    public void setShowed(boolean showed) {
        isShowed = showed;
    }
}
