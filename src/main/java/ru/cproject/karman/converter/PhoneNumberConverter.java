package ru.cproject.karman.converter;

/**
 * Created by ioann on 25.06.16
 */
public class PhoneNumberConverter {

    public static String convert(String phoneNumber) {

        if (phoneNumber == null || phoneNumber.isEmpty()) {
            throw new RuntimeException("Не указан номер телефона");
        }

        if (phoneNumber.length() == 12 && phoneNumber.startsWith("+7")) {
            return phoneNumber;
        }

        if (phoneNumber.length() == 10) {
            return "+7" + phoneNumber;
        } else if (phoneNumber.length() == 11 && '8' == phoneNumber.charAt(0)) {
            return "+7" + phoneNumber.substring(1);
        } else {
            throw new RuntimeException("Некорректный формат номера телефона");
        }

    }

}
