package ru.cproject.karman.conf.security.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.DisabledException;
import org.springframework.stereotype.Component;
import ru.cproject.karman.conf.security.CustomSecurityContext;
import ru.cproject.karman.conf.security.Roles;
import ru.cproject.karman.domain.account.Company;

/**
 * Created by ioann on 29.06.16
 * Аспект для проверки безопасности с использованием {@link ActiveCompany}
 */
@Aspect
@Component
@Configurable
public class ActiveCompanyAspect {

    private static final String COMPANY_IS_NOT_ACTIVE = "Учетная запись не активна";

    private CustomSecurityContext customSecurityContext;

    @Autowired
    public void setCustomSecurityContext(CustomSecurityContext customSecurityContext) {
        this.customSecurityContext = customSecurityContext;
    }


    /**
     * Перед вызовом публичного аннотированного метода
     */
    @Secured(Roles.COMPANY)
    @Before("@annotation(ActiveCompany)")
    public void doMethodCheck() {
        Company company = customSecurityContext.getCurrentPrincipal().getCompany();

        if (!company.isActive()) {
            throw new DisabledException(COMPANY_IS_NOT_ACTIVE);
        }
    }

}
