package ru.cproject.karman.conf.security;

/**
 * Created by ioann on 25.05.16.
 */
public class Roles {

    public static final String ADMIN = "ROLE_ADMIN";
    public static final String CUSTOMER = "ROLE_CUSTOMER";
    public static final String COMPANY = "ROLE_COMPANY";
    public static final String IS_AUTHENTICATED_FULLY = "IS_AUTHENTICATED_FULLY";

}
