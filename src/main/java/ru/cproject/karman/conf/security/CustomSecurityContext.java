package ru.cproject.karman.conf.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.domain.account.User;

/**
 * Created by ioann on 31.05.16
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CustomSecurityContext {

    private UserRepository userRepository;

    private User user;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    
    public User getCurrentPrincipal() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null || !auth.isAuthenticated()) {
            return null;
        }

        if (auth.getPrincipal() != null) {

            if (user == null) {
                user = userRepository
                    .findOneByPhoneNumber((String) auth.getPrincipal())
                    .toDomain();
            }

            return user;
        }

        return null;
    }

}
