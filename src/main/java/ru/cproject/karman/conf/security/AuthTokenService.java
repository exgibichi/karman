package ru.cproject.karman.conf.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by ioann on 25.05.16.
 */
@Component
public class AuthTokenService {

    @Value("${authTokenSecret}")
    private String secret;

    private UserDetailsService userDetailsService;

    @Autowired
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public String createTokenForUser(UserDetails userDetails) {
        return Jwts.builder()
            .setSubject(userDetails.getUsername())
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
    }

    public UserDetails parseUserFromToken(String token) throws UsernameNotFoundException {
        String userName = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .getBody()
            .getSubject();

        return userDetailsService.loadUserByUsername(userName);
    }
}
