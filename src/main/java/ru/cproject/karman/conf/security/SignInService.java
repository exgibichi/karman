package ru.cproject.karman.conf.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.cproject.karman.domain.account.CustomAuthentication;

/**
 * Created by ioann on 25.05.16.
 */
@Component
public class SignInService {

    private UserDetailsService userDetailsService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    public Authentication signIn(String phoneNumber, String password) {

        try {
            //Достаем юзера по номеру
            UserDetails userDetails = userDetailsService.loadUserByUsername(phoneNumber);

            //Проверяем пароль
            String passwordHash = userDetails.getPassword();

            if (password == null
                || !passwordEncoder.matches(password, passwordHash)) {
                return null;
            }

            Authentication auth = new CustomAuthentication(userDetails);
            return auth;
        } catch (UsernameNotFoundException e) {
            return null;
        }

    }

}
