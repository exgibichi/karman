package ru.cproject.karman.conf.security.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ioann on 29.06.16
 * Аннотация позволяет защитить методы, добавляя в них проверку
 * прав доступа только для активных компаний
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ActiveCompany {
}
