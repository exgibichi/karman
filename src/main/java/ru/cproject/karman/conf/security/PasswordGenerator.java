package ru.cproject.karman.conf.security;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by ioann on 27.05.16
 */
@Component
public class PasswordGenerator {

    public String generatePassword() {
        return UUID.randomUUID().toString().substring(0, 6);
    }

}
