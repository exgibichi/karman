package ru.cproject.karman.conf.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cproject.karman.dao.jpa.repositories.account.UserRepository;
import ru.cproject.karman.domain.account.CustomUserDetails;
import ru.cproject.karman.domain.account.User;
import ru.cproject.karman.model.account.UserEntity;

/**
 * Created by ioann on 25.05.16.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        final UserEntity userEntity = userRepository.findOneByPhoneNumber(s);

        if (userEntity == null) {
            throw new UsernameNotFoundException("user not found");
        }

        User user = userEntity.toDomain();
        UserDetails userDetails = new CustomUserDetails(user);
        detailsChecker.check(userDetails);

        return userDetails;
    }

}
