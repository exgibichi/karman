package ru.cproject.karman.conf.security;

/**
 * Created by ioann on 25.05.16.
 */
public enum RoleEnum {
    ADMIN,
    CUSTOMER,
    COMPANY
}
