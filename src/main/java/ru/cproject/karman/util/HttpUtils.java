package ru.cproject.karman.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ioann on 27.05.16
 */
public class HttpUtils {

    private HttpUtils() {
        throw new AssertionError();
    }


    /**
     * Позволяет получить JSON из текста ответа HttpResponse
     *
     * @param response объект ответа проктокола http/https
     * @return JSONObject
     * @throws IOException
     * @throws ParseException
     */
    public static JSONObject getJsonFromHttpResponse(HttpResponse response)
        throws IOException, ParseException {
        String strResponse = getResponseText(response);
        return (JSONObject) new JSONParser().parse(strResponse);
    }

    /**
     * Позволяет получить текст ответа HttpResponse
     *
     * @param httpResponse объект ответа проктокола http/https
     * @return Ответ в виде String
     * @throws IOException
     */
    private static String getResponseText(HttpResponse httpResponse) throws IOException {
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity);
    }


    /**
     * Позволяет вернуть Map параметров запроса в удобоиспользуемом виде
     *
     * @param request объект класса HttpServletRequest
     * @return Map<String, Object>
     */
    public static Map<String, Object> getParamsMapFromRequest(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        Map<String, String[]> params = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String[] value = entry.getValue();
            Object o = (value.length == 1) ? value[0] : value;
            map.put(entry.getKey(), o);
        }

        return map;
    }


    /**
     * Позволяет записать текст в объект http ответа
     *
     * @param text     текст для записи
     * @param response объект http ответа
     */
    public static void writeTextToResponse(String text, HttpServletResponse response) {
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            writer.write(text);
            writer.flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }


    /**
     * Позволяет получить Cookie из объекта запроса
     *
     * @param request объект запроса
     * @param name    имя Cookie
     * @return Cookie
     */
    public static Cookie getCookie(HttpServletRequest request, String name) {

        return Arrays.stream(request.getCookies()).filter(
            c -> c.getName().equals(name)
        ).findFirst().orElse(null);

    }

}
