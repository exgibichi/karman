Проект Карман

Проект собирается по упрощенной схеме при помощи фреймворка Spring Boot.

Ниже приведена подроная инструкция по установке, сборке и запуску проекта.

1. Качаем с репозитория исходники.
2. Ставим Maven 3.1.11 или выше и подключаем его в качестве основного для своей IDE.
3. Импортируем Maven проект через pom.xml в корневой директории (в IntellIJ IDEA это делается из коробки).
4. Ставим Java 8 и выставляем в настройках проекта.
5. Устанавливаем PostgreSQL 9.4 и привязываем к порту 5432
   (можно использовать другой порт, но придется изменить файл application.properties в директории src/main/resources)
   Далее создаем пользователя в Postgres: create user saleband with password 'saleband';
   Назначаем права супервользователя: alter role saleband with superuser;
   И создаем этому пользователю свою базу: create database saleband with owner = saleband ENCODING = 'UTF8';
6. Запускаем проект Maven командой mvn spring-boot:run.

7. Для работы под Windows добавляем GIT в PATH

To add into PATH:
Right-Click on My Computer.
Click Advanced System Settings.
Click Environment Variables.
Then under System Variables look for the path variable and click edit.
Add the path to git's bin and cmd at the end of the string like this: ;C:\Program Files\Git\bin\git.exe;C:\Program Files\Git\cmd.


Другие полезные команды:

mvn clean - очищает собранный проект и скомпилированные файлы;
mvn package - компилит и собирает war архив (доступен в директории target под именем ROOT.war)

Можно применять цепочки команд, например mvn clean package

Продакшн сервер должен быть tomcat 8.0.35
Для деплоя приложения нужно поместить war архив в директорию /путь_к_tomcat/webapps
